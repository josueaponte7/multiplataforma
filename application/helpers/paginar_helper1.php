<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('paginar')){

    function paginar($config)
    {

        $ci = & get_instance();

        $pag_aumen   = 0;
        $total_rows  = $config['total_rows'];
        $per_page    = $config['per_page'];
        $uri_segment = $config['uri_segment'];
        $base_url    = $config['base_url'];
        $first       = (isset($config['first'])) ? $config['first'] : '&lt;&lt;';
        $previous    = (isset($config['previous'])) ? $config['previous'] : '&lt;';
        $next        = (isset($config['next'])) ? $config['next'] : '&gt;';
        $last        = (isset($config['last'])) ? $config['next'] : '&gt;&gt;';

        $total_pages = ceil($total_rows / $per_page);
        $offset = $ci->uri->segment($uri_segment,0);

        ?>
        <ul class="pagination">
            <?php
            if ($total_pages > 1) {
                if ($offset != 0){
                    if($offset-$pag_aumen == 0){
                        ?>
                        <li><a href="<?php echo base_url($base_url)?>"> <?php echo $previous ;?> </a></li>
                        <?php
                    }else{
                        ?>
                        <li><a href="<?php echo base_url($base_url)?>"> <?php echo $first;?> </a></li>
                        <li><a href="<?php echo base_url($base_url.($offset-1))?>"> <?php echo $previous ;?> </a></li>
                        <?php
                    }
                }
                if($offset == 0){
                    $offset = 1;
                }
                for ($i=$offset;$i<=$total_pages;$i++) {
                    if ($offset == $i){
                        ?>
                        <li class="active"><a><?php echo $i?></a></li>
                        <?php

                    }else{
                        if($i > 1){

                            ?>

                            <li><a href="<?php echo base_url($base_url.$i)?>"><?php  echo $i;?></a></li>
                            <?php
                        }else{
                            ?>
                            <li><a href="<?php echo base_url($base_url)?>"><?php echo $i?></a></li>
                            <?php
                        }
                    }
                }
                if($offset != ($total_pages)){
                    ?>
                    <li><a href="<?php echo base_url($base_url.($offset+1))?>"> <?php echo $next  ;?> </a></li>
                    <li><a href="<?php echo base_url($base_url.(($total_pages)))?>"> <?php echo $last  ;?> </a></li>
                    <?php
                }

            }
            ?>
        </ul>
        <?php
    }
}

if ( ! function_exists('offset')){

    function offset($offset, $per_page=5)
    {

        $ci  = & get_instance();
        $of  = $ci->uri->segment($offset);
        $off = (isset($of)) ? $ci->uri->segment($offset) : 0;
        $of  = 0;
        if($of == 0 || $off == 1){
            $of  = 0;
        }else if($off > 1 ){
            $of = ($off-1) * $per_page;
        }

        return $of;
    }
}
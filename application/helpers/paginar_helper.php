<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('paginar')){

    function paginar($config)
    {

        $ci = & get_instance();

        $pag_aumen   = 0;
        $total_rows  = $config['total_rows'];
        $per_page    = $config['per_page'];
        $uri_segment = $config['uri_segment'];
        $base_url    = $config['base_url'];
        $first       = (isset($config['first'])) ? $config['first'] : '&lt;&lt;';
        $previous    = (isset($config['previous'])) ? $config['previous'] : '&lt;';
        $next        = (isset($config['next'])) ? $config['next'] : '&gt;';
        $last        = (isset($config['last'])) ? $config['next'] : '&gt;&gt;';

        $total_pages = ceil($total_rows / $per_page);
        $current = $ci->uri->segment($uri_segment,0);

        $previous = $current - 1;
        $next = $current + 1;
        $text = '';

        if($current == 0){
            $current = 1;
        }
        if ($current>1){
            $text = "<a href=".base_url($base_url).">&laquo; </a> ";
            $text .= "<a href=".base_url($base_url.$previous)."> &lsaquo; </a> ";
        }else{
            if($current > 1){
                $text = "<b>&laquo;</b> ";
            }
        }
        for ($i=1; $i<$current; $i++){
            $link = base_url($base_url);
            if($i == 1){
                $link = base_url($base_url.$i+1);
            }
            $text .= "<a style='text-decoration: underline;' href=".base_url($base_url.$i).">$i</a> ";
        }
        $text .= "<b>$current</b> ";
        for ($i=$current+1; $i<=$total_pages; $i++){
            $text .= "<a style='text-decoration: underline;' href=".base_url($base_url.$i).">$i</a> ";
        }
        if ($current<$total_pages){
            if($current == 1){
                $next = $next + 1;
            }
            $text .= "<a href=".base_url($base_url.$next).">&rsaquo;</a> ";
            $text .= "<a href=".base_url($base_url.$total_pages)."> &raquo;</a>";
        }else{
            if($current != $total_pages){
                $text .= "<b>&raquo;</b>";
            }

        }
        return $text;

    }
}

if ( ! function_exists('offset')){

    function offset($offset, $per_page=5)
    {

        $ci  = & get_instance();
        $of  = $ci->uri->segment($offset);
        $off = (isset($of)) ? $ci->uri->segment($offset) : 0;
        $of  = 0;
        if($of == 0 || $off == 1){
            $of  = 0;
        }else if($off > 1 ){
            $of = ($off-1) * $per_page;
        }

        return $of;
    }
}
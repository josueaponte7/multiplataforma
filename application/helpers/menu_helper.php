<?php
if (!function_exists('menu')) {
    function menu() {
        $ci = & get_instance();

        $menu = $ci->libreria->menu();

        $opciones = '';
        $index = '';

        foreach ($menu as $value) {

            $url_p = '#';
            if($value->route != ''){
                $url_p = $value->route;
            }

            $submenu = $ci->libreria->submenu($value->id);
            if($submenu){
                $opciones .= '<li>
                                <a>
                                <i class="'.$value->icono.'"></i> '
                                .$value->modulo.'
                                <span class="fa fa-chevron-down">
                                </span>
                             </a>';
                $opciones .= '<ul class="nav child_menu">';
                foreach ($submenu as $key1 => $value1) {
                    $url_h = '#';
                    if($value1->route != ''){
                        $url_h = $value1->route;
                    }
                    $targe = '';
                    if($value->modulo == 'REPORTES'){
                        $targe = 'target="_blank"';
                    }
                    $opciones .= '<li><a href="'.base_url($index.$url_h).'" '.$targe.' >'.$value1->modulo.'</a></li>';
                }
                $opciones .= '</ul></li>';
            }else{

                $opciones .= '<li><a href="'.base_url($index.$url_p).'"><i class="'.$value->icono.'"></i> '.$value->modulo.'</a></li>';
            }
        }
        return $opciones;
    }
}
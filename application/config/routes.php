<?php

defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller']   = 'CHome';
$route['404_override']         = 'Error404';
$route['translate_uri_dashes'] = FALSE;
$route['assets/(:any)']        = 'assets/$1';

require_once( BASEPATH . 'database/DB.php');
$db     = & DB();
$db->select('slug, controller');
$query  = $db->get('se_app_routes');
$result = $query->result();
foreach ($result as $row) {
    $route[$row->slug]                = $row->controller;
    $route[$row->slug . '/agregar']   = $row->controller . '/agregar';
    $route[$row->slug . '/modificar'] = $row->controller . '/modificar';
    $route[$row->slug . '/eliminar']  = $row->controller . '/eliminar';
}

$query  = $db->select('route, controller, accion')->where("accion !=''" )->get('se_modulo');
$result = $query->result();

foreach ($result as $row) {
    if ($row->route != '') {

        list($guardar, $modificar, $eliminar, $buscar)= explode(',', $row->accion);
        $route[$row->route]                  = $row->controller;
        $route[$row->route. '/'.$guardar]    = $row->controller . '/'.$guardar;
        $route[$row->route. '/'.$modificar] = $row->controller . '/'.$modificar;
        $route[$row->route . '/'.$eliminar]  = $row->controller . '/'.$eliminar;
        $route[$row->route . '/'.$buscar]    = $row->controller . '/'.$buscar;

    }
}

$query  = $db->select('route, controller, reporte')->where("reporte" ,1)->get('se_modulo');
$result = $query->result();

foreach ($result as $row) {
    if ($row->route != '') {
        $route[$row->route]  = $row->controller;
    }
}

$route['registro/contribuyente/procesar'] = "registro/CContribuyente/procesar";
$route['seguridad/acceso/methods'] = "seguridad/CAcceso/methods";
$route['reporte/reporte/comprobante_factura'] = "reporte/CReporte/comprobante_factura";
$route['reporte/reporte/comprobante_remision'] = "reporte/CReporte/comprobante_remision";
$route['reporte/reporte/comprobante_retencion'] = "reporte/CReporte/comprobante_retencion";
$route['reporte/reporte/nota_debito'] = "reporte/CReporte/nota_debito";
$route['reporte/reporte/nota_credito'] = "reporte/CReporte/nota_credito";
$route['registro/producto/buscar_producto'] = "registro/CProducto/buscar_producto";
$route['registro/personal/estudio'] = "registro/CPersonal/estudio";
$route['registro/personal/referencia'] = "registro/CPersonal/referencia";
$route['registro/personal/empresa'] = "registro/CPersonal/empresa";
$route['registro/profesion/ajax_lista'] = "registro/CProfesion/ajax_lista";
$route['registro/personal/estudio_ajax'] = "registro/CPersonal/estudio_ajax";
$route['registro/personal/referencia_ajax'] = "registro/CPersonal/referencia_ajax";
$route['registro/personal/empresa_ajax'] = "registro/CPersonal/empresa_ajax";
$route['registro/personal/eliminar_estudio'] = "registro/CPersonal/eliminar_estudio";
$route['registro/personal/eliminar_referencia'] = "registro/CPersonal/eliminar_referencia";
$route['registro/personal/eliminar_empresa'] = "registro/CPersonal/eliminar_empresa";
$route['registro/aspirante/ajax_search'] = "registro/CAspirante/ajax_search";
$route['registro/aspirante/aspirante'] = "registro/CAspirante/aspirante";

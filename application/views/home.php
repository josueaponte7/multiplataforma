<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo assets_url('img/favicon.ico')?>">
    <title>DINDON</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
    <link href="<?php echo assets_url('vendors/bootstrap/dist/css/bootstrap.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/main.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/responsive.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/btn.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/superfish/superfish.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/theme-color-selector.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/owl.carousel.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/twitterfeed.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/typicons/typicons.min.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/forms.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/flickrfeed.css'); ?>" rel="stylesheet" />

    <link href="<?php echo assets_url('css/pulse/pm-slider.css'); ?>" rel="stylesheet" />

    <script src="<?php echo assets_url('vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <script src="<?php echo assets_url('vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo assets_url('js/url.js'); ?>"></script>
    <script src="<?php echo assets_url('js/login/login.js'); ?>"></script>
    <style type="text/css" media="screen">

        .navbar-brand {
            padding: 0px;
        }
        .navbar-default .navbar-nav > li > a {
            color: #3D1B0F;
        }
        .navbar-default {
            background-color: #FFCA00;
            border-color: transparent;
        }

        .navbar-default .navbar-nav > li > a:hover{
            color:#EA1B25;
        }

        .navbar > .container .navbar-brand, .navbar > .container-fluid .navbar-brand {
            margin-left: 15px;
        }

        .carousel-indicators .active {
            background-color:#FFFFFF;
            border-color: #FFFFFF;
            height: 12px;
            margin: 0;
            width: 12px;
        }

        .carousel-indicators li {
            background-color: #FFC600;
            border: 1px solid #FFC600;
            border-radius: 10px;
            cursor: pointer;
            display: inline-block;
            height: 10px;
            margin: 1px;
            text-indent: -999px;
            width: 10px;
        }
        .nopadding {
            padding: 0 !important;
            margin: 0 !important;
        }

        .img-responsive, .thumbnail > img, .thumbnail a > img{
            display: block;
            height: 100%;
            max-width: 100%;
        }

        .navbar-default .navbar-nav > li > a:hover {
            color: #ef5438;
        }

        .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
            background-color: transparent;
            color: #ef5438;
        }

        .navbar-default .navbar-toggle:hover,
        .navbar-default .navbar-toggle:focus {
            background-color: #3d1b0f;
        }

        .fondo-login{
            background-color:rgba(255, 202, 0, 0.8);
        }

        .main-text{
            position: absolute;
            top: 75%;
            left:25%;
            width: 50%;
        }

        .search{
            border-radius: 95px;
            background-color: rgba(255, 202, 0, 0.5);
            border-color:  #FFCA00;
            color: #3d1b0f;
            font-weight: 500;
        }

        .search::-webkit-input-placeholder {
            color: #3d1b0f;
        }
        .search:-moz-placeholder {
            color: #3d1b0f;
        }
        .search::-moz-placeholder {
            color: #3d1b0f;
        }
        .search:-ms-input-placeholder {
            color: #3d1b0f;
        }

        .search:focus{
            border-color: #FFCA00;;
            box-shadow: none;
        }

        .buscar{
            color: #3d1b0f;
            cursor: pointer;
            margin-right: 15px;
            margin-top: 8px;
        }

        .text-center p.fondo{
            background-color: rgba(0, 0, 0, 0.7);
            display: block;
            /*width: 100%;*/
            height: 16px;
            margin-left:0px;
            margin-top:3px;
            border-radius: 95px;
            text-align: center;
        }
        .text-center > p.fondo  > a{
            color: #ffca00;
            font-family: "Roboto Slab";
            font-weight: 600;
            font-size: 11px;
            font-style: italic;
            position: relative;
            vertical-align: middle;
            padding-bottom: 10px;
            line-height: 10px;
            display: inline-block;
        }

        .text-center > p.fondo > a:hover{
            color:#FFFFFF;
        }

        body {
            padding-top: 10px;
        }

        @media screen and (min-width:240px) and (max-width:318px) {
            .fondo-login{
                /*background-color:rgba(255, 202, 0, 0.8);*/
                background-color: gray;
            }
            .main-text{
                position: absolute;
                top: 10%;
                left:0;
                width: 100%;
            }
            .search{
                height: 25px;
            }

            .buscar{
                color: #FFCA00;
                cursor: pointer;
                margin-top: 5px;
            }

            .text-center p.fondo{
                width: 240px;
                height: 16px;
                top:40px;
                right: 15px;
                position: relative;
            }
            .text-center > p.fondo  > a{
                font-size:7px;
                font-style: italic;
                position: relative;
                vertical-align: middle;
                padding-bottom: 10px;
                line-height: 10px;
                display: inline-block;
                right: 6px;
            }
        }

        @media screen and (min-width:320px) and (max-width:438px) {
            .navbar-nav .open .dropdown-menu {
                background-color:rgba(255, 202, 0, 0.8);
            }
            .main-text{
                position: absolute;
                top: 10%;
                left:0;
                width: 100%;
            }
            .search{
                height: 25px;
            }

            .buscar{
                margin-top: 5px;
            }

            .text-center p.fondo{
                width: 100%;
                height: 16px;
                top:70px;
                position: relative;
            }
            .text-center > p.fondo  > a{
                font-size:8.4px;
            }
        }

        @media screen and (min-width:480px) and (max-width:768px) {
            .fondo-login{
                /*background-color:rgba(255, 202, 0, 0.8);*/
                background-color: green;
            }
            .main-text{
                position: absolute;
                top: 57%;
                left:90px;
                width: 60%;
            }
            .search{
                height: 25px;
            }

            .buscar{
                margin-top: 5px;
            }

            .text-center p.fondo{
                width: 100%;
                height: 16px;
                position: relative;
            }
            .text-center > p.fondo  > a{
                font-size:8.4px;
            }
        }

        @media screen and (min-width:768px) and (max-width:990px) {
            .main-text{
                position: absolute;
                top: 68%;
                left:90px;
                width: 75%;
            }
            .search{
                height: 25px;
            }

            .buscar{
                margin-top: 5px;
            }

            .text-center > p.fondo  > a{
                font-size:10px;
            }
        }
        .center-text > a{
            font-size:  10px;
        }

        h3 {
            color: #ef5438;
            font-family: "Roboto Slab",serif;
            font-size: 24px;
            font-weight: 300;
            margin-bottom: 10px;
            margin-top: 0;
            padding: 0;
        </style>
    </head>
    <body>
        <header>

            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="<?php echo assets_url('img/logosinfondo.png'); ?>" class="img-responsive pm-header-logo" alt="DINDON">
                        </a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-cogs">
                                    </i> <b class="color-blue">Acceder</b>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu login-panel fondo-login" >
                                    <li>
                                        <div class="dropdown-header">
                                            <!-- <span class="login-header color-blue" style="color:#3d1b0f">Sign In</span> -->
                                            <span class="forgot-password color-blue">
                                                <a href="" style="color: #FFFFFF;font-weight: 600;text-align: center;">Registrate</a>
                                            </span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div style="padding: 8px">
                                            <?php
                                            $attributes = array('id' => 'frmlogin');
                                            echo form_open('seguridad/users/validatelogin', $attributes);
                                            ?>
                                            <div style="margin-bottom: 10px" class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-user color-blue"></i>
                                                </span>
                                                <input type="text" class="form-control" name="user" id="user" value="" placeholder="Usuario" pattern="[a-zA-Z0-9]{5,}" title="Minimum 5 letters or numbers." oninvalid="this.setCustomValidity('Enter User Name Here')" oninput="setCustomValidity('')" required>
                                            </div>
                                            <div style="margin-bottom: 10px" class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-lock color-blue"></i>
                                                </span>
                                                <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" pattern=".{5,}" title="Minmimum 5 letters or numbers." oninvalid="this.setCustomValidity('Enter a password')" oninput="setCustomValidity('')" required>
                                            </div>
                                            <div id="error" class="center-text" style="color: #FF0000;font-size: 10px;display: none">
                                                <span class="error-message color-red">
                                                <i class="glyphicon glyphicon-warning-sign">

                                                </i>Usuario o Clave incorrectos</span>
                                            </div>
                                            <div style="margin-top:10px" class="form-group">
                                                <!-- Button -->
                                                <div class="col-sm-12 controls center-text">
                                                    <button id="login" type="button" class="btn btn-block btn-success">Iniciar Sesion</button>
                                                    <!--<a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a>-->
                                                </div>
                                            </div>
                                            <div class="center-text">
                                                <a href="" style="color: #FFFFFF;font-weight: 600;text-align: center;">&iquestOlvido su contraseña?</a>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right pm-footer-social-icons" >
                        <li title="Twitter" class="pm_tip_static_top">
                            <a href="#">
                                <i class="fa fa-twitter tw"></i>
                            </a>
                        </li>
                        <li title="Facebook" class="pm_tip_static_top">
                            <a href="#">
                                <i class="fa fa-facebook fb">
                                </i>
                            </a>
                        </li>
                        <li title="Instagram" class="pm_tip_static_top" >
                            <a href="#">
                                <i class="fa fa-instagram in">
                                </i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#">Restaurante</a>
                        </li>
                        <li>
                            <a href="#">Comida</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="container" >
        <br>
        <br>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 nopadding">
                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="5000">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img class="first-slide" src="<?php echo assets_url('img/slider/slide1.jpg'); ?>" alt="First slide">
                        </div>
                        <div class="item">
                            <img class="second-slide" src="<?php echo assets_url('img/slider/slide1.jpg'); ?>" alt="Second slide">
                        </div>
                        <div class="item">
                            <img class="third-slide" src="<?php echo assets_url('img/slider/slide1.jpg'); ?>" alt="Third slide">
                        </div>
                    </div>
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="main-text">
                    <div class="col-md-12 text-center">
                        <input type="text" class="form-control search" placeholder="Buscar" />
                        <i  class="form-control-feedback fa fa-search buscar"></i>
                        <p class="fondo">
                            <a href="">Busca segun tu informacion /</a>
                            <a href="">Restaurante Favorito /</a>
                            <a href="">Comida favorita</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-xs-12 nopadding">
                <div class="pm-image-container" >
                    <img class="img-responsive" src="<?php echo assets_url('img/comidas/kfc1.jpg'); ?>" alt="image1">
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 nopadding"  style="background-color: #F73349;height: 354px;">
                <h2 style="margin-top:40%;margin-left:25px;">ORDENAR <br/>AHORA</h2>
                <a href="menus.html" class="flecha" style="display: block;margin-top: 100px;font-size: 50px;">
                    <i class="fa fa-long-arrow-right"></i>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12" >
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 nopadding" style="height: 247px;">
                        <img class="img-responsive" src="<?php echo assets_url('img/comidas/sundae.jpg'); ?>" alt="image3">
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12 nopadding" style="height: 107px;background-color: #3D1B0F;">
                        <h3 style="color:#FFFFFF; margin-left: 20px;">SUNDAE <br/>GRATIS</h3>
                        <p style="color:#D9D9D9;margin-left: 20px;margin-top:-15px;">En ordenes superiores a los $20 .
                            <a style="margin-bottom: 150px;float: right;margin-right:10px;font-size: 30px;" href="store.html">
                                <i class="fa fa-long-arrow-down" style="color: #FFFFFF"></i>
                            </a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 354px;">
            <div class="col-lg-6 col-md-6 col-xs-12 nopadding" >
                <img class="img-responsive" src="<?php echo assets_url('img/comidas/coffe.jpg'); ?>" alt="image1">
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12" style="background-color: #21672B;height: 354px;">
                <p>
                    <h3 style="color:#FFFFFF">TE APETECE <br/>UN CAFE</h3>
                    <p style="color:#D9D9D9;text-transform: uppercase;">Conoce nuestros locales en quito <br/>  y guayaquil. </p>
                    <a href="">Ver</a>
                    <a href="store.html" class="">
                        <i class="fa fa-long-arrow-right"></i>
                    </a>
                </p>
            </div>
        </div>
        <div class="row" style="height: 175">
            <div class="col-lg-12 col-md-12 col-xs-12 nopadding">
                <img class="img-responsive" src="<?php echo assets_url('img/comidas/pan.jpg'); ?>" alt="image1">
            </div>
        </div>
        <div class="row" >
            <div class="col-lg-12 col-md-12 col-xs-12 nopadding" style="height: 175px; background-color: #FFC600">
                <h6 style="color:#3d1b0f">EL PAN DE SIEMPRE</h6>
                <h6 style="color:#3d1b0f">COMO A TU TE GUSTA</h6>
                <p style="color:#3d1b0f">HAZ TU ORDEN DE PAN AHORA.</p>
                <a href="store.html" class="pm-rounded-btn animated pm-primary">
                    <i class="fa fa-long-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="row" >
            <div class="col-lg-4 col-md-3 col-xs-12 nopadding" style="height: 354px;">
                <img class="img-responsive" src="<?php echo assets_url('img/comidas/kfc.jpg'); ?>" alt="image1">
            </div>
            <div class="col-lg-2 col-md-1 col-xs-12 nopadding" style="height: 354px; background-color: #3D1B0F;">
                <h6 style="color:#FFFFFF">TIENES </h6>
                <h6 style="color:#FFFFFF">HAMBRE?</h6>
                <p style="color:#FFFFFF">HAZ TU ORDEN DE PAN AHORA.</p>
                <a href="store.html" class="pm-rounded-btn animated pm-primary">
                    <i class="fa fa-long-arrow-right"></i>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-12 nopadding" style="height: 354px; background-color: #F73349">
                <h6 style="color:#FFFFFF">PARA</h6>
                <h6 style="color:#FFFFFF">LLEVAR</h6>
                <a href="store.html" class="pm-rounded-btn animated pm-primary">
                    <i class="fa fa-long-arrow-right"></i>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-12 nopadding" style="height: 354px; background-color: grey">
                <img class="img-responsive" src="<?php echo assets_url('img/comidas/para_llevar.jpg'); ?>" alt="image1">
            </div>
        </div>
        <div class="row" >
            <div class="col-lg-12 col-md-12 col-xs-12 nopadding" style="height: 600px;background-color: #D9D9D9">
                &nbsp;
            </div>
        </div>
        <footer>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" style="background-color: #FFC600;height: 180px;">
                    <div class="pm-footer-social-info-container">
                        <h6>&nbsp;</h6>
                        <ul class="pm-footer-social-icons">
                            <li title="Twitter" class="pm_tip_static_top"><a href="#"><i class="fa fa-twitter tw"></i></a></li>
                            <li title="Facebook" class="pm_tip_static_top"><a href="#"><i class="fa fa-facebook fb"></i></a></li>
                            <li title="Google Plus" class="pm_tip_static_top"><a href="#"><i class="fa fa-google-plus gp"></i></a></li>
                            <li title="Linkedin" class="pm_tip_static_top"><a href="#"><i class="fa fa-linkedin linked"></i></a></li>
                            <li title="YouTube" class="pm_tip_static_top"><a href="#"><i class="fa fa-youtube yt"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script src="<?php echo assets_url('js/jquery.viewport.mini.js'); ?>"></script>
    <script src="<?php echo assets_url('js/jquery.easing.1.3.js'); ?>"></script>
    <script src="<?php echo assets_url('js/modernizr.custom.js'); ?>"></script>
    <script src="<?php echo assets_url('js/owl.carousel.js'); ?>"></script>
    <script src="<?php echo assets_url('js/main.js'); ?>"></script>
    <script src="<?php echo assets_url('js/jquery.tooltip.js'); ?>"></script>
    <script src="<?php echo assets_url('js/jquery.hoverPanel.js'); ?>"></script>
    <script src="<?php echo assets_url('js/superfish.js'); ?>"></script>
    <script src="<?php echo assets_url('js/hoverIntent.js'); ?>"></script>
    <script src="<?php echo assets_url('js/tinynav.js'); ?>"></script>
    <script src="<?php echo assets_url('js/jquery.stellar.js'); ?>"></script>
    <script src="<?php echo assets_url('js/countdown.js'); ?>"></script>
    <script src="<?php echo assets_url('js/theme-color-selector.js'); ?>"></script>
    <script src="<?php echo assets_url('js/wow.min.js'); ?>"></script>
    <script src="<?php echo assets_url('js/wow.min.js'); ?>"></script>
    <script src="<?php echo assets_url('js/jquery.PMSlider.js'); ?>"></script>
    <p id="back-top" class="visible-lg visible-md visible-sm"> </p>
</body>
</html>

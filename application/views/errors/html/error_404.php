<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SISTEMA DE RESEÑA</title>

    <!-- Bootstrap -->

    <link href="<?php echo assets_url('vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo assets_url('vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo assets_url('vendors/nprogress/nprogress.css'); ?>" rel="stylesheet" />
	<link href="<?php echo assets_url('build/css/custom.min.css'); ?>" rel="stylesheet" />
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">404</h1>
              <h2>Lo sentimos no se pudo encontrar la pagina</h2>
              <p><a href="<?php echo base_url();?>">Haga click para volver</a>
              </p>

            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>

    <script src="<?php echo assets_url('vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <script src="<?php echo assets_url('vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo assets_url('vendors/nprogress/nprogress.js'); ?>"></script>

    <script src="<?php echo assets_url('js/bootbox.min.js'); ?>"></script>
  </body>
</html>

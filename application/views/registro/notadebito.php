<style type="text/css">
    .span{
        color:red;font-weight:bold;
    }
    .wizard a {
        padding: 10px 12px 10px;
        margin-right: 5px;
        background: #efefef;
        position: relative;
        display: inline-block;
    }
    .wizard a:before {
        width: 0;
        height: 0;
        border-top: 20px inset transparent;
        border-bottom: 20px inset transparent;
        border-left: 20px solid #fff;
        position: absolute;
        content: "";
        top: 0;
        left: 0;
    }
    .wizard a:after {
        width: 0;
        height: 0;
        border-top: 20px inset transparent;
        border-bottom: 20px inset transparent;
        border-left: 20px solid #efefef;
        position: absolute;
        content: "";
        top: 0;
        right: -20px;
        z-index: 2;
    }
    .wizard a:first-child:before,
    .wizard a:last-child:after {
        border: none;
    }
    .wizard a:first-child {
        -webkit-border-radius: 4px 0 0 4px;
        -moz-border-radius: 4px 0 0 4px;
        border-radius: 4px 0 0 4px;
    }
    .wizard a:last-child {
        -webkit-border-radius: 0 4px 4px 0;
        -moz-border-radius: 0 4px 4px 0;
        border-radius: 0 4px 4px 0;
    }
    .wizard .badge {
        margin: 0 5px 0 18px;
        position: relative;
        top: -1px;
    }
    .wizard a:first-child .badge {
        margin-left: 0;
    }
    .wizard .current {
        background: #007ACC;
        color: #fff;
    }
    .wizard .current:after {
        border-left-color: #007ACC;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="wizard">
                    <?php foreach ($estado as $value) { ?>
                    <?php if($value->id == 1){?>
                    <a class="current" id="wizard_<?php echo $value->id?>"><span class="badge"><?php echo $value->id?></span><?php echo $value->comprobante_electronico?></a>
                    <?php }else{?>
                    <a><span class="badge" id="wizard_<?php echo $value->id?>"><?php echo $value->id?></span> <?php echo $value->comprobante_electronico?></a>
                    <?php }?>
                    <?php }?>
                </div>
                <br />
                <?php
                $attributes = array('id' => 'frmimpuesto', 'class' => 'formulario');
                echo form_open('registro/impuesto/guardar', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <input type="hidden" name="estado_id" value="1">
                <div class="form-group">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="contribuyente_id">Nro Factura*</label>
                        <select name="contribuyente_id" id="contribuyente_id" class="form-control" data-validate='required'  style="width:100%;">
                            <option value="0">Seleccione</option>
                            <?php foreach ($contribuyente as $value) { ?>
                            <option value="<?php echo $value->id?>"><?php echo str_pad((int) $value->num_comprobante, (int) 9, '0', STR_PAD_LEFT);?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <label for="comprobante_id">Comprobante *</label>
                        <select id="comprobante_id" name='comprobante_id' class="form-control" style="width:100%;" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($comprobante as $value) { ?>
                            <?php if($value->id == 3){?>
                            <option selected title="<?php echo $value->descripcion?>" value="<?php echo $value->id?>"><?php echo $value->nom_comprobante?></option>
                            <?php }?>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="clave_acceso_id">Clave de Acceso *</label>
                        <select name="clave_acceso_id" id="clave_acceso_id" class="form-control" data-validate='required'  style="width:100%;">
                            <option value="0">Seleccione</option>
                            <?php foreach ($clave_acceso as $value) { ?>
                            <option value="<?php echo $value->id?>"><?php echo $value->clave_acceso ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <table id="tbldetalles" style="width:99%" data-fields='0' data-counter="0" data-column='0' class="table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                        <thead>
                            <tr>
                            <th>Razón de la Modificación</th>
                                <th style='text-align:right;'>Valor</th>
                                <th style='text-align:right;'>Plazo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input id="razon_modificacion" name="razon_modificacion" class="form-control reset" type="text" data-validate='required|max(50)|min(5)' data-type="alphanumeric" data-text="upper" style="width:100%;">
                                </td>
                                <td style='text-align:right;'>
                                    <input id="total" name="total" class="form-control reset" type="text" data-validate='required' data-type="integer">
                                </td>
                                <td style='text-align:right;'>
                                    <input id="plazo" name="plazo" class="form-control reset" type="text" data-validate='required|min(1)|max(3)' data-type="integer">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table style="width: 100%;" id="tbl_impuesto" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Fecha de Emisión</th>
                                    <th class="column-title">Razón de la Modificación</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->fecha_emision?></td>
                                        <td><?php echo $lista->razon_modificacion?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
    <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
    <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


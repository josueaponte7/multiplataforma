<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Registro de Consumidor Final / Distribuidor</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmcliente');
                echo form_open('registro/cliente/guardar', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <input type="hidden" value="<?php echo $num_ruc?>" id="id_codigo">
                <div class="form-group">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="pais_id">Pais *</label>
                        <select name="pais_id" id="pais_id" class="form-control" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($pais as $value) { ?>
                                <option value="<?php echo $value->id?>"><?php echo $value->pais?></option>
                            <?php }?>
                            option
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <label for="categoria_cliente_id">Categoria Cliente *</label>
                        <select name="categoria_cliente_id" id="categoria_cliente_id" class="form-control" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($categoriacliente as $value) { ?>
                                <option value="<?php echo $value->id?>"><?php echo $value->categoria_cliente?></option>
                            <?php }?>
                            option
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="tipo_cliente_id">Categoria Identificación *</label>
                        <select name="tipo_cliente_id" id="tipo_cliente_id" class="form-control" data-add="id" data-mod="1" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($identificacion as $value) { ?>
                                <option value="<?php echo $value->id?>"><?php echo $value->nom_identificacion?></option>
                            <?php }?>
                            option
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label for="cedula">Cédula * </label>
                        <input id="cedula" placeholder="Cédula" name="cedula" class="form-control" type="text" data-validate='required|max(10)|min(10)' data-type="integer" data-add="id" data-mod="1">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="identificacion">Identificación * </label>
                        <input id="identificacion" placeholder="Identificación (Número de RUC, Cédula o Pasaporte)" name="identificacion" class="form-control" type="text" data-validate='max(13)|min(5)' data-type="alphanumeric" data-text="upper" data-add="id" data-mod="1">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="telefono">Teléfono * </label>
                        <input id="telefono" placeholder="Teléfono" name="telefono" class="form-control" type="text" data-validate='required|max(16)|min(16)' data-type="phone" data-text="upper">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="razon_social">Razón Social * </label>
                        <input id="razon_social" placeholder="Nombres y Apellidos o Razón Social" name="razon_social" class="form-control" type="text" data-validate='required|max(30)|min(5)' data-type="alphanumeric" data-text="upper">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <label for="email">Dirección de Correo Electrónico * </label>
                        <input id="email" placeholder="Dirección de Correo Electrónico" name="email" class="form-control" type="text" data-validate='required|email' data-text="lower">
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="placa_guia_remisión">Placa / Guia de Remision * </label>
                        <input readonly="" id="placa_guia_remision" placeholder="Guia de Remisión" name="placa_guia_remision" class="form-control" type="text" data-validate='max(10)|min(5)' data-type="alphanumeric" data-text="upper">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="direccion">Dirección / Ubicación * </label>
                        <textarea id="direccion" placeholder="Dirección / Ubicación" name="direccion" class="form-control" type="text" data-validate='required|max(30)|min(5)' data-type="alphanumeric" data-text="upper"></textarea>
                    </div>
                </div>
                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table id="tbl_cliente" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Identificación</th>
                                    <th class="column-title">Razón Social</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->identificacion?></td>
                                        <td><?php echo $lista->razon_social?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo assets_url('script/cliente.js'); ?>" type="text/javascript" charset="utf-8" ></script>


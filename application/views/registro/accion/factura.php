<br />
<?php
$attributes = array('id' => 'frmcontribuyente', 'class' => 'formulario');
echo form_open('registro/contribuyente/guardar', $attributes);
?>
<?php echo form_hidden('token', $token) ?>
<input type="hidden" name="id" value="<?php echo $id?>" id="id" class='lastId'>
<input type="hidden" value="<?php echo $num_ruc?>" id="id_codigo">
<input type='hidden' name='estado_id' value='1'/>
<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <label for="cliente_id">Cliente *</label>
        <select id="cliente_id" name='cliente_id' class="form-control" style="width:100%;" data-validate='required'>
            <option value="0">Seleccione</option>
            <?php foreach ($cliente as $value) { ?>
            <option value="<?php echo $value->id?>"><?php echo $value->razon_social."-".$value->identificacion?></option>
            <?php }?>
        </select>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        <label for="comprobante_id">Comprobante *</label>
        <select id="comprobante_id" name='comprobante_id' class="form-control" style="width:100%;" data-validate='required'>
            <option value="0">Seleccione</option>
            <?php foreach ($comprobante as $value) { ?>
            <option title="<?php echo $value->descripcion?>" value="<?php echo $value->id?>"><?php echo $value->nom_comprobante?></option>
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <label for="num_ruc">Número de Registro *</label>
        <input id="num_ruc" readonly='' placeholder="Número de Registro Unico del Contribuyente" name="num_ruc" class="form-control reset" type="text" data-validate='required|max(13)|min(13)' data-add="id" data-mod="1" data-type="alphanumeric" data-text="upper"  style="width:100%;">
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4">
        <label for="cod_auxiliar">Ambiente *</label>
        <select name="ambiente_id" id="ambiente_id" class="form-control"  style="width:100%;" data-add="id" data-mod="1" data-validate='required'>
            <option value="0">Seleccione</option>
            <?php foreach ($ambiente as $value) { ?>
                <option selected value="<?php echo $value->id?>"><?php echo $value->nom_ambiente?></option>
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-md-4 col-sm-4 col-xs-4">
        <label for="num_serie">Número de serie *</label>
        <input id="num_serie" readonly="" value="<?php echo "001".$num_serie?>" placeholder="Número de serie" name="num_serie" class="form-control reset" type="text" data-validate='required|max(6)|min(6)' data-type="alphanumeric" data-text="upper" style="width:100%;">
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4">
        <label for="num_comprobante">Número de comprobante secuencial *</label>
        <input id="num_comprobante" readonly="" value="<?php echo $num_comprobante?>" placeholder="Número de comprobante secuencial" name="num_comprobante" class="form-control reset" type="text" data-validate='required|max(9)|min(9)' data-type="alphanumeric" data-text="upper"  style="width:100%;">
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4">
        <label for="cod_numerico">Código numérico *</label>
        <input id="cod_numerico" readonly="" value='<?php echo $cod_numerico;?>' placeholder="Número de comprobante secuencial" name="cod_numerico" class="form-control reset" type="text" data-validate='required|max(8)|min(8)' data-type="alphanumeric" data-text="upper"  style="width:100%;">
    </div>
</div>
<div class="form-group">
    <div class="col-md-4 col-sm-4 col-xs-4">
        <label for="emision_id">Emisión *</label>
        <select name="emision_id" id="emision_id" class="form-control" data-validate='required'  style="width:100%;">
            <option value="0">Seleccione</option>
            <?php foreach ($emision as $value) { ?>
                <option selected value="<?php echo $value->id?>"><?php echo $value->nom_emision ?></option>
            <?php }?>
        </select>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-8">
          <label for="clave_acceso_id">Clave Acceso *</label>
          <select name="clave_acceso_id" id="clave_acceso_id" class="form-control" data-validate='required'  style="width:100%;">
                <option value="0">Seleccione</option>
                <?php foreach ($clave_acceso as $value) { ?>
                    <option value="<?php echo $value->id?>"><?php echo $value->clave_acceso ?></option>
                <?php }?>
          </select>
    </div>
</div>
<?php echo form_close();?>
<br/>
<!--<div class="ln_solid"></div>
<div class="col-xs-12">
    <div class="table-responsive">
        <table id="tbl_factura" data-counter="2" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action" style="width:100%;">
            <thead>
                <tr>
                    <th class="column-title">#</th>
                    <th class="column-title">Cliente</th>
                    <th class="column-title">Razón Social</th>
                    <th class="column-title">Modificar</th>
                    <th class="column-title">Eliminar</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($lista as $lista) {
                    ?>
                    <tr id="<?php echo $lista->id?>">
                        <td><?php echo $i?></td>
                        <td><?php echo $lista->identificacion?></td>
                        <td><?php echo $lista->razon_social?></td>
                        <td>
                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                        </td>
                        <td>
                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>
</div>-->

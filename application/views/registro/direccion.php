
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmimpuesto', 'class' => 'formulario');
                echo form_open('registro/impuesto/guardar', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <?php echo form_hidden('aspirante_id', $pk) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="region_id">Región *</label>
                        <select name="region_id" id="region_id" class="form-control" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($region as $value) { ?>
                            <option value="<?php echo $value->id?>"><?php echo $value->region;?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label for="codigo">Código *</label>
                        <input id="codigo" name="codigo" class="form-control reset" type="text" data-validate='required|max(3)|min(1)' data-type="numeric" data-add="id" data-mod="1">
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10">
                        <label for="provincia"> Provincia * </label>
                        <input id="provincia" name="provincia" class="form-control reset" type="text" data-validate='required|max(50)|min(5)' data-type="alpha" data-text="upper" data-add="id" data-mod="1">
                    </div>
                </div>
                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table style="width: 100%;" id="tbl_impuesto" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Código</th>
                                    <th class="column-title">Provincia</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->codigo?></td>
                                        <td><?php echo $lista->provincia?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
    <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
    <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


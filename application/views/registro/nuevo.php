   <style type="text/css">
       #image-preview {
           width: 130px;
           height: 180px;
           position: relative;
           overflow: hidden;
           color: #BD5800;
           font-weight: bold;
           margin-left: 11px;
           cursor: pointer;
           background-color: rgba(196,196,196, 0.8);
           /*background-image: url(assets_url("images/default_avatar_male.jpg"));*/
       }
       #image-preview input {
           line-height: 200px;
           font-size: 200px;
           position: absolute;
           opacity: 0;
           cursor: pointer;
           z-index: 10;
           margin-left:11px;
       }
       #image-preview label {
           position: absolute;
           z-index: 5;
           opacity: 0.8;
           cursor: pointer;
           background-color: #bdc3c7;
           width: 200px;
           height: 25px;
           cursor: pointer;
           font-size: 15px;
           line-height: 25px;
           text-transform: uppercase;
           top: 25;
           left: 0;
           right: 0;
           bottom: 0;
           margin: auto;
           text-align: center;
       }
   </style>
   <div class="col-md-12 col-sm-12 col-xs-12">
       <div class="x_panel">
           <div class="x_title">
               <h2><?php echo $titulo;?></h2>
               <div class="clearfix"></div>
           </div>
           <div class="container">
               <br />

               <div class="tab-content">
                   <!-- Datos de la Factura -->
                   <div class="tab-pane active">
                       <div class="form-group">
                           <div class="col-md-12 col-sm-12 col-xs-12">
                               <fieldset>
                                   <legend>Datos Principales</legend>
                               </fieldset>
                           </div>
                       </div>
                       <?php
                       $attributes = array('id' => 'frmaspirante', 'class' => 'formulario', 'enctype' => 'multipart/form-data');
                       echo form_open('registro/aspirante/guardar', $attributes);
                       ?>
                       <?php echo form_hidden('token', $token) ?>
                       <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                       <div class="form-group">
                           <div class="col-md-2 col-sm-2 col-xs-2">
                               <div id="image-preview">
                                   <label for="foto_perfil" id="image-label">Perfil</label>
                                   <input type="file" name="foto_perfil" id="foto_perfil" />
                               </div>
                           </div>
                           <div class="col-md-5 col-sm-5 col-xs-5">
                               <label for="p_nombre">Primer Nombre * </label>
                               <input id="p_nombre" value="<?php echo $row->p_nombre; ?>" name="p_nombre" class="form-control reset" type="text" data-validate='required|max(30)|min(4)' data-type="alpha" data-text="upper">
                           </div>
                           <div class="col-md-5 col-sm-5 col-xs-5">
                               <label for="s_nombre">Segundo Nombre * </label>
                               <input id="s_nombre" value="<?php echo $row->s_nombre; ?>" name="s_nombre" class="form-control reset" type="text" data-validate='max(30)|min(4)' data-type="alpha" data-text="upper">
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="col-md-5 col-sm-5 col-xs-5">
                               <label for="p_apellido">Primer Apellido * </label>
                               <input id="p_apellido" value="<?php echo $row->p_apellido; ?>" name="p_apellido" class="form-control reset" type="text" data-validate='required|max(30)|min(4)' data-type="alpha" data-text="upper">
                           </div>
                           <div class="col-md-5 col-sm-5 col-xs-5">
                               <label for="s_apellido">Segundo Apellido * </label>
                               <input id="s_apellido" value="<?php echo $row->s_apellido; ?>" name="s_apellido" class="form-control reset" type="text" data-validate='max(30)|min(4)' data-type="alpha" data-text="upper">
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="col-md-2 col-sm-2 col-xs-2">
                               <label for="nacionalidad">Nacionalidad * </label>
                               <select name="nacionalidad" id="nacionalidad" class="form-control reset" data-validate='required'>
                                   <option value="0">Seleccione</option>
                                   <option value="1">ECUATORIANO</option>
                                   <option value="2">EXTRANJERO</option>
                               </select>
                           </div>
                           <div class="col-md-3 col-sm-3 col-xs-3">
                               <label for="cedula">Cédula * </label>
                               <input id="cedula" value="<?php echo $row->cedula; ?>" name="cedula" class="form-control reset" type="text" data-validate='required|max(10)|min(8)' data-type="integer">
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="col-md-3 col-sm-3 col-xs-3">
                               <label for="fecha_nac">Fecha / Nacimiento *</label>
                               <input id="fecha_nac" value="<?php echo $row->fecha_nac; ?>" name="fecha_nac" class="form-control reset" type="text" data-validate='required|max(10)|min(10)' data-type="date">
                           </div>
                           <div class="col-md-2 col-sm-2 col-xs-2">
                               <label for="edad">Edad *</label>
                               <input id="edad" value="<?php echo $row->edad; ?>" readonly="" name="edad" class="form-control reset" type="text" data-validate='required|max(2)|min(2)' data-type="integer" data-text="upper">
                           </div>
                       </div>
                       <div class='hide_contenido'>
                           <div class="form-group">
                               <div class="col-md-12 col-sm-12 col-xs-12">
                                   <label for="lugar_nac">Lugar de Nacimiento *</label>
                                   <input id="lugar_nac" value="<?php echo $row->lugar_nac; ?>" name="lugar_nac" class="form-control reset" type="text" data-validate='required|max(255)|min(5)' data-type="alphanumeric" data-text="upper">
                               </div>
                           </div>
                           <div class="form-group">
                               <div class="col-md-12 col-sm-12 col-xs-12">
                                   <label for="direccion">Dirección *</label>
                                   <input id="direccion" value="<?php echo $row->direccion; ?>" name="direccion" class="form-control reset" type="text" data-validate='required|max(255)|min(5)' data-type="alpha">
                               </div>
                           </div>
                           <div class="form-group">
                               <div class="col-md-6 col-sm-6 col-xs-6">
                                   <label for="genero_id">Género *</label>
                                   <select name="genero_id" id="genero_id" class="form-control reset" data-validate='required'>
                                       <option value="0">Seleccione</option>
                                       <?php foreach ($genero as $value) {
                                           $ec = $row->genero_id;
                                           $sele = 0;
                                           if($value->id == (int)$ec){
                                               $sele = "selected";
                                           }
                                           ?>
                                           <option value="<?php echo $value->id; ?>" <?php echo $sele ?>><?php echo $value->genero; ?></option>
                                           <?php }?>
                                       </select>
                                   </div>
                                   <div class="col-md-6 col-sm-6 col-xs-6">
                                       <label for="estado_civil_id">Estado Civil *</label>
                                       <select name="estado_civil_id" id="estado_civil_id" class="form-control reset" data-validate='required'>
                                           <option value="0">Seleccione</option>
                                           <?php foreach ($estadocivil as $value) {
                                               $ec = $row->estado_civil_id;
                                               $sele = 0;
                                               if($value->id == (int)$ec){
                                                   $sele = "selected";
                                               }
                                               ?>
                                               <option value="<?php echo $value->id; ?>" <?php echo $sele ?>><?php echo $value->descripcion; ?></option>
                                               <?php }?>
                                           </select>
                                       </div>
                                   </div>

                                   <div class="form-group">
                                       <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%;">
                                           <fieldset>
                                               <legend>Información Adicional</legend>
                                           </fieldset>
                                       </div>
                                   </div>
                                   <div class="col-md-3 col-sm-3 col-xs-3">
                                       <label for="estatus">Estátus *</label>
                                       <select name="estatus" id="estatus" class="form-control reset" data-validate='required'>
                                           <option value="0">Seleccione</option>
                                           <option value="1" disabled="">ASPIRANTE</option>
                                           <option value="2">PERSONAL</option>
                                           <option value="3">EGRESADO</option>
                                       </select>
                                   </div>
                                   <div class="col-md-3 col-sm-3 col-xs-3">
                                       <label for="tiempo_contracto">Tiempo de Contrato *</label>
                                       <input id="tiempo_contracto" name="tiempo_contracto" class="form-control reset" type="text" data-validate='required|max(2)|min(1)' data-type="integer">
                                   </div>
                                   <div class="col-md-3 col-sm-3 col-xs-3">
                                       <label for="telefono">Teléfono *</label>
                                       <input id="telefono" name="telefono" class="form-control telefono reset" type="text" data-validate='required|max(16)|min(16)'>
                                   </div>
                                   <div class="col-md-3 col-sm-3 col-xs-3">
                                       <label for="correo">Correo *</label>
                                       <input id="correo" name="correo" class="form-control reset" type="text" data-validate='required|email' data-text="lower">
                                   </div>
                               </div>
                               <?php echo form_close();?>
                           </div>
                       </div>
                   </div>

                   <div class="form-group" style="margin-top: 4%;text-align: left;">
                       <table>
                           <tr>
                               <td>
                                   <button type="button" class="btn btn-info" data-toggle="modal" data-target="#referenciaModal">
                                       Referencia Personal
                                   </button>
                               </td>
                               <td>
                                   <button type="button" class="btn btn-info" data-toggle="modal" data-target="#habilidadModal">
                                       Habilidades y Actitudes
                                   </button>
                               </td>
                               <td>
                                   <button type="button" class="btn btn-info" data-toggle="modal" data-target="#experienciaModal">
                                       Experiencia Laboral
                                   </button>
                               </td>
                               <td>
                                   <button type="button" class="btn btn-info" data-toggle="modal" data-target="#educacionModal">
                                       Educación
                                   </button>
                               </td>
                               <td>
                                   <button type="button" class="btn btn-info" data-toggle="modal" data-target="#cursosModal">
                                       Cursos
                                   </button>
                               </td>
                               <td>
                                   <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                               </td>
                               <td>
                                   <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                               </td>
                           </tr>
                       </table>
                   </div>
               </div>
           </div>
           <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
           <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
           <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Registro de Tarifa de Impuesto(s)</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmimpuestotice');
                echo form_open('registro/impuestotice/guardar', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <div class="form-group">
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <label for="impuesto">Impuesto * </label>
                        <select name="impuesto_id" id="impuesto_id" class="form-control" data-add="id" data-mod="1">
                            <option value="0">Seleccione</option>
                            <?php foreach ($impuesto as $value) { ?>
                            <option value="<?php echo $value->id?>"><?php echo $value->impuesto?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label for="codigo">Código *</label>
                        <input id="codigo" placeholder="Código del impuesto" name="codigo" class="form-control" type="text" data-validate='required|max(3)|min(1)' data-type="numeric" data-add="id" data-mod="1">
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label for="porcentaje">Porcentaje *</label>
                        <input id="porcentaje" placeholder="Porcentaje" name="porcentaje" class="form-control" type="text" data-type="float" data-validate='required|max(5)|min(2)'>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="grupo">Grupo *</label>
                        <input id="grupo" readonly="" placeholder="Nombre de la lista de Grupo" name="grupo" class="form-control" type="text" data-type="alphanumeric" data-validate='max(20)|min(5)' data-text="upper">
                    </div>
                </div>

                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table id="tbl_impuesto" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Código</th>
                                    <th class="column-title">Impuesto</th>
                                    <th class="column-title">Porcentaje</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->codigo?></td>
                                        <td><?php echo $lista->impuesto?></td>
                                        <td><?php echo $lista->porcentaje?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo assets_url('script/impuesto_tarifa_ice.js'); ?>" type="text/javascript" charset="utf-8" ></script>


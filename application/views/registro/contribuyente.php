<style type="text/css">
    .span{
        color:red;font-weight:bold;
    }
    .wizard a {
        padding: 10px 12px 10px;
        margin-right: 5px;
        background: #efefef;
        position: relative;
        display: inline-block;
    }
    .wizard a:before {
        width: 0;
        height: 0;
        border-top: 20px inset transparent;
        border-bottom: 20px inset transparent;
        border-left: 20px solid #fff;
        position: absolute;
        content: "";
        top: 0;
        left: 0;
    }
    .wizard a:after {
        width: 0;
        height: 0;
        border-top: 20px inset transparent;
        border-bottom: 20px inset transparent;
        border-left: 20px solid #efefef;
        position: absolute;
        content: "";
        top: 0;
        right: -20px;
        z-index: 2;
    }
    .wizard a:first-child:before,
    .wizard a:last-child:after {
        border: none;
    }
    .wizard a:first-child {
        -webkit-border-radius: 4px 0 0 4px;
        -moz-border-radius: 4px 0 0 4px;
        border-radius: 4px 0 0 4px;
    }
    .wizard a:last-child {
        -webkit-border-radius: 0 4px 4px 0;
        -moz-border-radius: 0 4px 4px 0;
        border-radius: 0 4px 4px 0;
    }
    .wizard .badge {
        margin: 0 5px 0 18px;
        position: relative;
        top: -1px;
    }
    .wizard a:first-child .badge {
        margin-left: 0;
    }
    .wizard .current {
        background: #007ACC;
        color: #fff;
    }
    .wizard .current:after {
        border-left-color: #007ACC;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="wizard">
                    <?php foreach ($estado as $value) { ?>
                    <?php if($value->id == 1){?>
                    <a class="current" id="wizard_<?php echo $value->id?>"><span class="badge"><?php echo $value->id?></span><?php echo $value->comprobante_electronico?></a>
                    <?php }else{?>
                    <a><span class="badge" id="wizard_<?php echo $value->id?>"><?php echo $value->id?></span> <?php echo $value->comprobante_electronico?></a>
                    <?php }?>
                    <?php }?>
                </div>
                <div id="exTab2" class="container" style="margin-top: 1%;">
                    <ul class="nav nav-tabs">
                        <li id="li_factura">
                            <a  href="#div_factura" data-toggle="tab">Factura Electrónica</a>
                        </li>
                    </ul>
                    <?php
                    $attributes = array('id' => 'frmcontribuyente', 'class' => 'formulario');
                    echo form_open('registro/contribuyente/guardar', $attributes);
                    ?>
                    <?php echo form_hidden('token', $token) ?>
                    <div class="tab-content">
                        <!-- Datos de la Factura -->
                        <div class="tab-pane active" id="div_factura">
                            <input type="hidden" name="id" value="<?php echo $id?>" id="id" class='lastId'>
                            <input type='hidden' name='estado_id' value='1'/>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label for="comprobante_id">Comprobante *</label>
                                    <select id="comprobante_id" name='comprobante_id' class="form-control" style="width:100%;" data-validate='required'>
                                        <option value="0">Seleccione</option>
                                        <?php foreach ($comprobante as $value) { ?>
                                        <?php if($value->id == 1){?>
                                        <option selected="" title="<?php echo $value->descripcion?>" value="<?php echo $value->id?>"><?php echo $value->nom_comprobante?></option>
                                        <?php }?>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label for="forma_pago_id">Forma de Pago *</label>
                                    <select id="forma_pago_id" name='forma_pago_id' class="form-control" style="width:100%;" data-validate='required'>
                                        <option value="0">Seleccione</option>
                                        <?php foreach ($formapago as $value) { ?>
                                        <option value="<?php echo $value->id?>"><?php echo $value->forma_pago; ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <label for="agente_id">Agente de Retención *</label>
                                    <select id="agente_id" name='agente_id' class="form-control" style="width:100%;" data-validate='required'>
                                        <option value="0">Seleccione</option>
                                        <?php foreach ($agente as $value) { ?>
                                        <option value="<?php echo $value->id?>"><?php echo $value->num_ruc." (".$value->razon_social.") "?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <label for="cliente_id">Cliente *</label>
                                    <select id="cliente_id" name='cliente_id' class="form-control" style="width:100%;" data-validate='required'>
                                        <option value="0">Seleccione</option>
                                        <?php foreach ($cliente as $value) { ?>
                                        <option value="<?php echo $value->id?>"><?php echo $value->razon_social."-".$value->identificacion?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <label for="num_ruc">Número de Registro *</label>
                                    <input id="num_ruc" readonly='' placeholder="Número de Registro Unico del Contribuyente" name="num_ruc" class="form-control reset" type="text" data-validate='required|max(13)|min(13)' data-add="id" data-mod="1" data-type="alphanumeric" data-text="upper"  style="width:100%;">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <label for="cod_auxiliar">Ambiente *</label>
                                    <select name="ambiente_id" id="ambiente_id" class="form-control"  style="width:100%;" data-add="id" data-mod="1" data-validate='required'>
                                        <option value="0">Seleccione</option>
                                        <?php foreach ($ambiente as $value) { ?>
                                        <option selected value="<?php echo $value->id?>"><?php echo $value->nom_ambiente?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <label for="num_serie">Número de serie *</label>
                                    <input id="num_serie" readonly="" value="<?php echo "001".$num_serie?>" placeholder="Número de serie" name="num_serie" class="form-control reset" type="text" data-validate='required|max(6)|min(6)' data-type="alphanumeric" data-text="upper" style="width:100%;">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <label for="num_comprobante">Número de comprobante *</label>
                                    <input id="num_comprobante" readonly="" value="<?php echo $num_comprobante?>" placeholder="Número de comprobante secuencial" name="num_comprobante" class="form-control reset" type="text" data-validate='required|max(9)|min(9)' data-type="alphanumeric" data-text="upper"  style="width:100%;">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <label for="cod_numerico">Código numérico *</label>
                                    <input id="cod_numerico" readonly="" value='<?php echo $cod_numerico;?>' placeholder="Número de comprobante secuencial" name="cod_numerico" class="form-control reset" type="text" data-validate='required|max(8)|min(8)' data-type="alphanumeric" data-text="upper"  style="width:100%;">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <label for="emision_id">Emisión *</label>
                                    <select name="emision_id" id="emision_id" class="form-control" data-validate='required' style="width:100%;">
                                        <option value="0">Seleccione</option>
                                        <?php foreach ($emision as $value) { ?>
                                        <option selected value="<?php echo $value->id?>"><?php echo $value->nom_emision ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <label for="clave_acceso_id">Clave Acceso *</label>
                                    <select name="clave_acceso_id" id="clave_acceso_id" class="form-control" data-validate='required'  style="width:100%;">
                                        <option value="0">Seleccione</option>
                                        <?php foreach ($clave_acceso as $value) { ?>
                                        <option value="<?php echo $value->id?>"><?php echo $value->clave_acceso ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label for="producto_id">Producto * </label>
                                        <select name="producto_id" id="producto_id" class="form-control" style="width:100%;">
                                            <option value="0">Seleccione</option>
                                            <?php foreach ($producto as $value) { ?>
                                            <option value="<?php echo $value->id?>"><?php echo $value->nom_producto ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div style="width:100% !important;">
                                    <table id="tblproducto" style="width:100%" data-fields='0' data-counter="0" data-column='0' class="table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                                        <thead>
                                            <tr>
                                                <th>Nº</th>
                                                <th>Producto</th>
                                                <th style='text-align:right;'>Precio Unitario</th>
                                                <th style='text-align:right;'>Descuento</th>
                                                <th style='text-align:right;'>Cantidad</th>
                                                <th style='text-align:right;'>Precio Total</th>
                                                <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <!--<tr>
                                                <td colspan='4'></td>
                                                <td style='font-weight:bold;'>Total</td>
                                                <td style='text-align:right;'></td>
                                            </tr>-->
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>SUBTOTAL <span class="iva_vigente">0</span>%</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="iva_vigente" id="iva_vigente" value="0.00">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>SUBTOTAL 0%</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="iva_cero" id="iva_cero" value="0.00">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>SUBTOTAL No objeto de IVA</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="objeto_iva" id="objeto_iva" value="0.00">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>SUBTOTAL Exento de IVA</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="exento_iva" id="exento_iva" value="0.00">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>SUBTOTAL SIN IMPUESTOS</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="sin_impuesto" id="sin_impuesto" value="0.00">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>ICE</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="ice" id="ice" value="0.00">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>IVA 0%</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="valor_vigente" id="valor_vigente" value="0.00">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>IRBPNR</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="irbpnr" id="irbpnr" value="0.00">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>Propina</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="propina" id="propina" value="0.00">
                                                </td>
                                            </tr>
                                            <!--<tr>
                                                <td colspan='4'></td>
                                                <td>Descuento Solidario 2% IVA</td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="descuento_solidario" id="descuento_solidario">
                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>Total Descuento:</td>
                                                <td colspan='2' style='text-align:right;'>
                                                <input type="text" name="descuento" id="descuento"  value="0.00">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='4'></td>
                                                <td>VALOR A PAGAR </td>
                                                <td colspan='2' style='text-align:right;'>
                                                    <input type="text" name="valor_total" id="valor_total" value="0.00">
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6 col-xs-push-6" style="text-align: center">
                    <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                    <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    <button type="button" class="btn btn-primary btn-validar-clave-acceso">Validar</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
<input type="hidden" name="file" id="file" value="<?php echo $file;?>">
<script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>
<!--<script>
        flatpickr("#fecha_inicio_transporte,#fecha_fin_transporte", {
            utc: true,
            dateFormat: 'd-m-Y H:i:s',
            enableTime: true,
        });
    </script>-->
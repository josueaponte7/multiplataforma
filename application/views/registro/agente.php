
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmagente', 'enctype' => 'multipart/form-data' , 'class' => 'formulario');
                echo form_open('registro/agente/guardar', $attributes);
                ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="hora_servicio">Hora / Servicio *</label>
                        <input id="hora_servicio" type="text" name="hora_servicio" class="form-control" />
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="num_ruc">RUC * </label>
                        <input id="num_ruc" placeholder="RUC" name="num_ruc" class="form-control reset" type="text" data-validate='required|max(13)|min(13)' data-type="integer" data-add="id" data-mod="1">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label for="razon_social">Razón Social / Nombres o Apellidos * </label>
                        <input id="razon_social" placeholder="Razón Social / Nombres o Apellidos" name="razon_social" class="form-control reset" type="text" data-validate='max(300)|min(5)' data-type="alphanumeric" data-text="upper" data-add="id" data-mod="1">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <label for="nom_comercial">Nombre Comercial * </label>
                        <input id="nom_comercial" placeholder="Nombre Comercial" name="nom_comercial" class="form-control reset" type="text" data-validate='required|max(300)|min(5)' data-type="alphanumeric" data-text="upper">
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="pais_id">Pais *</label>
                        <select name="pais_id" id="pais_id" class="form-control reset" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($pais as $value) { ?>
                                <option value="<?php echo $value->id?>"><?php echo $value->pais?></option>
                            <?php }?>
                            option
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label for="directorio_matriz">Dirección del Establecimiento Matriz * </label>
                        <textarea id="directorio_matriz" placeholder="Dirección del Establecimiento Matriz" name="directorio_matriz" class="form-control reset" type="text" data-validate='required|max(300)|min(5)' data-text="upper"></textarea>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label for="directorio_emisor">Dirección del Establecimiento Emisor * </label>
                        <textarea id="directorio_emisor" placeholder="Dirección del Establecimiento Emisor" name="directorio_emisor" class="form-control reset" type="text" data-validate='required|max(300)|min(5)' data-text="upper"></textarea>
                    </div>
                </div>
               <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="cod_establecimiento_emisor">Código Establecimiento * </label>
                        <input id="cod_establecimiento_emisor" placeholder="Código del Establecimiento" name="cod_establecimiento_emisor" class="form-control reset" type="text" data-validate='required|max(3)' data-type="integer">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="cod_emision">Código Punto de Emisión * </label>
                        <input id="cod_emision" placeholder="Código Punto de Emisión" name="cod_emision" class="form-control reset" type="text" data-validate='required|max(3)' data-type="integer">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="contribuyente">Número de Resolución * </label>
                        <input id="contribuyente" placeholder="Número de Resolución" name="contribuyente" class="form-control reset" type="text" data-validate='required|max(5)' data-type="integer">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="contabilidad">Contabilidad * </label>
                        <select id='contabilidad' name='contabilidad' class='form-control' data-validate='required'>
                            <option value='0'>Seleccione</option>
                            <option value='1'>Si</option>
                            <option value='2'>No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label for="tipo_emision_id">Tipo de Emisión *</label>
                        <select name="tipo_emision_id" id="tipo_emision_id" class="form-control reset" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($emision as $value) { ?>
                                <option selected value="<?php echo $value->id?>"><?php echo $value->nom_emision?></option>
                            <?php }?>
                            option
                        </select>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label for="tipo_ambiente_id">Tipo de Ambiente *</label>
                        <select name="tipo_ambiente_id" id="tipo_ambiente_id" class="form-control reset" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($ambiente as $value) { ?>
                                <option value="<?php echo $value->id?>"><?php echo $value->nom_ambiente?></option>
                            <?php }?>
                            option
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="logo_emisor">Logo(Imagen) *</label>
                    <input type='file' id='logo_emisor' name='logo_emisor' class='form-control'/>
                </div>
                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table id="tbl_cliente" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">RUC</th>
                                    <th class="column-title">Razón Social / Nombres o Apellidos</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->num_ruc?></td>
                                        <td><?php echo $lista->razon_social?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
    <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
    <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>
    <script>
        flatpickr("#hora_servicio", {
            utc: true,
            dateFormat: 'd-m-Y H:i:s',
            enableTime: true,
        });
    </script>


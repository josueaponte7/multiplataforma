<style type="text/css">
	.formato{
		text-align: right;
	}
	.bold{
		font-weight: bold;
	}
</style>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?php echo $titulo;?></h2>
				<div class="clearfix"></div>
			</div>
			<div class="alert alert-info">
				<span style="color:#000000;font-weight: bold;">
					<?php echo $nom;?>
					<span class="fa fa-money"></span>
				</span>
			</div>
			<table style="width:100%">
				<tr>
					<td class="bold">Cuenta </td>
					<td><?php if($lista[0]->tipo_cuenta == 1){ echo "AHORRO";}else{ echo "CORRIENTE";}?></td>
					<td class="bold">Banco</td>
					<td><?php echo $lista[0]->banco;?></td>
				</tr>
				<tr>
					<td class="bold">Número de Cuenta </td>
					<td><?php echo $lista[0]->num_cuenta;?></td>
					<td class="bold">Disponibilidad</td>
					<td>
						<span class="label label-success">
							<?php echo $this->transaccion->format_number($lista[0]->disponibilidad);?>
						</span>
					</td>
				</tr>
			</table>
			<div class="x_content jumbotron">
				<br/>
				<div class="ln_solid"></div>
				<div class="col-xs-12">
					<div class="table-responsive botr">
						<table style="width: 100%;" id="tbl_impuesto" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
							<thead>
								<tr>
									<th class="column-title">#</th>
									<th class="column-title">Fecha / Hora Emisión</th>
									<th class="column-title">Porcentaje de Descuento</th>
									<th class="column-title">Monto</th>
									<th class="column-title">Sub Total</th>
									<th class="column-title">Total</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i = 1;
								foreach ($lista as $lista) {
									?>
									<tr id="<?php echo $lista->id?>">
										<td><?php echo $i?></td>
										<td>
											<?php
											$cadena = explode(' ', $lista->fecha_hora_emision);
											$fecha = date('d/m/Y', strtotime($cadena[0]));
											$fecha_hora_emision = $fecha." ".$cadena[1];
											echo $fecha_hora_emision;
											?></td>
											<td><?php echo $lista->porcentaje."%"?></td>
											<td class="formato"><?php echo $this->transaccion->format_number($lista->monto);?></td>
											<td class="formato"><?php echo $this->transaccion->format_number($lista->sub_total);?></td>
											<td class="formato">
												<span class="label label-success"><?php echo $this->transaccion->format_number($lista->total);?></span>	
											</td>
										</tr>
										<?php
										$i++;
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
		<input type="hidden" name="file" id="file" value="<?php echo $file;?>">
		<script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


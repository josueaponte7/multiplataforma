
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmexperiencia', 'class' => 'formulario', 'enctype' => 'multipart/form-data');
                echo form_open('registro/personal/guardar_xperiencia', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <?php echo form_hidden('aspirante_id', $pk) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="universidad">Universidad *</label>
                        <input id="universidad" name="universidad" class="form-control reset" type="text" data-validate='required|max(255)|min(5)' data-text="upper">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="titulacion">Título obtenenido *</label>
                        <input id="titulacion" name="titulacion" class="form-control reset" type="text" data-validate='required|max(255)|min(5)' data-text="upper">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="ano">Año *</label>
                        <input id="ano" name="ano" class="form-control reset" type="text" data-type="integer" data-validate='required|max(4)'>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="desde">Periódo desde *</label>
                        <input id="desde" name="desde" class="form-control reset" type="text" data-validate='required|max(255)|min(5)'>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="hasta">Periódo hasta *</label>
                        <input id="hasta" name="hasta" class="form-control reset" type="text" data-validate='required|max(255)|min(5)'>
                    </div>
                </div>
                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                        <a href='<?php echo base_url("registro/aspirante");?>'>
                            <input type="button" class="btn btn-default" id="volver" value="Volver"/>
                        </a>
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table style="width: 100%;" id="tbl_impuesto" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Universidad</th>
                                    <th class="column-title">Titulación</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->universidad?></td>
                                        <td><?php echo $lista->titulacion?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
    <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
    <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


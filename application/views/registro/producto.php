
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Registro de Producto(s)</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmproducto');
                echo form_open('registro/producto/guardar', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <div class="form-group">
                    <fieldset>
                        <legend>Datos Principales</legend>
                    </fieldset>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="impuesto_tarifa">Impuesto de la Tarifa *</label>
                        <input id="impuesto_tarifa" placeholder="Tasa que se impone" name="impuesto_tarifa" class="form-control reset" type="text" data-validate='required|max(3)|min(2)' data-type="alphanumeric" data-add="id" data-mod="1" data-text="upper">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="cod_identificador">Código de Identificación *</label>
                        <input id="cod_identificador" placeholder="Código identificador del producto" name="cod_identificador" class="form-control reset" type="text" data-validate='required|max(15)|min(10)' data-add="id" data-mod="1" data-type="numeric"  data-text="upper">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="cod_principal">Código Principal *</label>
                        <input id="cod_principal" placeholder="Código Principal" name="cod_principal" class="form-control reset" type="text" data-validate='max(3)|min(2)' data-type="alphanumeric"  data-text="upper">
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label for="cod_auxiliar">Código Auxiliar *</label>
                        <input id="cod_auxiliar" placeholder="Código identificador auxiliar" name="cod_auxiliar" class="form-control reset" type="text" data-validate='max(3)|min(2)' data-type="alphanumeric"  data-text="upper">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="nom_producto">Nombre del Producto *</label>
                        <input id="nom_producto" placeholder="Nombre del Producto o Servicio" name="nom_producto" class="form-control reset" type="text" data-validate='required|max(30)|min(10)' data-type="alphanumeric" data-text="upper">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="descripcion">Descripción del Producto *</label>
                        <input id="descripcion" placeholder="Breve descripción del producto" name="descripcion" class="form-control reset" type="text" data-validate='max(255)|min(10)' data-type="alphanumeric" data-text="upper">
                    </div>
                </div>
                <fieldset>
                    <legend><br/>Característica(s) Aplicadas</legend>
                </fieldset>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="validador">Categoria *</label>
                        <select name="validador" id="validador" class="form-control" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <option value="1">IVA Vigente</option>
                            <option value="2">IVA 0%</option>
                            <option value="3">No Objeto de IVA</option>
                            <option value="4">Exento de IVA</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="impuesto_id">Impuesto *</label>
                        <select name="impuesto_id" id="impuesto_id" class="form-control">
                            <option value="0">Seleccione</option>
                            <?php foreach ($impuesto as $value) { ?>
                            <option value="<?php echo $value->id?>"><?php echo $value->impuesto." Porcentaje (".$value->porcentaje. ") "?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label for="valor_unitario">Valor Unitario *</label>
                        <input id="valor_unitario" placeholder="Valor Unitario" name="valor_unitario" class="form-control reset" type="text" data-validate='required|max(10)|min(3)' data-type="numeric">
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label for="descuento">Descuento *</label>
                        <input id="descuento" placeholder="Descuento" name="descuento" class="form-control reset" type="text" data-validate='max(3)|min(1)' data-type="numeric" value="0.00">
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label for="tarifa_impuesto">Tarifa *</label>
                        <input id="tarifa_impuesto" placeholder="Tarifa" name="tarifa_impuesto" class="form-control reset" type="text" data-validate='required|max(10)|min(3)' data-type="numeric">
                    </div>
                </div>
                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table id="tbl_proveedor" data-counter="2" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Código de Indentificación</th>
                                    <th class="column-title">Nombre del Producto</th>
                                    <th class="column-title">Precio</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->cod_identificador?></td>
                                        <td><?php echo $lista->nom_producto?></td>
                                        <td><?php echo $lista->valor_unitario?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo assets_url('script/producto.js'); ?>" type="text/javascript" charset="utf-8" ></script>


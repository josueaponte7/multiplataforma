
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmhabilidades', 'class' => 'formulario', 'enctype' => 'multipart/form-data');
                echo form_open('registro/personal/guardar_habilidad', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <?php echo form_hidden('aspirante_id', $pk) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label for="habilidad">Habilidades *</label>
                        <textarea id="habilidad" name="habilidad" class="form-control reset" type="text" data-validate='required|max(250)|min(5)' data-text="upper"></textarea>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label for="actitud">Actitudes *</label>
                        <textarea id="actitud" name="actitud" class="form-control reset" type="text" data-validate='required|max(250)|min(5)' data-text="upper"></textarea>
                    </div>
                </div>
                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                        <a href='<?php echo base_url("registro/aspirante");?>'>
                            <input type="button" class="btn btn-default" id="volver" value="Volver"/>
                        </a>
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table style="width: 100%;" id="tbl_impuesto" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Habilidades</th>
                                    <th class="column-title">Actitudes</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->habilidad?></td>
                                        <td><?php echo $lista->actitud?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
    <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
    <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


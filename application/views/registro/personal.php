
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmimpuesto', 'class' => 'formulario');
                echo form_open('registro/impuesto/guardar', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <div class="form-group">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="estatus">Estátus *</label>
                        <select id="estatus" name='estatus' class="form-control reset" style="width:100%;" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <option value="1" disabled="">ASPIRANTE</option>
                            <option value="2">PERSONAL</option>
                            <option value="3">EGRESADO</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <label for="aspirante_id">Personal *</label>
                        <select id="aspirante_id" name='aspirante_id' class="form-control reset" style="width:100%;" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($aspirante as $value) { ?>
                            <option value="<?php echo $value->id?>"><?php echo $value->p_nombre." ".$value->s_nombre." ".$value->p_apellido." ".$value->s_apellido;?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="cargo_id">Cargo *</label>
                        <select id="cargo_id" name='cargo_id' class="form-control reset" style="width:100%;" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php foreach ($cargo as $value) { ?>
                            <option value="<?php echo $value->id?>"><?php echo $value->nombre;?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-5 col-sm-5 col-xs-5">
                        <label for="fecha_ingreso">Fecha de Ingreso *</label>
                        <input id="fecha_ingreso" name="fecha_ingreso" class="form-control reset" type="text" data-validate='required|max(10)|min(10)' data-type="date" data-add="id" data-mod="1">
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-5">
                        <label for="fecha_egreso">Fecha de Egreso *</label>
                        <input id="fecha_egreso" name="fecha_egreso" class="form-control reset" type="text" data-validate='max(10)|min(10)' data-type="date" data-add="id" data-mod="1">
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label for="salario">Sueldo *</label>
                        <input id="salario" name="salario" class="form-control reset" type="text" data-validate='required|max(10)|min(4)' data-type="integer" data-add="id" data-mod="1">
                    </div>
                </div>
                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table style="width: 100%;" id="tbl_impuesto" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Fecha de Ingreso</th>
                                    <th class="column-title">Nombres y Apellidos</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo date('d/m/Y', strtotime($lista->fecha_ingreso));?></td>
                                        <td><?php echo $lista->nombres?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
    <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
    <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


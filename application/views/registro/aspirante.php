
<style type="text/css">
    #image-preview {
        width: 130px;
        height: 180px;
        position: relative;
        overflow: hidden;
        color: #BD5800;
        font-weight: bold;
        margin-left: 11px;
        cursor: pointer;
        background-color: rgba(196,196,196, 0.8);
        /*background-image: url(assets_url("images/default_avatar_male.jpg"));*/
    }
    #image-preview input {
        line-height: 200px;
        font-size: 200px;
        position: absolute;
        opacity: 0;
        cursor: pointer;
        z-index: 10;
        margin-left:11px;
    }
    #image-preview label {
        position: absolute;
        z-index: 5;
        opacity: 0.8;
        cursor: pointer;
        background-color: #bdc3c7;
        width: 200px;
        height: 25px;
        cursor: pointer;
        font-size: 15px;
        line-height: 25px;
        text-transform: uppercase;
        top: 25;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        text-align: center;
    }
</style>
<div class="row">
    <button type="button" class="btn btn-info nuevo-aspirante">Nuevo</button>
    <div class="col-md-12 col-sm-12 col-xs-12" id='content_asp' style="display: none;">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="container">
                <br />

                <div class="tab-content">
                    <!-- Datos de la Factura -->
                    <div class="tab-pane active">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <fieldset>
                                    <legend>Datos Principales</legend>
                                </fieldset>
                            </div>
                        </div>
                        <?php
                        $attributes = array('id' => 'frmaspirante', 'class' => 'formulario', 'enctype' => 'multipart/form-data');
                        echo form_open('registro/aspirante/guardar', $attributes);
                        ?>
                        <?php echo form_hidden('token', $token) ?>
                        <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                        <div class="form-group">
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <div id="image-preview">
                                    <label for="foto_perfil" id="image-label"> Perfil</label>
                                    <input type="file" name="foto_perfil" id="foto_perfil" />
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <label for="p_nombre"> Primer Nombre * </label>
                                <input id="p_nombre" name="p_nombre" class="form-control reset" type="text" data-validate='required|max(30)|min(4)' data-type="alpha" data-text="upper">
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <label for="s_nombre"> Segundo Nombre * </label>
                                <input id="s_nombre" name="s_nombre" class="form-control reset" type="text" data-validate='max(30)|min(4)' data-type="alpha" data-text="upper">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <label for="p_apellido"> Primer Apellido * </label>
                                <input id="p_apellido" name="p_apellido" class="form-control reset" type="text" data-validate='required|max(30)|min(4)' data-type="alpha" data-text="upper">
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <label for="s_apellido"> Segundo Apellido * </label>
                                <input id="s_apellido" name="s_apellido" class="form-control reset" type="text" data-validate='max(30)|min(4)' data-type="alpha" data-text="upper">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <label for="nacionalidad"> Nacionalidad * </label>
                                <select name="nacionalidad" id="nacionalidad" class="form-control reset" data-validate='required'>
                                    <option value="0">Seleccione</option>
                                    <option value="1">ECUATORIANO</option>
                                    <option value="2">EXTRANJERO</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="cedula"> Cédula * </label>
                                <input id="cedula" name="cedula" class="form-control reset" type="text" data-validate='required|max(10)|min(8)' data-type="integer">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="fecha_nac"> Fecha / Nacimiento *</label>
                                <input id="fecha_nac" name="fecha_nac" class="form-control reset" type="text" data-validate='required|max(10)|min(10)' data-type="date">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <label for="edad"> Edad *</label>
                                <input id="edad" readonly="" name="edad" class="form-control reset" type="text" data-validate='required|max(2)|min(2)' data-type="integer" data-text="upper">
                            </div>
                        </div>
                        <div class='hide_contenido'>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label for="lugar_nac"> Lugar de Nacimiento *</label>
                                    <input id="lugar_nac" name="lugar_nac" class="form-control reset" type="text" data-validate='required|max(255)|min(5)' data-type="alphanumeric" data-text="upper">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <label for="genero_id"> Género *</label>
                                    <select name="genero_id" id="genero_id" class="form-control reset" data-validate='required' style="width:100%;">
                                        <option value="0">Seleccione</option>
                                        <?php foreach ($genero as $value) { ?>
                                        <option value="<?php echo $value->id; ?>"><?php echo $value->genero; ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <label for="estado_civil_id"> Estado Civil *</label>
                                    <select name="estado_civil_id" id="estado_civil_id" class="form-control reset" data-validate='required' style="width:100%;">
                                        <option value="0">Seleccione</option>
                                        <?php foreach ($estadocivil as $value) { ?>
                                        <option value="<?php echo $value->id; ?>"><?php echo $value->descripcion; ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%;">
                                    <fieldset>
                                        <legend>Información Adicional</legend>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="estatus"> Estátus *</label>
                                <select name="estatus" id="estatus" class="form-control reset" data-validate='required' style="width:100%;">
                                    <option value="0">Seleccione</option>
                                    <option value="1" selected="">ASPIRANTE</option>
                                    <option value="2" disabled="">PERSONAL</option>
                                    <option value="3" disabled="">EGRESADO</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="tiempo_contracto"> Tiempo de Contrato *</label>
                                <input id="tiempo_contracto" name="tiempo_contracto" class="form-control reset" type="text" data-validate='required|max(2)|min(1)' data-type="integer">
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="telefono"> Teléfono *</label>
                                <input id="telefono" name="telefono" class="form-control telefono reset" type="text" data-validate='required|max(16)|min(16)'>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="correo"> Correo *</label>
                                <input id="correo" name="correo" class="form-control reset" type="text" data-validate='required|email' data-text="lower">
                            </div>
                        </div>
                        <?php echo form_close();?>
                    </div>
                    <!-- Formulario de ingreso de direcciones -->

                    <?php
                    $attributes = array('id' => 'frmaspirante', 'class' => 'formulario', 'enctype' => 'multipart/form-data');
                    echo form_open('registro/aspirante/guardar', $attributes);
                    ?>
                    <?php echo form_hidden('token', $token) ?>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="continente_id"> Continente * </label>
                            <select name="continente_id" id="continente_id" class="form-control reset topologia" data-table="mp_pais" data-validate='required' style="width:100%;">
                                <option value="0">Seleccione</option>
                                <?php foreach ($continente as $value) { ?>
                                <option value="<?php echo $value->id; ?>"><?php echo $value->continente; ?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="pais_id"> Pais * </label>
                            <select name="pais_id" id="pais_id" class="form-control reset topologia" data-table="mp_region" data-validate='required' style="width:100%;">
                                <option value="0">Seleccione</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="region_id"> Región * </label>
                            <select name="region_id" id="region_id" class="form-control reset topologia" data-table="mp_provincia" data-validate='required' style="width:100%;">
                                <option value="0">Seleccione</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="provincia_id"> Provincia * </label>
                            <select name="provincia_id" id="provincia_id" class="form-control reset topologia" data-table="mp_canton" data-validate='required' style="width:100%;">
                                <option value="0">Seleccione</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="canton_id"> Cantón * </label>
                            <select name="canton_id" id="canton_id" class="form-control reset topologia" data-table="mp_parroquia" data-validate='required' style="width:100%;">
                                <option value="0">Seleccione</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="parroquia_id"> Parroquia * </label>
                            <select name="parroquia_id" id="parroquia_id" class="form-control reset topologia" data-validate='required' style="width:100%;">
                                <option value="0">Seleccione</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="sector_id"> Sector * </label>
                            <select name="sector_id" id="sector_id" class="form-control reset"  data-validate='required' style="width:100%;">
                                <option value="0">Seleccione</option>
                            </select>
                        </div>
                        <?php echo form_close();?>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="direccion"> Dirección * </label>
                            <textarea name="direccion" id="direccion" class="form-control reset" type="text" data-validate='required|max(250)|min(5)' data-type="alphanumeric" data-text="upper"></textarea>
                        </div>
                    </div>
                    <!-- Fin Formulario de ingreso de direcciones -->
                </div>
            </div>
            <div class="form-group" style="margin-top: 4%;text-align: left;">
                <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
            </div>
        </div>
    </div>
    <br/>
    <div class="ln_solid"></div>
    <div class="col-xs-12">
        <div class="table-responsive table-asp">
            <table id="tbl_proveedor" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                <thead>
                    <tr>
                        <th class="column-title">#</th>
                        <th class="column-title">Roto</th>
                        <th class="column-title">Nombres y Apellidos</th>
                        <th class="column-title">Edad</th>
                        <th class="column-title">Modificar</th>
                        <th class="column-title">Perfil</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($lista as $lista) {
                        ?>
                        <tr id="<?php echo $lista->id?>">
                            <td><?php echo $i?></td>
                            <td><img src="<?php echo assets_url("fotos/".$lista->foto_perfil)?>" style='width: 70px;height: 70px;'></td>
                            <td><?php echo $lista->p_nombre." ".$lista->s_nombre." ".$lista->p_apellido." ".$lista->s_apellido;?></td>
                            <td><?php echo $lista->edad?> Años</td>
                            <td>
                                <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                            </td>
                            <td>
                                <button class="btn btn-warning panel-show" title="Click para desplegar panel de opciones">
                                    <span class="fa fa-plus"></span>
                                </button>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
            <div class="alert alert-info content-panel-element" style="display: none;">
                <div class="panel-valor" hidden=""></div>
                <button title="Click para agregar nueva referencia" class="btn btn-info referencia">Referencia</button>
                <button title="Click para agregar nueva habilidad" class="btn btn-info habilidad">Habilidad</button>
                <button title="Click para agregar nueva experiencia" class="btn btn-info experiencia">Experiencia</button>
                <button title="Click para agregar nueva educación" class="btn btn-info educacion">Educación</button>
                <button title="Click para agregar nueva curso" class="btn btn-info curso">Curso</button>
                <button title="Click para cerrar panel" class="btn btn-default close-panel-element">Cerrar</button>
            </div>
        </div>
    </div>
</div>
</div>
</div>









<input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
<input type="hidden" name="file" id="file" value="<?php echo $file;?>">
<script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


<style type="text/css">
    #image-preview, #image-preview-doc_legal {
        width: 130px;
        height: 130px;
        position: relative;
        overflow: hidden;
        background-color: #ffffff;
        color: #ecf0f1;
        margin-left: 11px;
        background-color: rgba(196,196,196, 0.8);
    }
    #image-preview input, #image-preview-doc_legal input {
        line-height: 200px;
        font-size: 200px;
        position: absolute;
        opacity: 0;
        z-index: 10;
        margin-left:11px;
    }
    #image-preview label, #image-preview-doc_legal label {
        position: absolute;
        z-index: 5;
        opacity: 0.8;
        cursor: pointer;
        background-color: #bdc3c7;
        width: 200px;
        height: 25px;
        font-size: 15px;
        line-height: 25px;
        text-transform: uppercase;
        top: 25;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        text-align: center;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="container">
                <br />
                <?php
                $attributes = array('id' => 'frmempresa', 'class' => 'formulario', 'enctype' => 'multipart/form-data');
                echo form_open('registro/empresa/guardar', $attributes);
                ?>
                <ul class="nav nav-tabs">
                    <li id="li_empresa">
                        <a  href="#div_empresa" data-toggle="tab">Datos de la Empresa</a>
                    </li>
                    <li id="li_representante">
                        <a  href="#div_representante" data-toggle="tab">Datos del Representante</a>
                    </li>
                </ul>
                <?php echo form_hidden('token', $token) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">

                <div class="tab-content">
                    <!-- Datos de la Factura -->
                    <div class="tab-pane active" id="div_empresa">
                        <div class="form-group">
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <label for="estatus">Estátus *</label>
                                <input id="estatus" readonly="" value="Activo" class="form-control reset" type="text">
                                <input name="estatus" class="form-control" value="1" type="hidden">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <label for="ruc">RUC *</label>
                                <input id="ruc" name="ruc" class="form-control reset" type="text" data-validate='required|max(13)|min(13)' data-type="integer" data-text="upper" data-add="id" data-mod="1">
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <label for="razon_social">Razón Social *</label>
                                <input id="razon_social" name="razon_social" class="form-control reset" type="text" data-add="id" data-mod="1" data-validate='required|max(50)|min(5)' data-type="alphanumeric" data-text="upper">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <label for="nom_comercial">Nombre Comercial *</label>
                                <input id="nom_comercial" name="nom_comercial" class="form-control reset" type="text" data-validate='max(50)|min(5)' data-type="alphanumeric" data-text="upper">
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <label for="clase_contribuyente">Clase Contribuyente *</label>
                                <input id="clase_contribuyente" name="clase_contribuyente" class="form-control reset" type="text" data-add="id" data-mod="1" data-validate='required|max(20)|min(5)' data-type="alphanumeric" data-text="upper">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label for="direccion_emp">Dirección *</label>
                                <textarea id="direccion_emp" name="direccion_emp" class="form-control reset" rows="2" data-validate='required|max(255)|min(5)' data-type="alphanumeric" data-text="upper"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="telefono_emp">Teléfono *</label>
                                <input id="telefono_emp" name="telefono_emp" class="form-control reset" data-validate='required|max(16)|min(16)' data-type="phone" type="text">
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <label for="email_emp">Correo Electrónico *</label>
                                <input id="email_emp" name="email_emp" class="form-control reset" type="text" data-validate='required|email' data-text="lower">
                            </div>
                            <div class="form-group">

                                <!--<input type='file' id='logo_emisor' name='logo_emisor' class='form-control'/>
                                <img id="image_upload_preview" src="http://placehold.it/100x100" alt="your image" />-->
                                <img src="" id="hola" alt="">
                                <div id="image-preview">
                                    <label for="logo_emisor" id="image-label">Subir Imagen</label>
                                    <input type="file" name="logo_emisor" id="logo_emisor" />
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="div_representante">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label for="representante_legal">Representante Legal *</label>
                                <input id="representante_legal" name="representante_legal" class="form-control reset" type="text" data-validate='required|max(50)|min(5)' data-type="alphanumeric" data-text="upper" data-add="id" data-mod="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label for="direccion_rep">Dirección *</label>
                                <textarea id="direccion_rep" name="direccion_rep" class="form-control reset" rows="2" data-validate='required|max(255)|min(5)' data-type="alphanumeric" data-text="upper"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="telefono_rep">Teléfono *</label>
                                <input id="telefono_rep" name="telefono_rep" class="form-control reset" data-validate='required|max(16)|min(16)' data-type="phone" type="text">
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <label for="email_rep">Correo Electrónico *</label>
                                <input id="email_rep" name="email_rep" class="form-control reset" type="text" data-validate='required|email' data-text="lower">
                            </div>
                            <div class="form-group">
                            <!--<label for="doc_legal">Documento Legal *</label>
                                <input type='file' id='doc_legal' name='doc_legal' class='form-control file-loading'/> -->

                                <div id="image-preview-doc_legal">
                                    <label for="doc_legal" id="image-label-doc_legal">Subir Imagen</label>
                                    <input type="file" name="doc_legal" id="doc_legal" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close();?>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>

            </div>
            <br/>
            <div class="ln_solid"></div>
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table id="tbl_proveedor" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                        <thead>
                            <tr>
                                <th class="column-title">#</th>
                                <th class="column-title">Logo</th>
                                <th class="column-title">Razón Social</th>
                                <th class="column-title">Dirección Empresa</th>
                                <th class="column-title">Modificar</th>
                                <th class="column-title">Estátus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($lista as $lista) {
                                ?>
                                <tr id="<?php echo $lista->id?>">
                                    <td><?php echo $i?></td>
                                    <td><img src="<?php echo assets_url($lista->logo_emisor)?>" alt=""></td>
                                    <td><?php echo $lista->razon_social?></td>
                                    <td><?php echo $lista->direccion_emp?></td>
                                    <td>
                                        <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                    </td>
                                    <td>
                                        <img class="cursor cambiar" src="<?php echo assets_url('img/ajax-loader.gif') ?>" alt="">
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
<input type="hidden" name="file" id="file" value="<?php echo $file;?>">
<script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>



<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmproveedor', 'class' => 'formulario', 'enctype' => 'multipart/form-data');
                echo form_open('registro/proveedor/guardar', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <div class="form-group">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <label for="proveedor">RUC *</label>
                        <input id="rif" name="rif" class="form-control reset" type="text" data-validate='required|max(13)|min(13)' data-type="alphanumeric" data-text="upper" data-add="id" data-mod="1">
                        <input id='estatus' name='estatus' type='hidden' value="1"/>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <label for="nombre">Nombre *</label>
                        <input id="nombre" name="nombre" class="form-control reset" type="text" data-add="id" data-mod="1" data-validate='required|max(50)|min(5)' data-type="alphanumeric" data-text="upper">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="direccion">Dirección *</label>
                        <textarea id="direccion" name="direccion" class="form-control reset" rows="2" data-validate='required|max(255)|min(5)' data-type="alphanumeric" data-text="upper"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="telefono">Teléfono *</label>
                        <input id="telefono" name="telefono" class="form-control reset" data-validate='required|max(16)|min(16)' data-type="phone" type="text">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <label for="email">Correo Electrónico *</label>
                        <input id="email" name="email" class="form-control reset" type="text" data-validate='required|email' data-text="lower">
                    </div>
                    <div class="form-group">
                        <label for="logo_emisor">Logo(Imagen) *</label>
                        <input type='file' id='logo_emisor' name='logo_emisor' class='form-control'/>
                    </div>
                </div>
                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table id="tbl_proveedor" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Proveedor</th>
                                    <th class="column-title">Dirección</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->nombre?></td>
                                        <td><?php echo $lista->direccion?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
    <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
    <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <div class="form-group">
                    <label for="contribuyente_id">Factura</label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <select id="factura" name="factura" class="form-control" data-validate='required' style="width:50%;">
                            <option value="0">Seleccione</option>
                            <?php foreach ($factura as $value) { ?>
                            <option value="<?php echo $value->id?>"><?php echo str_pad((int) $value->num_comprobante, (int) 9, '0', STR_PAD_LEFT);?></option>
                            <?php }?>
                        </select>
                        <div class="input-group-addon btn btn-primary" id="comprobante_factura" style="color:#FFFFFF;cursor:pointer;">Factura</div>
                        <div class="input-group-addon btn btn-primary" id="comprobante_remision" style="color:#FFFFFF;cursor:pointer;">Remisión</div>
                        <div class="input-group-addon btn btn-primary" id="comprobante_retencion" style="color:#FFFFFF;cursor:pointer;">Retención</div>
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>
    <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
    <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
    <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


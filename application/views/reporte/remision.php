<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//$this->pdf   = new Pdf($orientation = 'L', $unit        = 'mm', $format      = 'A4');

// Agregamos una página
$this->pdf->AddPage();
// set border width
$this->pdf->SetLineWidth(0);
$this->pdf->setCellHeightRatio(1);

/* Se define el titulo, márgenes izquierdo, derecho y
 * el color de relleno predeterminado
 */
$this->pdf->SetLeftMargin(15);
$this->pdf->SetRightMargin(15);
$this->pdf->SetFillColor(56, 119, 119);
$this->pdf->SetLineWidth(1);
// Se define el formato de fuente: Arial, negritas, tamaño 9
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Image(base_url() . "assets/images/logo.png", 15, 15, 70);
$this->pdf->Cell(120, 10, utf8_decode('R.U.C.: '), '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 12);
// Bloque 1
$this->pdf->Cell(60, 10, '1911259889001', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 14);
$this->pdf->Cell(159, 10, 'GUIA DE REMISIÓN', '', 1, 'R', '0');
$this->pdf->SetFont('Arial', 'B', 12);
$this->pdf->Cell(115, 10, 'No.', '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 12);
$this->pdf->Cell(60, 10, '001-001-000000007', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(157, 10, 'NÚMERO DE AUTORIZACIÓN', '', 1, 'R', '0');
$this->pdf->SetFont('Arial', 'B', 7);
$this->pdf->Cell(188, 3, '4545435435345345345345345345345345345345435435444', '', 1, 'R', '0');
$this->pdf->Ln(5);
$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(130, 10, 'AMBIENTE:', '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 10);
$this->pdf->Cell(20, 10, 'PRUEBA', '', 1, 'R', '0');
$this->pdf->Ln(2);
$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(126.5, 10, 'EMISIÓN:', '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 10);
$this->pdf->Cell(20, 10, 'NORMAL', '', 1, 'R', '0');

$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(145.6, 10, 'CLAVE DE ACCESO:', '', 0, 'R', '0');
$this->pdf->Ln(10);


$style = array(
    'position' => '',
    'align' => '',
    'stretch' => true,
    'fitwidth' => false,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'text' => true,
    'label' => '4545435435345345345345345345345345345345435435444',
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
    );

// CODE 39 EXTENDED + CHECKSUM

$this->pdf->SetLineStyle(
    array(
        'width' => 1,
        'cap' => 'butt',
        'join' => 'miter',
        'dash' => 0,
        'color' => array(0, 0, 0),
        'align' => 'L',
        )
    );
$code = "4545435435345345345345345345345345345345435435444";
$this->pdf->SetX(120);                              # Anchura, Altura, Grozor
$this->pdf->write1DBarcode("$code", 'C128A', '', '', 85, 25, 0.4, $style, 'N');

// Bloque 2
$this->pdf->SetFont('Arial', '', 10);
$this->pdf->SetX(1);
$this->pdf->SetY(60);
$this->pdf->Cell(40, 10, 'SERVICIO DE RENTAS BASICAS', '', 1, 'L', '0');
$this->pdf->Cell(40, 20, 'SRI', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(40, 7, 'Dirección', '', 0, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(20, 7, 'SALINAS Y SANTIAGO', '', 1, 'R', '0');
$this->pdf->Cell(40, 5, 'Matriz:', '', 0, 'L', '0');
$this->pdf->Ln(5);
$this->pdf->Cell(40, 7, 'Dirección', '', 0, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(30, 7, 'PAEZ Y RAMIREZ DAVALOS', '', 1, 'R', '0');
$this->pdf->Cell(40, 5, 'Sucursal:', '', 0, 'L', '0');
$this->pdf->Ln(5);
$this->pdf->Cell(55, 7, 'Contribuyente Especial Nro: ', '', 0, 'L', '0');
$this->pdf->Cell(200, 7, '123456', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 8);
$this->pdf->Cell(63, 7, 'OBLIGADO A LLEVAR CONTABILIDAD', '', 0, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(200, 7, "SI", '', 1, 'L', '0');
$this->pdf->Ln(5);
$this->pdf->Cell(63, 5, '(Indentificación Transportistas)', '', 0, 'L', '0');
$this->pdf->Cell(67, 5, '1710707024', '', 1, 'L', '0');
$this->pdf->Cell(63, 5, 'Razón Social / Nombres y Apellidos:', '', 0, 'L', '0');
$this->pdf->Cell(60, 5, 'TRANSPORTISTA', '', 1, 'L', '0');
$this->pdf->Cell(55, 5, 'Placa:', '', 0, 'L', '0');
$this->pdf->Cell(50, 5, 'BWH221:', '', 1, 'L', '0');
$this->pdf->Cell(55, 5, 'Punto de Partida:', '', 0, 'L', '0');
$this->pdf->Cell(50, 5, 'Av. 6 de Diciembre y los cedros', '', 1, 'L', '0');
$this->pdf->Cell(55, 5, 'Fecha inicio Transporte:', '', 0, 'L', '0');
$this->pdf->Cell(50, 5, '15/01/2017', '', 0, 'L', '0');
$this->pdf->Cell(55, 5, 'Fecha fin Transporte:', '', 0, 'L', '0');
$this->pdf->Cell(50, 5, '20/01/2017', '', 1, 'L', '0');
$this->pdf->Ln(2);
$this->pdf->Cell(70, 5, 'Comprobante de Venta:', '', 0, 'L', '0');
$this->pdf->Cell(25, 5, 'Factura:', '', 0, 'L', '0');
$this->pdf->Cell(43, 5, '001-012-2222222222', '', 0, 'L', '0');
$this->pdf->Cell(35, 5, 'Fecha de Emisión:', '', 0, 'L', '0');
$this->pdf->Cell(10, 5, '16/02/2017', '', 1, 'L', '0');
$this->pdf->Ln(2);
$this->pdf->Cell(70, 5, 'Motivo Traslado:', '', 0, 'L', '0');
$this->pdf->Cell(25, 5, 'Envio', '', 1, 'L', '0');
$this->pdf->Cell(70, 5, 'Destino (Punto de llegada):', '', 0, 'L', '0');
$this->pdf->Cell(25, 5, 'Salinas N17', '', 1, 'L', '0');
$this->pdf->Cell(70, 5, 'Indentificación (Destinatario):', '', 0, 'L', '0');
$this->pdf->Cell(25, 5, '1701225556544', '', 1, 'L', '0');
$this->pdf->Cell(70, 5, 'Razón Social / Nombres Apellidos:', '', 0, 'L', '0');
$this->pdf->Cell(25, 5, 'SERVICIOS DE RENTAS INTERNAS', '', 1, 'L', '0');
$this->pdf->Cell(70, 5, 'Documento Aduanero:', '', 0, 'L', '0');
$this->pdf->Cell(25, 5, '123456678544545445', '', 1, 'L', '0');
$this->pdf->Cell(70, 5, 'Código Establecimiento Destino:', '', 0, 'L', '0');
$this->pdf->Cell(25, 5, '001', '', 1, 'L', '0');
$this->pdf->Cell(70, 5, 'Ruta:', '', 0, 'L', '0');
$this->pdf->Cell(25, 5, '', '', 1, 'L', '0');
// Bloque 3 Producto(s)
$this->pdf->Ln(3);
$this->pdf->SetLineStyle(array('width' => 0.0, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0));
$this->pdf->Cell(20, 5, "Cantidad", 1, 0, 'C', 0);
$this->pdf->Cell(110, 5, "Descripción", 1, 0, 'C', 0);
$this->pdf->Cell(27, 5, "Cod Principal", 1, 0, 'C', 0);
$this->pdf->Cell(27, 5, "Cod Auxiliar", 1, 0, 'C', 0);
foreach(range(1,2) as $row){
    $this->pdf->Ln(6);
    $this->pdf->Cell(20, 5, "1", 1, 0, 'C', 0);
    $this->pdf->Cell(110, 5, "Prueba", 1, 0, 'C', 0);
    $this->pdf->Cell(27, 5, "002", 1, 0, 'C', 0);
    $this->pdf->Cell(27, 5, "002", 1, 0, 'C', 0);
}

# Bloque informacion adicional
$this->pdf->Ln(33.5);
$this->pdf->Cell(100, 5, 'Información Adicional', '', 0, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->SetFont('Arial', '', 9);
$this->pdf->Cell(63, 5, 'Dirección: Salinas N17-203 y Santiago', '', 0, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->Cell(63, 5, 'Teléfono: 1700-774-744', '', 0, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->Cell(63, 5, 'Email: ejemplo@sri.gob.ec', '', 0, 'L', '0');



// Salida del Formato PDF
$this->pdf->Output(utf8_decode("Factura Demo"), 'I');


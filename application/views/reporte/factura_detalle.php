<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$num_rows = count($data);
//$this->pdf   = new Pdf($orientation = 'L', $unit        = 'mm', $format      = 'A4');
// Agregamos una página

$this->pdf->AddPage();
$nom = "";
$cedula = "";
if($num_rows > 0){
    $nom = $data[0]->razon_social;
    $cedula = $data[0]->cedula;
}
/* Se define el titulo, márgenes izquierdo, derecho y
 * el color de relleno predeterminado
 */
if($bandera == "true"){
    $cliente = 0;
    $monto = 80;
    $fecha_emision = 90;
}else{
    $cliente = 90;
    $monto = 30;
    $fecha_emision = 50;
}
$this->pdf->SetLeftMargin(15);
$this->pdf->SetRightMargin(15);
$this->pdf->SetFillColor(56, 119, 119);
// Se define el formato de fuente: Arial, negritas, tamaño 9
$this->pdf->SetFont('Arial', 'B', 14);
$this->pdf->SetFillColor(242, 242, 242);
$this->pdf->Cell(180, 10, utf8_decode('DETALLES DE PAGO'), '', 0, 'C', 1);
$this->pdf->SetFont('Arial', '', 9);
if($bandera == "true"){
    $this->pdf->Ln(10);
    $this->pdf->SetFont('Arial', 'B', 8);
    $this->pdf->Cell(18, 5, utf8_decode('CLIENTE: '), '', 0, 'C', '0');
    $this->pdf->SetFont('Arial', '', 9);
    $this->pdf->Cell(25, 5, utf8_decode("$nom"), '', 1, 'C', '0');
    $this->pdf->SetFont('Arial', 'B', 8);
    $this->pdf->Cell(23, 5, utf8_decode('CEDULA: '), '', 0, '', '0');
    $this->pdf->SetFont('Arial', '', 9);
    $this->pdf->Cell(18, 5, utf8_decode("$cedula"), '', 1, 'C', '0');
}

$this->pdf->Ln(10);
$this->pdf->SetFillColor(42, 63, 84);
$this->pdf->SetTextColor(255, 255, 255);
$this->pdf->Cell(10, 10, utf8_decode('#'), '', 0, 'C', 1);
if($bandera == "false"){
    $this->pdf->Cell($cliente, 10, utf8_decode('Nombres y apellidos'), '', 0, 'C', 1);
}
$this->pdf->Cell($monto, 10, utf8_decode('MONTO'), '', 0, 'C', 1);
$this->pdf->Cell($fecha_emision, 10, utf8_decode('FECHA / HORA EMISION'), '', 0, 'C', 1);
$this->pdf->SetTextColor(0, 0, 0);

$i = 1;

if($num_rows > 0){
    foreach ($data as $row) {

        $relleno = '0';
        if ($i % 2 == 0) {
            $relleno = '1';
        }
        if ($i == 23) {
            $this->pdf->AddPage();
            $this->pdf->SetFont('Arial', 'B', 14);
            $this->pdf->SetFillColor(242, 242, 242);
            $this->pdf->Cell(180, 10, utf8_decode('DETALLES DE PAGO'), '', 0, 'C', 1);

            $this->pdf->Ln(10);
            $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(18, 5, utf8_decode('CLIENTE: '), '', 0, 'C', '0');
            $this->pdf->SetFont('Arial', '', 9);
            $this->pdf->Cell(25, 5, utf8_decode("$nom"), '', 1, 'C', '0');
            $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(23, 5, utf8_decode('CEDULA: '), '', 0, '', '0');
            $this->pdf->SetFont('Arial', '', 9);
            $this->pdf->Cell(18, 5, utf8_decode("$cedula"), '', 1, 'C', '0');
            $this->pdf->Ln(10);
            $this->pdf->SetFillColor(42, 63, 84);
            $this->pdf->SetTextColor(255, 255, 255);
            $this->pdf->Cell(10, 10, utf8_decode('#'), '', 0, 'C', 1);
            if($bandera == "false"){
                $this->pdf->Cell($cliente, 10, utf8_decode("$row->razon_social"), '', 0, 'L', $relleno);
            }
            $this->pdf->Cell($monto, 10, $row->monto, '', 0, 'R', 1);
            $this->pdf->Cell($fecha_emision, 10, $row->fecha_hora_emision, '', 0, 'C', 1);
            $this->pdf->SetTextColor(0, 0, 0);
        }
        $this->pdf->Ln(10);
        $this->pdf->SetFillColor(239, 239, 239);
        $this->pdf->Cell(10, 10, "$i", '', 0, 'C', 1);
        if($bandera == "false"){
            $this->pdf->Cell($cliente, 10, utf8_decode("$row->razon_social"), '', 0, 'L', $relleno);
        }
        $this->pdf->Cell($monto, 10, $row->monto, '', 0, 'R', $relleno);
        $cadena = explode(' ', $row->fecha_hora_emision);
        $fecha = date('d/m/Y', strtotime($cadena[0]));
        $fecha_hora_emision = $fecha." ".$cadena[1];
        $this->pdf->Cell($fecha_emision, 10, "$fecha_hora_emision", '', 0, 'C', $relleno);

        $i++;
    }
}else{
    $this->pdf->SetFont('Arial', 'B', 9);
    $this->pdf->SetFillColor(255, 255, 255);
    $this->pdf->Ln(10);
    $this->pdf->Cell(180, 10, "No se encuentran registros", 1, 1, 'C', 1);
}


// Salida PDF
$this->pdf->Output(utf8_decode("Factura Detalles"), 'I');


<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//$this->pdf   = new Pdf($orientation = 'L', $unit        = 'mm', $format      = 'A4');

// Agregamos una página
$this->pdf->AddPage();
// set border width
$this->pdf->SetLineWidth(0);
$this->pdf->setCellHeightRatio(1);

/* Se define el titulo, márgenes izquierdo, derecho y
 * el color de relleno predeterminado
 */
$this->pdf->SetLeftMargin(15);
$this->pdf->SetRightMargin(15);
$this->pdf->SetFillColor(56, 119, 119);
$this->pdf->SetLineWidth(1);
// Se define el formato de fuente: Arial, negritas, tamaño 9
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Image(base_url() . "assets/images/logo.png", 15, 15, 70);
$this->pdf->Cell(120, 10, utf8_decode('R.U.C.: '), '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 12);
// Bloque 1
$this->pdf->Cell(60, 10, '1911259889001', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 14);
$this->pdf->Cell(134, 10, 'FACTURA', '', 1, 'R', '0');
$this->pdf->SetFont('Arial', 'B', 12);
$this->pdf->Cell(115, 10, 'No.', '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 12);
$this->pdf->Cell(60, 10, '001-001-000000007', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(157, 10, 'NÚMERO DE AUTORIZACIÓN', '', 1, 'R', '0');
$this->pdf->SetFont('Arial', 'B', 7);
$this->pdf->Cell(188, 3, '4545435435345345345345345345345345345345435435444', '', 1, 'R', '0');
$this->pdf->Ln(5);
$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(130, 10, 'AMBIENTE:', '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 10);
$this->pdf->Cell(20, 10, 'PRUEBA', '', 1, 'R', '0');
$this->pdf->Ln(2);
$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(126.5, 10, 'EMISIÓN:', '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 10);
$this->pdf->Cell(20, 10, 'NORMAL', '', 1, 'R', '0');

$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(145.6, 10, 'CLAVE DE ACCESO:', '', 0, 'R', '0');


$style = array(
    'position' => '',
    'align' => '',
    'stretch' => true,
    'fitwidth' => false,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'text' => true,
    'label' => '4545435435345345345345345345345345345345435435444',
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
    );

// CODE 39 EXTENDED + CHECKSUM

$this->pdf->SetLineStyle(
    array(
        'width' => 1,
        'cap' => 'butt',
        'join' => 'miter',
        'dash' => 0,
        'color' => array(0, 0, 0),
        'align' => 'L',
        )
    );
$code = "4545435435345345345345345345345345345345435435444";
$this->pdf->Ln(10);
$this->pdf->SetX(120);                              # Anchura, Altura, Grozor
$this->pdf->write1DBarcode("$code", 'C128A', '', '', 85, 25, 0.4, $style, 'N');

// Bloque 2
$this->pdf->SetFont('Arial', '', 10);
$this->pdf->SetX(1);
$this->pdf->SetY(60);
$this->pdf->Cell(40, 10, 'SERVICIO DE RENTAS BASICAS', '', 1, 'L', '0');
$this->pdf->Cell(40, 20, 'SRI', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(40, 7, 'Dirección', '', 0, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(20, 7, 'SALINAS Y SANTIAGO', '', 1, 'R', '0');
$this->pdf->Cell(40, 5, 'Matriz:', '', 0, 'L', '0');
$this->pdf->Ln(5);
$this->pdf->Cell(40, 7, 'Dirección', '', 0, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(30, 7, 'PAEZ Y RAMIREZ DAVALOS', '', 1, 'R', '0');
$this->pdf->Cell(40, 5, 'Sucursal:', '', 0, 'L', '0');
$this->pdf->Ln(5);
$this->pdf->Cell(55, 7, 'Contribuyente Especial Nro: ', '', 0, 'L', '0');
$this->pdf->Cell(200, 7, '123456', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 8);
$this->pdf->Cell(63, 7, 'OBLIGADO A LLEVAR CONTABILIDAD', '', 0, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(200, 7, "SI", '', 1, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->Cell(40, 7, 'Razón Social / Nombres y Apellidos:', '', 0, 'L', '0');
$this->pdf->Cell(127, 7, "Identificación: 999999999", '', 1, 'R', '0');
$this->pdf->Ln(1);
$this->pdf->Cell(35, 7, "Fecha de Emisión: ", '', 0, 'L', '0');
$this->pdf->Cell(95, 7, "12/01/2017", '', 0, 'L', '0');
$this->pdf->Cell(60, 7, "Guía de Remisión: ", '', 1, 'L', '0');
// Bloque 3 Producto(s)
$this->pdf->Ln(3);
$this->pdf->SetLineStyle(array('width' => 0.0, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0));
$this->pdf->Cell(27, 7, "Cod Principal", 1, 0, 'C', 0);
$this->pdf->Cell(27, 7, "Cod Auxiliar", 1, 0, 'C', 0);
$this->pdf->Cell(15, 7, "Cant", 1, 0, 'C', 0);
$this->pdf->Cell(40, 7, "Descripción", 1, 0, 'C', 0);
$this->pdf->Cell(20, 7, "Descuento", 1, 0, 'C', 0);
$this->pdf->Cell(30, 7, "Precio Unitario", 1, 0, 'C', 0);
$this->pdf->Cell(30, 7, "Precio Total", 1, 0, 'C', 0);
foreach(range(1,2) as $row){
    $this->pdf->Ln(7);
    $this->pdf->Cell(27, 5, "01", 1, 0, 'C', 0);
    $this->pdf->Cell(27, 5, "01", 1, 0, 'C', 0);
    $this->pdf->Cell(15, 5, "2", 1, 0, 'C', 0);
    $this->pdf->Cell(40, 5, "Detergente", 1, 0, 'C', 0);
    $this->pdf->Cell(20, 5, "2", 1, 0, 'C', 0);
    $this->pdf->Cell(30, 5, "1500", 1, 0, 'C', 0);
    $this->pdf->Cell(30, 5, "3000", 1, 0, 'C', 0);
}

# Bloque informacion adicional
$this->pdf->Ln(38);
$this->pdf->Cell(100, 7, 'Información Adicional', '', 0, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->SetFont('Arial', '', 9);
$this->pdf->Cell(63, 7, 'Dirección: Salinas N17-203 y Santiago', '', 0, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->Cell(63, 7, 'Teléfono: 1700-774-744', '', 0, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->Cell(63, 7, 'Email: ejemplo@sri.gob.ec', '', 0, 'L', '0');
# Bloque Forma de Pago
$this->pdf->Ln(15);
$this->pdf->Cell(50, 7, 'Forma de Pago', 1, 0, 'C', '0');
$this->pdf->Cell(15, 7, 'Valor', 1, 0, 'C', '0');
$this->pdf->Cell(15, 7, 'Plazo', 1, 0, 'C', '0');
$this->pdf->Cell(15, 7, 'Tiempo', 1, 1, 'C', '0');

$this->pdf->Cell(50, 7, 'Dinero electrónico', 1, 0, 'C', '0');
$this->pdf->Cell(15, 7, '112.00', 1, 0, 'C', '0');
$this->pdf->Cell(15, 7, '20', 1, 0, 'C', '0');
$this->pdf->Cell(15, 7, 'Dias', 1, 0, 'C', '0');

# Bloque de total producto
$this->pdf->Ln(15);
$this->pdf->SetX(300);
$this->pdf->SetY(195);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'SUBTOTAL 14%', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '100.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(200);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'SUBTOTAL 0%', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(205);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'SUBTOTAL No objeto de IVA%', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(210);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'SUBTOTAL Exento de IVA%', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(215);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'SUBTOTAL SIN IMPUESTOS', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(220);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'TOTAL Descuento', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(225);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'ICE', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(230);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'IVA 14%', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(235);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'IRBPNR', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(240);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'PROPINA', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(245);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'Valor total', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');
$this->pdf->SetX(300);
$this->pdf->SetY(250);
$this->pdf->Cell(109, 5, '', 0, 0, 'C', '0');
$this->pdf->Cell(50, 5, 'Descuento Solidario 2% IVA', 1, 0, 'L', '0');
$this->pdf->Cell(30, 5, '0.00', 1, 1, 'R', '0');


// Salida del Formato PDF
$this->pdf->Output(utf8_decode("Factura Demo"), 'I');


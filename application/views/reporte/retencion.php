<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//$this->pdf   = new Pdf($orientation = 'L', $unit        = 'mm', $format      = 'A4');

// Agregamos una página
$this->pdf->AddPage();
// set border width
$this->pdf->SetLineWidth(0);
$this->pdf->setCellHeightRatio(1);

/* Se define el titulo, márgenes izquierdo, derecho y
 * el color de relleno predeterminado
 */
$this->pdf->SetLeftMargin(15);
$this->pdf->SetRightMargin(15);
$this->pdf->SetFillColor(56, 119, 119);
$this->pdf->SetLineWidth(1);
// Se define el formato de fuente: Arial, negritas, tamaño 9
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Image(base_url() . "assets/images/logo.png", 15, 15, 70);
$this->pdf->Cell(120, 10, utf8_decode('R.U.C.: '), '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 12);
// Bloque 1
$this->pdf->Cell(60, 10, '1911259889001', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(168, 10, 'COMPROBANTE DE RETENCIÓN', '', 1, 'R', '0');
$this->pdf->SetFont('Arial', 'B', 12);
$this->pdf->Cell(115, 10, 'No.', '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 12);
$this->pdf->Cell(60, 10, '001-001-000000007', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(157, 10, 'NÚMERO DE AUTORIZACIÓN', '', 1, 'R', '0');
$this->pdf->SetFont('Arial', 'B', 7);
$this->pdf->Cell(188, 3, '4545435435345345345345345345345345345345435435444', '', 1, 'R', '0');
$this->pdf->Ln(5);
$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(130, 10, 'AMBIENTE:', '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 10);
$this->pdf->Cell(20, 10, 'PRUEBA', '', 1, 'R', '0');
$this->pdf->Ln(2);
$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(126.5, 10, 'EMISIÓN:', '', 0, 'R', '0');
$this->pdf->SetFont('Arial', '', 10);
$this->pdf->Cell(20, 10, 'NORMAL', '', 1, 'R', '0');

$this->pdf->SetFont('Arial', 'B', 10);
$this->pdf->Cell(145.6, 10, 'CLAVE DE ACCESO:', '', 0, 'R', '0');


$style = array(
    'position' => '',
    'align' => '',
    'stretch' => true,
    'fitwidth' => false,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'text' => true,
    'label' => '4545435435345345345345345345345345345345435435444',
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
    );

// CODE 39 EXTENDED + CHECKSUM

$this->pdf->SetLineStyle(
    array(
        'width' => 1,
        'cap' => 'butt',
        'join' => 'miter',
        'dash' => 0,
        'color' => array(0, 0, 0),
        'align' => 'L',
        )
    );
$code = "4545435435345345345345345345345345345345435435444";
$this->pdf->Ln(10);
$this->pdf->SetX(120);                              # Anchura, Altura, Grozor
$this->pdf->write1DBarcode("$code", 'C128A', '', '', 85, 25, 0.4, $style, 'N');

// Bloque 2
$this->pdf->SetFont('Arial', '', 10);
$this->pdf->SetX(1);
$this->pdf->SetY(60);
$this->pdf->Cell(40, 10, 'SERVICIO DE RENTAS BASICAS', '', 1, 'L', '0');
$this->pdf->Cell(40, 20, 'SRI', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(40, 7, 'Dirección', '', 0, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(20, 7, 'SALINAS Y SANTIAGO', '', 1, 'R', '0');
$this->pdf->Cell(40, 5, 'Matriz:', '', 0, 'L', '0');
$this->pdf->Ln(5);
$this->pdf->Cell(40, 7, 'Dirección', '', 0, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(30, 7, 'PAEZ Y RAMIREZ DAVALOS', '', 1, 'R', '0');
$this->pdf->Cell(40, 5, 'Sucursal:', '', 0, 'L', '0');
$this->pdf->Ln(5);
$this->pdf->Cell(55, 7, 'Contribuyente Especial Nro: ', '', 0, 'L', '0');
$this->pdf->Cell(200, 7, '123456', '', 1, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 8);
$this->pdf->Cell(63, 7, 'OBLIGADO A LLEVAR CONTABILIDAD', '', 0, 'L', '0');
$this->pdf->SetFont('Arial', 'B', 9);
$this->pdf->Cell(200, 7, "SI", '', 1, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->Cell(40, 7, 'Razón Social / Nombres y Apellidos:', '', 0, 'L', '0');
$this->pdf->Cell(127, 7, "Identificación: 999999999", '', 1, 'R', '0');
$this->pdf->Ln(1);
$this->pdf->Cell(35, 7, "Fecha de Emisión: ", '', 0, 'L', '0');
$this->pdf->Cell(95, 7, "12/01/2017", '', 0, 'L', '0');
$this->pdf->Cell(60, 7, "Guía de Remisión: ", '', 1, 'L', '0');
// Bloque 3 Producto(s)
$this->pdf->Ln(3);
$this->pdf->SetLineStyle(array('width' => 0.0, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0));
$this->pdf->SetFont('Arial', 'B', 7);
$this->pdf->MultiCell(27, 6,"Comprobante", 1, 'C', 0, 0, '', '', true);
$this->pdf->MultiCell(29, 6,"Numero", 1, 'C', 0, 0, '', '', true);
$this->pdf->MultiCell(22, 6,"Fecha Emisión", 1, 'C', 0, 0, '', '', true);
$this->pdf->MultiCell(20, 6,"Ejercio Fiscal", 1, 'C', 0, 0, '', '', true);
$this->pdf->MultiCell(31, 6,"Base Imponible para la Retención", 1, 'C', 0, 0, '', '', true);
$this->pdf->MultiCell(20, 6,"Impuesto", 1, 'C', 0, 0, '', '', true);
$this->pdf->MultiCell(20, 6,"Porcentaje Retención", 1, 'C', 0, 0, '', '', true);
$this->pdf->MultiCell(20, 6,"Valor Retenido", 1, 'C', 0, 0, '', '', true);

foreach(range(1,2) as $row){
    $this->pdf->Ln(6);
    $this->pdf->SetFont('Arial', '', 7);
    $this->pdf->MultiCell(27, 6,"NOTA DE DEBITO", 1, 'C', 0, 0, '', '', true);
    $this->pdf->MultiCell(29, 6,"001001234565857", 1, 'C', 0, 0, '', '', true);
    $this->pdf->MultiCell(22, 6,"20/10/2017", 1, 'C', 0, 0, '', '', true);
    $this->pdf->MultiCell(20, 6,"15/01/2017", 1, 'C', 0, 0, '', '', true);
    $this->pdf->MultiCell(31, 6,"20", 1, 'C', 0, 0, '', '', true);
    $this->pdf->MultiCell(20, 6,"IVA", 1, 'C', 0, 0, '', '', true);
    $this->pdf->MultiCell(20, 6,"30.0", 1, 'C', 0, 0, '', '', true);
    $this->pdf->MultiCell(20, 6,"6.00", 1, 'C', 0, 0, '', '', true);
}

# Bloque informacion adicional
$this->pdf->Ln(70);
$this->pdf->Cell(100, 7, 'Información Adicional', '', 0, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->SetFont('Arial', '', 9);
$this->pdf->Cell(63, 7, 'Dirección: Salinas N17-203 y Santiago', '', 0, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->Cell(63, 7, 'Teléfono: 1700-774-744', '', 0, 'L', '0');
$this->pdf->Ln(7);
$this->pdf->Cell(63, 7, 'Email: ejemplo@sri.gob.ec', '', 0, 'L', '0');


// Salida del Formato PDF
$this->pdf->Output(utf8_decode("Factura Demo"), 'I');



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo $this->config->item('project_name'); ?></title>

        <!-- Bootstrap -->

        <link href="<?php echo assets_url('vendors/bootstrap/dist/css/bootstrap.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('vendors/bootstrap/dist/css/bootstrap-theme.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('vendors/nprogress/nprogress.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet" />

        <link href="<?php echo assets_url('vendors/datatables.net-bs/css/dataTables.bootstrap.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet" />

        <link href="<?php echo assets_url('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('vendors/pnotify/dist/pnotify.css'); ?>" rel="stylesheet">
        <link href="<?php echo assets_url('vendors/pnotify/dist/pnotify.buttons.css'); ?>" rel="stylesheet">
        <link href="<?php echo assets_url('vendors/pnotify/dist/pnotify.nonblock.css'); ?>" rel="stylesheet">
        <link href="<?php echo assets_url('vendors/iCheck/skins/flat/green.css'); ?>" rel="stylesheet">

        <link href="<?php echo assets_url('build/css/custom.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('css/datepicker3.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('css/fileinput.css'); ?>" rel="stylesheet" />
        <link href="<?php echo assets_url('css/flatpickr.min.css'); ?>" rel="stylesheet" />

<!--<link href="<?php echo assets_url('css/fancybox/jquery.fancybox.css'); ?>" rel="stylesheet" />-->

        <script src="<?php echo assets_url('vendors/jquery/dist/jquery.min.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/nprogress/nprogress.js'); ?>"></script>

        <script src="<?php echo assets_url('js/bootbox.min.js'); ?>"></script>
        <script src="<?php echo assets_url('js/jquery.uploadPreview.js'); ?>"></script>
        <script src="<?php echo assets_url('js/fileinput.js'); ?>"></script>
        <script src="<?php echo assets_url('js/flatpickr.min.js'); ?>"></script>

        <script src="<?php echo assets_url('vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/datatables.net-buttons/js/buttons.html5.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/datatables.net-buttons/js/buttons.print.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>

        <script src="<?php echo assets_url('js/jquery.mask.js'); ?>"></script>
        <script src="<?php echo assets_url('js/bootstrap-datepicker.js'); ?>"></script>
        <script src="<?php echo assets_url('js/bootstrap-datepicker.es.js'); ?>"></script>

        <script src="<?php echo assets_url('vendors/select2/dist/js/select2.full.min.js'); ?>"></script>

        <script src="<?php echo assets_url('vendors/pnotify/dist/pnotify.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/pnotify/dist/pnotify.buttons.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/pnotify/dist/pnotify.nonblock.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/iCheck/icheck.min.js'); ?>"></script>
        <script src="<?php echo assets_url('vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'); ?>"></script>

        <script src="<?php echo assets_url('js/select2_locale_es.js'); ?>"></script>
        <script src="<?php echo assets_url('js/funciones.js'); ?>"></script>
        <script src="<?php echo assets_url('js/librerias.js'); ?>"></script>
        <script src="<?php echo assets_url('js/validarcampos.js'); ?>"></script>
        <script src="<?php echo assets_url('js/jquery.numeric.js') ?>" type="text/javascript" charset="utf-8" ></script>
        <!--<script src="<?php echo assets_url('js/fancybox/jquery.fancybox.js'); ?>"></script>-->
        <script src="<?php echo assets_url('js/urls.js'); ?>"></script>


        <style type="text/css" media="screen">
            .cursor{
                cursor: pointer;
            }
            .form-control[disabled], .form-control[readonly] {
                background-color: #FFFFFF;
                opacity: 1;
            }
        </style>
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?php echo base_url(); ?>" class="site_title">
                                <i class="fa fa-user"></i>
                                <span>
                                    <?php echo $this->config->item('project_name'); ?>
                                </span>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <!-- menu profile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <img src="<?php echo assets_url('img/profile/default.gif'); ?>" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Bienvenido,</span>
                                <h2><?php echo $this->libreria->getName(); ?></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <?php echo menu(); ?>
                                    <li>
                                        <a class="change_password">
                                            <i class="fa fa-edit"></i> CAMBIO / CONTRASEÑA
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /sidebar menu -->
                    </div>
                </div>
                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo assets_url('img/profile/default.gif'); ?>" alt=""><?php echo $this->libreria->getName(); ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="javascript:;"> Perfil</a></li>
                                        <li><a href="<?php echo base_url('index.php/seguridad/users/logout') ?>"><i class="fa fa-sign-out pull-right"></i>Salir</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->
                <!-- page content -->
                <div class="right_col" role="main">
                    <?php echo $content ?>
                </div>
                <!-- /page content -->
                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                       <?php echo $this->config->item('project_name'); ?>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <div class="modal fade" id="div_cambio_user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <form method="post" enctype="multipart/form-data" id="frmpassword_user">
                            <div class="col-xs-12">
                                <div class="form-group col-xs-6" style="margin:auto;">
                                    <label>Contraseña anterior</label>
                                    <input type="password" id='password_f' name='password_f' class="form-control" placeholder="Contraseña anterior" autofocus='autofocus'/>
                                </div>
                                <div class="form-group col-xs-6" style="margin:auto;">
                                    <label>Contraseña nueva</label>
                                    <input type="password" id='password_new' name='password' class="form-control" placeholder="Contraseña nueva"/>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group col-xs-12">
                                    <label>Ingrese de nuevo su Contraseña</label>
                                    <input type="password" id='clave_new' name="clave_new" class="form-control" placeholder="Repita su contraseña"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default actualizar_passwd">Cambiar</button>
                        <button type="button" data-dismiss="modal" class="btn btn-default">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>



        <!-- jQuery -->
        <script src="<?php echo assets_url('build/js/custom.min.js'); ?>"></script>
        <script>
            $(document).ready(function () {
                // alert("<?php echo $this->session->userdata('user_id'); ?>");

                $("a.change_password").click(function () {
                    $('div#div_cambio_user').modal({backdrop: 'static', keyboard: true});
                });

                $(".actualizar_passwd").click(function () {
                    var $password = $("#password_f");
                    var $password_new = $("#password_new");
                    var $clave_new = $("#clave_new");
                    var formData = new FormData(document.getElementById("frmpassword_user"));
                    if ($password.val().trim() == "") {
                        bootbox.alert("Debe ingresar su contraseña anterior", function () {
                        }).on('hidden.bs.modal', function (event) {
                            $password.parent('div').addClass('has-error');
                            $password.focus();
                        });

                    } else if ($password_new.val().trim() == "") {
                        bootbox.alert("Debe ingresar su contraseña nueva", function () {
                        }).on('hidden.bs.modal', function (event) {
                            $password_new.parent('div').addClass('has-error');
                            $password_new.focus();
                        });

                    } else if ($clave_new.val().trim() == "") {
                        bootbox.alert("Ingrese de nuevo su contraseña", function () {
                        }).on('hidden.bs.modal', function (event) {
                            $clave_new.parent('div').addClass('has-error');
                            $clave_new.focus();
                        });

                    } else if ($password_new.val().trim() != $clave_new.val().trim()) {
                        bootbox.alert("Disculpe, las contraseñas no coinciden", function () {
                        }).on('hidden.bs.modal', function (event) {
                            $password_new.parent('div').addClass('has-error');
                            $clave_new.parent('div').addClass('has-error');
                            $password_new.focus();
                            $clave_new.focus();
                        });

                    } else {

                        bootbox.confirm("¿Está seguro de actualizar la información?", function (result) {
                            if (result == true) {
                                var data_send = $('#frmpassword_user').serialize();
                                $.get('<?php echo base_url()."seguridad/users/changePassword"; ?>',{'password_f': $password.val(),'clave_new': $clave_new.val()}, function(res, textStatus, xhr) {
                                    if (res == 1) {
                                        location.reload();
                                    } else if (res == 2) {
                                        bootbox.alert("Disculpe, las contraseñas anterior no es correcta");
                                    }
                                });
                            }
                        });
                    }
                });
            });
        </script>

    </body>
</html>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url('css/bootstrap.css'); ?>">
    <link href="<?php echo assets_url('vendors/pnotify/dist/pnotify.css'); ?>" rel="stylesheet">
    <link href="<?php echo assets_url('vendors/pnotify/dist/pnotify.buttons.css'); ?>" rel="stylesheet">
    <link href="<?php echo assets_url('vendors/pnotify/dist/pnotify.nonblock.css'); ?>" rel="stylesheet">
    <link href="<?php echo assets_url('css/login/login.css'); ?>" rel="stylesheet">

    <script src="<?php echo assets_url('js/jquery.1.10.js'); ?>"></script>
    <script src="<?php echo assets_url('vendors/pnotify/dist/pnotify.js'); ?>"></script>
    <script src="<?php echo assets_url('vendors/pnotify/dist/pnotify.buttons.js'); ?>"></script>
    <script src="<?php echo assets_url('vendors/pnotify/dist/pnotify.nonblock.js'); ?>"></script>
    <script src="<?php echo assets_url('js/url.js'); ?>"></script>
    <script src="<?php echo assets_url('js/login/login.js'); ?>"></script>
</head>
<body>
    <title><?php echo $this->config->item('project_name'); ?></title>
    <div class="main modal-content bg">
        <?php
        $attributes = array('id' => 'frmlogin');
        echo form_open('seguridad/users/validatelogin', $attributes);
        ?>
        <div style='text-align:center;margin:5%' class="div_foto">
            <img class="modal-content bg-foto" src="<?php echo assets_url("img/profile/default.gif");?>" style="width: 80px; height: 80px;margin: 4%;margin-top: 1%;"/>
        </div>
        <div class="inset">
            <p>
                <label for="user">Usuario</label>
                <input id='user' autofocus name='user' type="text" placeholder="Usuario" class='form-control busqueda'/>
            </p>
            <p>
                <label for="password">Contraseña</label>
                <input id='password' name='password' type="password" placeholder="Contraseña" class='form-control'/>
            </p>
            <p>
                <input type="checkbox" id="remember" disabled>
                <label for="remember">¿Olvido su contraseña?</label>
            </p>
            <div class="col-xs-6">
                <input style="width: 100%;" type="button" class="btn btn-info" id="login" value="Iniciar">
                <br/><br/>
            </div>
            <div class="col-xs-6">
                <a href="<?php echo base_url();?>">
                    <input style="width: 100%;" type="button" class="btn btn-warnig" value="Volver">
                </a>
            </div>
        </div>
    </form>
</div>
</body>
</html>
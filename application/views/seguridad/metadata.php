<?php
$i = 1;
foreach ($this->db->list_tables() as  $table) {
    ?>
    <h1 style="background-color:#696060;color:#FFFFFF;font-weight: bold;">
        <span style="font-weight:bold;">Tabla(<?php echo $i;?>): </span>
        <span><?php echo $table;?></span>

    </h1>
    <table border="1" style="width:100% !important;">
        <thead>
            <tr>
                <th>Campos</th>
                <th>Tipo dato</th>
                <th>Tamaño</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($this->db->field_data($table) as $field)
            {
                ?>
                <tr>
                    <td><?php echo $field->name; ?></td>
                    <td><?php echo $field->type; ?></td>
                    <td><?php echo $field->max_length; ?></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    $i++;
}
?>
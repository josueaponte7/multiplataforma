
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $titulo;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $attributes = array('id' => 'frmacceso', 'class' => 'formulario');
                echo form_open('seguridad/acceso/guardar', $attributes);
                ?>
                <?php echo form_hidden('token', $token) ?>
                <input type="hidden" name="id" value="<?php echo $id?>" id="id">
                <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="all_access">Tipo de Acceso * </label>
                        <select name="all_access" id="all_access" class="form-control reset reset select2 input-sm" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <option value="1">Publico</option>
                            <option value="2">Privado</option>
                        </select>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <label for="controllers">Controlador * </label>
                        <select name="controllers" id="controllers" class="form-control reset reset select2 input-sm" data-validate='required' data-add="id" data-mod="1">
                            <option value="0">Seleccione</option>
                            <?php
                            foreach ($directorio as $directorio) {
                                ?>
                                ?>
                                <option value="<?php echo $directorio ?>"><?php echo $directorio ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <fieldset>
                            <legend>Methodos</legend>
                        </fieldset>
                        <select multiple="" id="methods" name="methods" class="form-control reset reset select2 input-sm" data-validate='required'>
                            <option value="0">Seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="users">Usuario(s) * </label>
                        <select multiple="" id="users" name="users" class="form-control reset reset select2 input-sm" data-validate='required'>
                            <option value="0">Seleccione</option>
                            <?php
                            foreach ($users as $value) {?>
                            <option value="<?php echo $value->id ?>"><?php echo $value->si_user ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label for="url">Url de Acceso *</label>
                        <input id="url" name="url" readonly="" class="form-control" type="text" data-validate='required|max(30)|min(5)' data-type="alphanumeric">
                    </div>
                </div>

                <?php echo form_close();?>
                <br/>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-push-4" style="text-align: center">
                        <input type="button" class="btn btn-primary" data-accion="guardar" id="guardar" name="guardar" value="Guardar" />
                        <input type="button" class="btn btn-warning" id="cancelar" name="cancelar" value="Cancelar" />
                    </div>
                </div>
                <br/>
                <div class="ln_solid"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table style="width: 100%;" id="tbl_impuesto" data-counter="1" data-column='1' class="tabla table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th class="column-title">#</th>
                                    <th class="column-title">Controlador</th>
                                    <th class="column-title">Methodo(s)</th>
                                    <th class="column-title">Modificar</th>
                                    <th class="column-title">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($lista as $lista) {
                                    ?>
                                    <tr id="<?php echo $lista->id?>">
                                        <td><?php echo $i?></td>
                                        <td><?php echo $lista->controllers?></td>
                                        <td><?php echo $lista->methods?></td>
                                        <td>
                                            <img class="cursor modificar" src="<?php echo assets_url('img/datatable/modificar.png') ?>" alt="">
                                        </td>
                                        <td>
                                            <img class="cursor eliminar" src="<?php echo assets_url('img/datatable/eliminar.png') ?>" alt="">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="folder" id="folder" value="<?php echo $folder;?>">
    <input type="hidden" name="file" id="file" value="<?php echo $file;?>">
    <script src="<?php echo assets_url('script/'.$file.'.js'); ?>" type="text/javascript" charset="utf-8" ></script>


<?php
class Migration_Add_users extends CI_Migration
{
    public function up()
  {
    $fields = array(
      'id serial NOT NULL PRIMARY KEY',
      'username character varying(20) DEFAULT NULL',
      'password character varying(20) DEFAULT NULL'
    );

    $this->dbforge->add_field($fields);
    $this->dbforge->add_key('id', TRUE);
    $this->dbforge->create_table('users');
  }

  public function down()
  {
    $this->dbforge->drop_table('users');
  }

}
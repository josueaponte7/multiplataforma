<?php
class Migration_Add_module extends CI_Migration
{
    public function up()
  {
    $fields = array(
      'id serial NOT NULL PRIMARY KEY',
      'name_module character varying(20) DEFAULT NULL'
    );

    $this->dbforge->add_field($fields);
    $this->dbforge->add_key('id', TRUE);
    $this->dbforge->create_table('module');
  }

  public function down()
  {
    $this->dbforge->drop_table('module');
  }

}
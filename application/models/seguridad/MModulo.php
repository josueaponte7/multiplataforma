<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of Modulo
 *
 * @author josue
 */
class MModulo extends SI_Model
{
    private $table = 'se_modulo';
    private $directorio = array();
    //private $ruta = "application/controllers";
    public function __construct()
    {
        parent::__construct();

    }
    public function modulo()
    {
        $this->db->select('id, modulo');
        $this->db->where_in('modulo_id', 0);
        $query = $this->db->get($this->table);
        return $query->result();
    }
    public function listar()
    {
        $this->db->select("m.id, m.modulo_id, CASE   WHEN modulo_id>0 THEN (SELECT modulo FROM se_modulo WHERE id=m.modulo_id) ELSE ''END as modulo, m.modulo as submodulo, m.posicion, m.route, m.activo");
        $this->db->order_by("id", "asc");
        $query = $this->db->get('se_modulo AS m');
        // echo $this->db->last_query();
        return $query->result();
    }
    public function agregar($data)
    {
        $insert = $this->db->insert($this->table, $data);
        return $insert;
    }
    public function buscar($id)
    {
        $this->db->select('modulo, posicion, controller, route, modulo_id, activo');
        $this->db->where('id',$id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function getControllers()
    {
        $controller = [];
        foreach(glob(APPPATH . "controllers/{registro/,seguridad/}", GLOB_BRACE ) as $directories) {
            $directory = basename($directories);
            foreach(glob($directories.'*.php' )  as $files){
                $file = basename($files, '.php');
                $contr = $directory.'/'.$file;
                $this->db->where('controller', $contr);
                $query = $this->db->get($this->table);
                if($query->num_rows()== 0){
                   $controller[] =  $contr;
                }
            }
        }
        return $controller;
    }

    public function modificar($id,$data)
    {
        $this->db->where('id', $id);
        $result = $this->db->update($this->table,$data);
        return $result;
    }
    public function eliminar($id)
    {
        $this->db->where('modulo_id =', $id);
        $result = $this->db->get('se_permissions');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }
    }

    public function getParent($id)
    {
        $this->db->select('modulo_id');
        $this->db->where('id',$id);
        $query = $this->db->get($this->table);
        return $query->row()->modulo_id;
    }
}
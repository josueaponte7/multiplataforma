<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MAcceso extends SI_Model {

    private $table = 'access';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->select("*")
        ->get($this->table);
        return $query->result();
    }

    public function listar_controllers()
    {
        $query = $this->db->select("controllers")
        ->where('all_access',1)
        ->get($this->table);
        return $query->result();
    }

    public function listar_methods($controller)
    {
        $query = $this->db->select("methods")
        ->where('controllers',$controller)
        ->get($this->table);
        return $query->row();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {

        $this->insert($this->table, $data);
        return 2;


    }

    public function modificar($data)
    {
        $result = $this->update($this->table,$data);
        return $result;
    }
    public function eliminar($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        $response_data['success']='ok';
        $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
        echo json_encode($response_data);
    }

}

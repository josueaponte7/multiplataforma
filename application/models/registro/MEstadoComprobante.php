<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MEstadoComprobante extends SI_Model {

    private $table = 'mp_estado_comprobante';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->order_by("id", "ASC");
        $query = $this->db->select("*")->get($this->table);

        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {
        $this->db->where('siglas = ', $data['siglas']);
        $this->db->or_where('comprobante_electronico = ', $data['comprobante_electronico']);
        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            $this->insert($this->table, $data);
            return 2;
        }

    }

    public function modificar($data)
    {
        $result = $this->update($this->table,$data);
        return $result;
    }
    public function eliminar($id)
    {
        $this->db->where('estado_id = ', $id);
        $result = $this->db->get('mp_solicitud_contribuyente');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }

    }

}

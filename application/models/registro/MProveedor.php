<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MProveedor extends SI_Model {

    private $table = 'mp_proveedor';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->select("*")
        ->where('estatus', '1')
        ->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("id, rif, nombre, direccion, telefono, email, estatus")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {
        $this->db->or_where('rif = ', $data['rif']);
        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            // Proceso de manejo de la imagen
            if ($_FILES['logo_emisor']['name'] != ""){
                $archivo = $_FILES['logo_emisor']['name'];
                $ex      = explode('.', $archivo);
                $ex      = $ex[1]; // Extencion
                $archivo = $this->input->post('rif') . "." . $ex;
                $ruta    = getcwd();  // Obtiene el directorio actual en donde se esta trabajando
                //echo $ruta;
                move_uploaded_file($_FILES['logo_emisor']['tmp_name'], $ruta . "/assets/fotos/" . $archivo);
                $data['logo_emisor'] = $archivo;
            }
            $this->insert($this->table, $data);
            return 2;
        }
    }

    public function modificar($data)
    {
        $result = $this->update($this->table,$data);
        return $result;
    }
    public function eliminar($id)
    {
        $this->db->where('proveedor_id = ', $id);
        $result = $this->db->get('mp_factura_detalles');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }

    }

}

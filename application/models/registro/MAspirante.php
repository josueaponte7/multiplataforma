<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MAspirante extends SI_Model {

    private $table = 'mp_aspirante';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->select("*")
        ->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    // Busqueda Ajax
    public function ajax_search($get)
    {
        if($get['v'] == 1){
            $table = "mp_pais";
            $campo = "continente_id";
            $valor = $get['continente'];
        }else if($get['v'] == 2){
            $table = "mp_region";
            $campo = "pais_id";
            $valor = $get['pais'];
        }else if($get['v'] == 3){
            $table = "mp_provincia";
            $campo = "region_id";
            $valor = $get['region'];
        }else if($get['v'] == 4){
            $table = "mp_canton";
            $campo = "provincia_id";
            $valor = $get['provincia'];
        }else if($get['v'] == 5){
            $table = "mp_parroquia";
            $campo = "canton_id";
            $valor = $get['canton'];
        }else if($get['v'] == 6){
            $table = "mp_sector";
            $campo = "parroquia_id";
            $valor = $get['parroquia'];
        }else if($get['v'] == 7){
            $sector = $get['sector'];
        }

        $query = $this->db->select("*")
        ->where("$campo", $valor)
        ->get("$table");
        return $query->result();
    }

    public function guardar($data)
    {
        $this->db->where('cedula = ', $data['cedula']);
        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            // Proceso de manejo de la imagen
            if ($_FILES['foto_perfil']['name'] != ""){
                $archivo = $_FILES['foto_perfil']['name'];
                $ex      = explode('.', $archivo);
                $ex      = $ex[1]; // Extencion
                if($ex == "jpg"){
                    @unlink(base_url("assets/fotos/").$this->input->post('cedula').".jpg");
                }else{
                    @unlink(base_url("assets/fotos/").$this->input->post('cedula').".png");
                }
                $archivo = $this->input->post('cedula') . "." . $ex;
                $ruta    = getcwd();  // Obtiene el directorio actual en donde se esta trabajando
                move_uploaded_file($_FILES['foto_perfil']['tmp_name'], $ruta . "/assets/fotos/" . $archivo);
                $data['foto_perfil'] = $archivo;
            }
            $this->insert($this->table, $data);
            /*$this->db->where('cedula', $data['cedula']);
            $datos['estatus'] = $data['estatus'];
            $this->db->update('mp_aspirante', $datos);*/
            return 2;
        }

    }

    public function modificar($data)
    {
        // Proceso de manejo de la imagen
        if ($_FILES['foto_perfil']['name'] != ""){
            $archivo = $_FILES['foto_perfil']['name'];
            $ex      = explode('.', $archivo);
                $ex      = $ex[1]; // Extencion
                if($ex == "jpg"){
                    @unlink(base_url("assets/fotos/").$this->input->post('cedula').".jpg");
                }else{
                    @unlink(base_url("assets/fotos/").$this->input->post('cedula').".png");
                }
                $archivo = $this->input->post('cedula') . "." . $ex;
                $ruta    = getcwd();  // Obtiene el directorio actual en donde se esta trabajando

                move_uploaded_file($_FILES['foto_perfil']['tmp_name'], $ruta . "/assets/fotos/" . $archivo);
                $data['foto_perfil'] = $archivo;
            }
            $result = $this->update($this->table,$data);
            /*$this->db->where('cedula', $data['cedula']);
            $datos['estatus'] = $data['estatus'];
            $this->db->update('mp_aspirante', $datos);*/
            return $result;
        }
        public function eliminar($id)
        {
            $this->db->where('cedula = ', $id);
            $result = $this->db->get('mp_empleado');
            if ($result->num_rows() > 0) {
                $response_data['success']='existe';
                $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
                echo json_encode($response_data);
            }else{
                $this->db->where('id', $id);
                $this->db->delete($this->table);
                $response_data['success']='ok';
                $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
                echo json_encode($response_data);
            }
        }

        // Guadar lista de estudios realizados
        public function estudio($data)
        {
            $this->db->where('profesion_id = ', $data['profesion_id']);
            $result = $this->db->get('mp_empleado_estudios_realizado');
            if ($result->num_rows() > 0) {
                return 1;
            }else{
                $result = $this->db->insert("mp_empleado_estudios_realizado", $data);
                return 2;
            }
        }

        public function referencia($data)
        {
            $this->db->where('profesion_id = ', $data['profesion_id']);
            $result = $this->db->get('mp_empleado_referencia_laboral');
            if ($result->num_rows() > 0) {
                return 1;
            }else{
                $result = $this->db->insert("mp_empleado_referencia_laboral", $data);
                return 2;
            }
        }

        public function empresa($data)
        {
            $this->db->where('nom_empresa = ', $data['nom_empresa']);
            $this->db->or_where('cargo = ', $data['cargo']);
            $result = $this->db->get('mp_empleado_empresa_laboral');
            if ($result->num_rows() > 0) {
                return 1;
            }else{
                $result = $this->db->insert("mp_empleado_empresa_laboral", $data);
                return 2;
            }
        }

        // Ajax carga de datos
        public function estudio_ajax($id)
        {
            $query = $this->db->select("*")
            ->where('empleado_id', $id)
            ->get("mp_empleado_estudios_realizado");
            return $query->result();
        }

        public function eliminar_estudio($id)
        {
            $this->db->where('id', $id);
            $this->db->delete("mp_empleado_estudios_realizado");
        }

        public function referencia_ajax($id)
        {
            $query = $this->db->select("*")
            ->where('empleado_id', $id)
            ->get("mp_empleado_referencia_laboral");
            return $query->result();
        }

        public function eliminar_referencia($id)
        {
            $this->db->where('id', $id);
            $this->db->delete("mp_empleado_referencia_laboral");
        }

        public function empresa_ajax($id)
        {
            $query = $this->db->select("*")
            ->where('empleado_id', $id)
            ->get("mp_empleado_empresa_laboral");
            return $query->result();
        }

        public function eliminar_empresa($id)
        {
            $this->db->where('id', $id);
            $this->db->delete("mp_empleado_empresa_laboral");
        }

    }

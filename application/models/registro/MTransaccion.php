<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MTransaccion extends SI_Model {

    private $table = 'mp_comprador_detalle';
    private $table_mp_cliente = "mp_cliente";

    public function __construct()
    {
        parent::__construct();
    }

    public function listar($ced)
    {
        $query = $this->db->select("
            $this->table.id,
             $this->table.fecha_hora_emision,
             $this->table.fecha_hora_emision,
             mp_descuento.porcentaje,
             $this->table.monto,
             $this->table.sub_total,
             $this->table.total,
             mp_cliente.disponibilidad,
             mp_cliente.num_cuenta,
             mp_cliente.tipo_cuenta,
             mp_banco.banco,
              ")
        ->where("$this->table.cedula", $ced)
        ->join('mp_descuento', $this->table.'.descuento_id = mp_descuento.id', 'inner')
        ->join('mp_cliente', $this->table.'.cedula = mp_cliente.cedula', 'inner')
        ->join('mp_banco', "mp_cliente.banco_id = mp_banco.id", 'inner')
        ->get($this->table);
        return $query->result();
    }

    public function disponibilidad_exist($ced,$disponibilidad)
    {   
        $datos['disponibilidad'] = $disponibilidad;
        $result = $this->db->where('cedula', $ced);
        $result = $this->db->update($this->table_mp_cliente, $datos);
        return $result;
    }

    function format_number($decimal)
    {
        $result = str_replace('', '', number_format($decimal, 2, ",", "."));
        return $result;
    }
}

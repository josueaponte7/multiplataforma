<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MDescuento extends SI_Model {

    private $table = 'mp_descuento';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->select("*")->where('estatus', 1)
        ->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {
        $this->db->where('porcentaje = ', $data['porcentaje']);
        $this->db->or_where('descripcion = ', $data['descripcion']);
        $this->db->or_where('estatus = ', 1);
        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            $this->insert($this->table, $data);
            return 2;
        }

    }

    public function modificar($data)
    {   

        $this->db->where('porcentaje = ', $data['porcentaje']);
        $this->db->where('estatus = ', 1);
        $this->db->where('id != ', $data['id']);

        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            $result = $this->update($this->table,$data);
            return 2;
        }
    }
    public function eliminar($id)
    {
        $this->db->where('retencion_id = ', $id);
        $result = $this->db->get('mp_producto');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }

    }

}

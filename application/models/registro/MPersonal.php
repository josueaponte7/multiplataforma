<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MPersonal extends SI_Model {

    private $table = 'mp_empleado';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->select("
            $this->table.id,
            $this->table.cargo_id,
            CONCAT(mp_aspirante.p_nombre,' ',mp_aspirante.s_nombre,' ',mp_aspirante.p_apellido,' ',mp_aspirante.s_apellido) AS nombres,
            $this->table.fecha_ingreso,
            $this->table.fecha_egreso,
            $this->table.estatus,
            $this->table.salario,
            ")
        ->join('mp_aspirante', $this->table.'.aspirante_id = mp_aspirante.id', 'inner')
        ->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {
        $this->db->where('aspirante_id = ', $data['aspirante_id']);
        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            $this->insert($this->table, $data);
            $this->db->where('id', $data['aspirante_id']);
            $datos['estatus'] = $data['estatus'];
            $this->db->update('mp_aspirante', $datos);
            return 2;
        }
    }

    public function modificar($data)
    {
        $result = $this->update($this->table,$data);
        $this->db->where('id', $data['aspirante_id']);
        $datos['estatus'] = $data['estatus'];
        $this->db->update('mp_aspirante', $datos);
        return $result;
    }
    public function eliminar($id)
    {
        $this->db->where('cedula_id = ', $id);
        $result = $this->db->get('mp_empleado');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }
    }

        // Guadar lista de estudios realizados
    public function estudio($data)
    {
        $this->db->where('profesion_id = ', $data['profesion_id']);
        $result = $this->db->get('mp_empleado_estudios_realizado');
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            $result = $this->db->insert("mp_empleado_estudios_realizado", $data);
            return 2;
        }
    }

    public function referencia($data)
    {
        $this->db->where('profesion_id = ', $data['profesion_id']);
        $result = $this->db->get('mp_empleado_referencia_laboral');
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            $result = $this->db->insert("mp_empleado_referencia_laboral", $data);
            return 2;
        }
    }

    public function empresa($data)
    {
        $this->db->where('nom_empresa = ', $data['nom_empresa']);
        $this->db->or_where('cargo = ', $data['cargo']);
        $result = $this->db->get('mp_empleado_empresa_laboral');
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            $result = $this->db->insert("mp_empleado_empresa_laboral", $data);
            return 2;
        }
    }

        // Ajax carga de datos
    public function estudio_ajax($id)
    {
        $query = $this->db->select("*")
        ->where('empleado_id', $id)
        ->get("mp_empleado_estudios_realizado");
        return $query->result();
    }

    public function eliminar_estudio($id)
    {
        $this->db->where('id', $id);
        $this->db->delete("mp_empleado_estudios_realizado");
    }

    public function referencia_ajax($id)
    {
        $query = $this->db->select("*")
        ->where('empleado_id', $id)
        ->get("mp_empleado_referencia_laboral");
        return $query->result();
    }

    public function eliminar_referencia($id)
    {
        $this->db->where('id', $id);
        $this->db->delete("mp_empleado_referencia_laboral");
    }

    public function empresa_ajax($id)
    {
        $query = $this->db->select("*")
        ->where('empleado_id', $id)
        ->get("mp_empleado_empresa_laboral");
        return $query->result();
    }

    public function eliminar_empresa($id)
    {
        $this->db->where('id', $id);
        $this->db->delete("mp_empleado_empresa_laboral");
    }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MContribuyente extends SI_Model {

    private $table = 'mp_solicitud_contribuyente';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->select("$this->table.id, $this->table.num_comprobante, $this->table.clave_acceso_id, mp_cliente.identificacion, mp_cliente.razon_social")
        ->join('mp_cliente', $this->table.'.cliente_id = mp_cliente.id', 'inner')
        ->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function procesar($id)
    {
        $data['estatus'] = 0;
        $data['observacion'] = "Error de Clave de Acceso";
        $result = $this->db->where('id', $id);
        $result = $this->db->update('mp_clave_acceso', $data);
        return $result;

    }

    public function guardar($data)
    {
        /*$this->db->where('num_ruc = ', $data['num_ruc']);
        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{*/
            $this->insert($this->table, $data);
            return 2;
        /*}*/

    }

    public function guardar_productos($data)
    {
        $this->insert("solicitud_contribuyente_factura_detalle", $data);
    }

    public function modificar($data)
    {
        $result = $this->update($this->table,$data);
        return $result;
    }
    public function eliminar($id)
    {
        $this->db->where('ambiente_id = ', $id);
        $result = $this->db->get('mp_solicitud_contribuyente');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }

    }

}

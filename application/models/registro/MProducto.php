<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MProducto extends SI_Model {

    private $table = 'mp_producto';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->select("*")
        ->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function buscar_producto($id)
    {
        $query = $this->db->select("
            $this->table.id,
            $this->table.valor_unitario,
            $this->table.descuento,
            $this->table.validador,
            mp_impuesto_tarifa.porcentaje,
            ")
        ->join('mp_impuesto_tarifa', $this->table.'.impuesto_id = mp_impuesto_tarifa.impuesto_id', 'left')
        ->where("$this->table.id", $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {
        $data['fecha_creacion'] = date('Y-m-d h:i:s');
        $insert = $this->insert($this->table, $data);
        return $insert;
    }

    public function modificar($data)
    {
        $result = $this->update($this->table,$data);
        return $result;
    }
    public function eliminar($id)
    {
        $this->db->where('producto_id = ', $id);
        $result = $this->db->get('mp_factura_producto');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }

    }

}

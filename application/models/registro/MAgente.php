<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MAgente extends SI_Model {

    private $table = 'mp_agente_retencion';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->select("*")
        ->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("id, num_ruc, razon_social, nom_comercial, directorio_matriz,
       directorio_emisor, cod_emision, contribuyente, contabilidad, tipo_ambiente_id, tipo_emision_id, cod_establecimiento_emisor,
       hora_servicio, pais_id")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {
        $this->db->or_where('num_ruc = ', $data['num_ruc']);
        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            // Proceso de manejo de la imagen
            if ($_FILES['logo_emisor']['name'] != ""){
				$archivo = $_FILES['logo_emisor']['name'];
				$ex      = explode('.', $archivo);
				$ex      = $ex[1]; // Extencion
				$archivo = $this->input->post('num_ruc') . "." . $ex;
				$ruta    = getcwd();  // Obtiene el directorio actual en donde se esta trabajando
				//echo $ruta;
				move_uploaded_file($_FILES['logo_emisor']['tmp_name'], $ruta . "/assets/fotos/" . $archivo);
				$data['logo_emisor'] = $archivo;
			}
            $this->insert($this->table, $data);
            return 2;
        }

    }

    public function modificar($data)
    {
        $this->update($this->table,$data);
        return 2;
    }
    public function eliminar($id)
    {
        $this->db->where('agente_id = ', $id);
        $result = $this->db->get('mp_solicitud_contribuyente');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }

    }

}

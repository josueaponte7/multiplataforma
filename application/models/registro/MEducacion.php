<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MEducacion extends SI_Model {

    private $table = 'mp_aspirante_educacion';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar($id)
    {
        $query = $this->db->select("*")
        ->where('aspirante_id', $id)
        ->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {
        $this->insert($this->table, $data);
        return 2;
    }

    public function modificar($data)
    {
        $result = $this->update($this->table,$data);
        return $result;
    }
    public function eliminar($id)
    {
        /*$this->db->where('contribuyente_id = ', $id);
        $result = $this->db->get('mp_solicitud_contribuyente');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{*/
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
            /*}*/

        }

    }

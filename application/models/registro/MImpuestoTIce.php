<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MImpuestoTIce extends SI_Model {

    private $table = 'mp_impuesto_tarifa';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $this->db->select('a.id, a.codigo, a.porcentaje, b.impuesto');
        $this->db->from('mp_impuesto_tarifa as a');
        $this->db->join('mp_impuesto as b', 'a.impuesto_id = b.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {
        $this->db->where('impuesto_id = ', $data['impuesto_id']);
        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            $this->insert($this->table, $data);
            return 2;
        }

    }

    public function modificar($data)
    {
        $result = $this->update($this->table,$data);
        return $result;
    }
    public function eliminar($id)
    {
        $this->db->where('impuesto_id = ', $id);
        $result = $this->db->get('mp_producto');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }

    }

}

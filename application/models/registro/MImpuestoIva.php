<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MImpuestoIva extends SI_Model {

    private $table = 'mp_impuesto_tarifa_iva';

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $query = $this->db->select("*")
        ->join('mp_impuesto', $this->table.'.impuesto_id = mp_impuesto.id', 'inner')
        ->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $query = $this->db->select("*")
        ->where('id', $id)
        ->get($this->table);
        return $query->row();
    }

    public function guardar($data)
    {
        $this->db->where('codigo = ', $data['codigo']);
        $this->db->or_where('impuesto = ', $data['impuesto']);
        $result = $this->db->get($this->table);
        if ($result->num_rows() > 0) {
            return 1;
        }else{
            $this->insert($this->table, $data);
            return 2;
        }

    }

    public function modificar($data)
    {
        $result = $this->update($this->table,$data);
        return $result;
    }
    public function eliminar($id)
    {
        $this->db->where('impuesto_id = ', $id);
        $result = $this->db->get('mp_producto');
        if ($result->num_rows() > 0) {
            $response_data['success']='existe';
            $response_data['msg']='<div>El registro está asociado a uno o mas registros</div>';
            echo json_encode($response_data);
        }else{
            $this->db->where('id', $id);
            $this->db->delete($this->table);
            $response_data['success']='ok';
            $response_data['msg']='<div>El registro fue eliminado exitosamente</div>';
            echo json_encode($response_data);
        }

    }

}

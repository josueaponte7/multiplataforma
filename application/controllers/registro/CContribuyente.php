<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CContribuyente extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'contribuyente');
    private $vista = '';
    private $table = 'mp_solicitud_contribuyente';
    private $titulo = 'Registro de Contribuyente';
    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MContribuyente','contribuyente');
        $this->load->model('registro/MComprobante','comprobante');
        $this->load->model('registro/MAmbiente','ambiente');
        $this->load->model('registro/MEmision','emision');
        $this->load->model('registro/MCliente','cliente');
        $this->load->model('registro/MProducto','producto');
        $this->load->model('registro/MClaveAcceso','clave_acceso');
        $this->load->model('registro/MEstadoComprobante','estado');
        $this->load->model('registro/MAgente','agente');
        $this->load->model('registro/MFormaPago','formapago');
        $this->load->model('registro/MPais','pais');
    }
    public function index()
    {
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['id']        = $this->modulo->lastId($this->table);
        $datos['num_comprobante'] = $this->modulo->lastId_correlative($this->table,9);
        $datos['num_serie'] = $this->modulo->lastId_correlative($this->table,3);
        $datos['cod_numerico'] = $this->modulo->lastId_correlative($this->table,8);
        $datos['lista']     = $this->contribuyente->listar();
        $datos['comprobante']     = $this->comprobante->listar();
        $datos['emision']     = $this->emision->listar();
        $datos['ambiente']     = $this->ambiente->listar();
        $datos['cliente']     = $this->cliente->listar();
        $datos['agente']     = $this->agente->listar();
        $datos['producto']    = $this->producto->listar();
        $datos['clave_acceso']     = $this->clave_acceso->listar();
        $datos['estado']     = $this->estado->listar();
        $datos['formapago']     = $this->formapago->listar();
        $datos['pais']     = $this->pais->listar();
        $datos['token']     = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->contribuyente->buscar($id);
        echo json_encode($row);
    }

    public function procesar()
    {
        $id = $this->input->get('id');
        $row = $this->contribuyente->procesar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->contribuyente->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $result = $this->contribuyente->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $response_data['success']='error';
            $data['fecha_emision'] = date('dmY'); # Fecha de Emision
            $result = $this->contribuyente->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }

            $datos = $data['campos'];
            foreach ($datos as $value) {
                $array = explode(";", $value);
                $data['producto_id'] = $array[0];
                $data['precio_unitario'] = $array[1];
                $data['descuento'] = $array[2];
                $data['cantidad'] = $array[3];
                $data['precio_total'] = $array[4];
                $data['solicitud_contribuyente_factura_id'] = $array[5];
                $this->contribuyente->guardar_productos($data);
            }

        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);

    }
}

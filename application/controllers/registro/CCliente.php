<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CCliente extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'cliente');
    private $vista = '';
    private $table = 'mp_cliente';
    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MCliente','cliente');
        $this->load->model('registro/MIdentificacion','identificacion');
        $this->load->model('registro/MCategoriaCliente','categoriacliente');
        $this->load->model('registro/MPais','pais');
    }
    public function index()
    {
        $datos['id']        = $this->modulo->lastId($this->table);
        $datos['lista']     = $this->cliente->listar();
        $datos['num_ruc'] = $this->modulo->lastId_correlative('mp_cliente',3);
        $datos['identificacion'] = $this->identificacion->listar();
        $datos['categoriacliente'] = $this->categoriacliente->listar();
        $datos['pais'] = $this->pais->listar();
        $datos['token']     = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->cliente->buscar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->cliente->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $result = $this->cliente->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $response_data['success']='error';
            $result = $this->cliente->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
}

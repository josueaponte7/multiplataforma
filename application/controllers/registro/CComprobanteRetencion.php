<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CComprobanteRetencion extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'comprobanteretencion');
    private $vista = '';
    private $table = 'mp_comprobante_retencion';
    private $titulo = 'Registro Comprobante de Retención';

    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MComprobanteRetencion','comprobanteretencion');
        $this->load->model('registro/MComprobante','comprobante');
        $this->load->model('registro/MClaveAcceso','clave_acceso');
        $this->load->model('registro/MContribuyente','contribuyente');
        $this->load->model('registro/MRetencionRenta','retencion');
        $this->load->model('registro/MEstadoComprobante','estado');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['id']     = $this->modulo->lastId($this->table);
        $datos['lista']  = $this->comprobanteretencion->listar();
        $datos['clave_acceso']  = $this->clave_acceso->listar();
        $datos['contribuyente']  = $this->contribuyente->listar();
        $datos['comprobante']  = $this->comprobante->listar();
        $datos['impuesto']  = $this->retencion->listar();
        $datos['estado']     = $this->estado->listar();
        $datos['token']  = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->comprobanteretencion->buscar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->comprobanteretencion->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $result = $this->comprobanteretencion->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {

        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $response_data['success']='error';
            $data['fecha_emision'] = date('dmY'); # Fecha de Emision
            $result = $this->comprobanteretencion->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
}

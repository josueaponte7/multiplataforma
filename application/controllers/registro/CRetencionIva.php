<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CRetencionIva extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'retencioniva');
    private $vista = '';
    private $table = 'mp_retencion_iva';
    private $titulo = 'Registro Retención IVA';

    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MRetencion','retencion');
        $this->load->model('registro/MRetencionIva','retencioniva');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['id']     = $this->modulo->lastId($this->table);
        $datos['lista']  = $this->retencioniva->listar();
        $datos['retencion']  = $this->retencion->listar();
        $datos['token']  = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->retencioniva->buscar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->retencioniva->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $data['porcentaje'] = str_replace(',', '.', $data['porcentaje']);
            $result = $this->retencioniva->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {

        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $response_data['success']='error';
            $data['porcentaje'] = str_replace(',', '.', $data['porcentaje']);
            $result = $this->retencioniva->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
}

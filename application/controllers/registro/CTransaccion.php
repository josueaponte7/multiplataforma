<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CTransaccion extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'transaccion');
    private $vista = '';
    private $table = 'mp_comprador_detalle';
    private $titulo = 'Actividades mas reciente';

    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MTransaccion','transaccion');
        $this->load->model('seguridad/MUsers','user');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {   
        $row = $this->user->buscar($this->session->userdata('user_id'));
        $ced = $row[0]->cedula;
        $nom = $row[0]->first_name." ".$row[0]->last_name;;
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['lista']  = $this->transaccion->listar($ced);
        $datos['nom'] = $nom;
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();
    }
}

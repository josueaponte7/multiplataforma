<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CPersonal extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'personal');
    private $vista = '';
    private $table = 'mp_empleado';
    private $titulo = 'Registro de Personal';

    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MPersonal','personal');
        $this->load->model('registro/MEstadoCivil','estadocivil');
        $this->load->model('registro/MAspirante','aspirante');
        $this->load->model('registro/MCargo','cargo');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['id']     = $this->modulo->lastId($this->table);
        $datos['lista']  = $this->personal->listar();
        $datos['estadocivil']  = $this->estadocivil->listar();
        $datos['aspirante']  = $this->aspirante->listar();
        $datos['cargo']  = $this->cargo->listar();
        $datos['token']  = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->personal->buscar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->personal->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $fecha_ingreso = date('Y-m-d', strtotime($data['fecha_ingreso']));
            $data['fecha_ingreso'] = $fecha_ingreso;
            if($data['fecha_egreso'] == ""){
                $data['fecha_egreso'] = NULL;
            }else{
                $data['fecha_egreso'] = $data['fecha_egreso'];
            }
            $result = $this->personal->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $response_data['success']='error';
            $fecha_ingreso = date('Y-m-d', strtotime($data['fecha_ingreso']));
            $data['fecha_ingreso'] = $fecha_ingreso;
            if($data['fecha_egreso'] == ""){
                $data['fecha_egreso'] = NULL;
            }else{
                $data['fecha_egreso'] = $data['fecha_egreso'];
            }
            $result = $this->personal->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }

    public function estudio()
    {
        $datos = $this->input->get('campos[]');
        foreach ($datos as $value) {
            $v = explode(";", $value);
            $data['nivel'] = $v[0];
            $data['profesion_id'] = $v[1];
            $data['instituto'] = $v[2];
            $data['empleado_id'] = $v[3];
            $result = $this->personal->estudio($data);
        }
        if($result == 1){
            $response_data['success']='existe';
            $response_data['msg']='<div>Ya se encuentra registrado</div>';
        }else if($result == 2){
            $response_data['success']='ok';
            $response_data['msg']='<div>Registro guardado con exito</div>';
        }
        echo json_encode($response_data);
    }

    public function referencia()
    {
        $datos = $this->input->get('campos[]');
        foreach ($datos as $value) {
            $v = explode(";", $value);
            $data['profesion_id'] = $v[0];
            $data['nombres'] = $v[1];
            $data['telefono'] = $v[2];
            $data['empleado_id'] = $v[3];
            $result = $this->personal->referencia($data);
        }
        if($result == 1){
            $response_data['success']='existe';
            $response_data['msg']='<div>Ya se encuentra registrado</div>';
        }else if($result == 2){
            $response_data['success']='ok';
            $response_data['msg']='<div>Registro guardado con exito</div>';
        }
        echo json_encode($response_data);
    }

    public function empresa()
    {
        $datos = $this->input->get('campos[]');
        foreach ($datos as $value) {
            $v = explode(";", $value);
            $data['cargo'] = $v[0];
            $data['nom_empresa'] = $v[1];
            $data['duracion'] = $v[2];
            $data['empleado_id'] = $v[3];
            $result = $this->personal->empresa($data);
        }
        if($result == 1){
            $response_data['success']='existe';
            $response_data['msg']='<div>Ya se encuentra registrado</div>';
        }else if($result == 2){
            $response_data['success']='ok';
            $response_data['msg']='<div>Registro guardado con exito</div>';
        }
        echo json_encode($response_data);
    }

    // Ajax carga de datos
    public function estudio_ajax()
    {
        $id = $this->input->get('id');
        $row = $this->personal->estudio_ajax($id);
        echo json_encode($row);
    }

    public function eliminar_estudio()
    {
        $id = $this->input->get('id');
        $this->personal->eliminar_estudio($id);
    }

    public function referencia_ajax()
    {
        $id = $this->input->get('id');
        $row = $this->personal->referencia_ajax($id);
        echo json_encode($row);
    }

    public function eliminar_referencia()
    {
        $id = $this->input->get('id');
        $this->personal->eliminar_referencia($id);
    }

    public function empresa_ajax()
    {
        $id = $this->input->get('id');
        $row = $this->personal->empresa_ajax($id);
        echo json_encode($row);
    }

    public function eliminar_empresa()
    {
        $id = $this->input->get('id');
        $this->personal->eliminar_empresa($id);
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CEstadoComprobante extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'estadocomprobante');
    private $vista = '';
    private $table = 'mp_estado_comprobante';
    private $titulo = 'Registro Estado Comprobante';
    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MEstadoComprobante','estadocomprobante');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {   
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['id']        = $this->modulo->lastId($this->table);
        $datos['lista']     = $this->estadocomprobante->listar();
        $datos['token']     = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->estadocomprobante->buscar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->estadocomprobante->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $result = $this->estadocomprobante->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $response_data['success']='error';
            $result = $this->estadocomprobante->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
}

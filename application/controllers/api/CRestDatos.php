<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'libraries/REST_Controller.php');

class CRestDatos extends REST_Controller {

    //con esto limitamos las consultas y los permisos a la api
    protected $methods = array(
        'users_get' => array('level' => 0),//para acceder a users_get debe tener level 1 y no hay limite de consultas por hora
        'user_get' => array('level' => 0, 'limit' => 10),//user_get sólo level 0, pero máximo 10 consultas por hora
        'posts_user_get' => array('level' => 0, 'limit' => 10),//se necesita level 0 y sólo se pueden hacer 10 consultas por hora
        'new_user_post' => array('level' => 1),//se necesita level 1, no hay limite de peticiones
        'user_post' => array('level' => 1, 'limit' => 5),//se necesita level 1 y 5 peticiones
        );

    public function __construct() {
        parent::__construct();

        header('Content-type: application/json');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET");
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

        $this->load->database();
        $this->load->helper('url');
        $this->load->model('registro/MContribuyente','factura');
        $this->load->model('registro/MDescuento','descuento');
        $this->load->model('registro/MTransaccion','transaccion');
        $this->load->model('registro/MCliente','cliente');
    }
    // Para Crear un recurso
    public function detalles_get()
    {   
        $datos = $this->input->get('datos');
        $piezas = explode(" ", $datos);
        
        $data['cedula'] = $piezas[0];
        $data['email']  = preg_replace('[\s+]','', $piezas[1]);
        $data['monto']  = str_replace(",", ".", $piezas[2]);
        $data['fecha_hora_emision'] = date('Y-m-d h:i:s');
        $descuento = $this->descuento->listar();
        $sub_total = (float)$data['monto'] * (float)$descuento[0]->porcentaje;
        $data['descuento_id'] = $descuento[0]->id;
        $data['sub_total'] = $sub_total;
        $data['total'] = (float)$data['monto'] + (float)$sub_total;
        // Operacion para realizar el calculo de la disponibilidad (Descuento del monto disponible del cliente)
        $ced = $data['cedula'];
        $cliente = $this->cliente->buscar_cliente($ced);
        $disponibilidad = (float)$data['total'] - (float)$cliente->disponibilidad;

        $this->transaccion->disponibilidad_exist($ced,$disponibilidad);
        $result = $this->factura->add_detalle($data);
        if($result){
            #echo json_encode($data);
            $this->response(array('id'=>$result,'message'=>"Se registro con exito"),200);
        }else{
            $this->response(array('id'=>1,'message'=>"Error"),500);
        }
    }
}
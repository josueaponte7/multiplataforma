<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CHome extends SI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        if ($this->uri->segment(1,0) == TRUE){
            redirect(base_url());
        }else{
            $this->load->view('home');
        }
    }

}
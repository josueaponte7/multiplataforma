<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CMetadata extends SI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('seguridad/metadata');
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('display_errors', 0);

class CAcceso extends SI_Controller {

    private $dir   = 'seguridad/';
    private $files = array('new'=>'acceso');
    private $vista = '';
    private $table = 'access';
    private $titulo = 'Registro Api Rest';

    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->library('controllerlist');
        $this->load->model('seguridad/MAcceso','acceso');
        $this->load->model('seguridad/MUsers','users');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['id']     = $this->modulo->lastId($this->table);
        $datos['lista']  = $this->acceso->listar();
        $datos['users']  = $this->users->listar();
        $datos['directorio'] = $this->libreria->getControllers();
        $datos['token']  = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->acceso->buscar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->acceso->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            /*$data['methods'] = implode(',', $data['methods']);
            $data['users'] = implode(',', $data['users']);*/
            $result = $this->acceso->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $controller_id = "";
            $data = $this->input->post();
            // $data['methods'] = implode(',', $data['methods']);
            // $data['users'] = implode(',', $data['users']);
            $response_data['success']='error';
            $result = $this->acceso->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }

    # Captura de los metodos segun el controlador seleccionado
    public function methods()
    {
        $directorio = $this->input->get('directorio');
        $controller = $this->input->get('controller');
        $this->controllerlist->setControllers($directorio,$controller);
        $methods = $this->controllerlist->getControllers();
        echo json_encode($methods);
    }
}

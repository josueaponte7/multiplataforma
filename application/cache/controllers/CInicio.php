<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CInicio extends SI_Controller
{

    private $dir   = array('sistema' => 'seguridad', 'registro' => 'registro', 'consulta' => 'seguridad');
    private $files = array('inicio' => 'inicio', 'sistema' => 'sistema', 'lista' => 'lista', 'new' => 'tickeks', 'consulta' => 'consulta');
    private $vista = '';
    private $table = 'ticket';

    public function __construct()
    {
        parent::__construct();
        $this->dir   = (object) $this->dir;
        $this->files = (object) $this->files;
    }

    public function index()
    {
        $this->vista           = $this->dir->sistema . '/' . $this->files->sistema;
        $this->template->write_view('content', $this->vista);
        $this->template->render();
    }
}

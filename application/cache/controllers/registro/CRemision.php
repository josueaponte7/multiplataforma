<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CRemision extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'remision');
    private $vista = '';
    private $table = 'mp_remision';
    private $titulo = 'Registro Guia de Remisión';

    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MRemision','remision');
        $this->load->model('registro/MClaveAcceso','clave_acceso');
        $this->load->model('registro/MCliente','cliente');
        $this->load->model('registro/MComprobante','comprobante');
        $this->load->model('registro/MEstadoComprobante','estado');
        $this->load->model('registro/MContribuyente','contribuyente');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['id']     = $this->modulo->lastId($this->table);
        $datos['lista']  = $this->remision->listar();
        $datos['clave_acceso']  = $this->clave_acceso->listar();
        $datos['distribuidor']  = $this->cliente->listar_distribuidor();
        $datos['comprobante']  = $this->comprobante->listar();
        $datos['contribuyente']  = $this->contribuyente->listar();
        $datos['estado']     = $this->estado->listar();
        $datos['token']  = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->remision->buscar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->remision->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $fecha_inicio_transporte = explode(' ', $data['fecha_inicio_transporte']);
            $fecha = date('Y-m-d', strtotime($fecha_inicio_transporte[0]));
            $data['fecha_inicio_transporte'] = $fecha." ".$fecha_inicio_transporte[1];

            $fecha_fin_transporte = explode(' ', $data['fecha_fin_transporte']);
            $fecha = date('Y-m-d', strtotime($fecha_fin_transporte[0]));
            $data['fecha_fin_transporte'] = $fecha." ".$fecha_fin_transporte[1];
            $result = $this->remision->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {

        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $response_data['success']='error';
            $data['fecha_emision'] = date('dmY'); # Fecha de Emision
            $fecha_inicio_transporte = explode(' ', $data['fecha_inicio_transporte']);
            $fecha = date('Y-m-d', strtotime($fecha_inicio_transporte[0]));
            $data['fecha_inicio_transporte'] = $fecha." ".$fecha_inicio_transporte[1];

            $fecha_fin_transporte = explode(' ', $data['fecha_fin_transporte']);
            $fecha = date('Y-m-d', strtotime($fecha_fin_transporte[0]));
            $data['fecha_fin_transporte'] = $fecha." ".$fecha_fin_transporte[1];
            $result = $this->remision->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CProducto extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'producto');
    private $vista = '';
    private $table = 'mp_producto';
    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MProducto','producto');
        $this->load->model('registro/MImpuestoTIce','impuesto');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {
        $datos['id']        = $this->modulo->lastId($this->table);
        $datos['lista']     = $this->producto->listar();
        $datos['impuesto']     = $this->impuesto->listar();
        $datos['token']     = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->producto->buscar($id);
        echo json_encode($row);
    }

    public function buscar_producto()
    {
        $id = $this->input->get('id');
        $row = $this->producto->buscar_producto($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->producto->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            if($data['impuesto_id'] == 0){
                $data['impuesto_id'] = 0;
            }else{
                $data['impuesto_id'] = $data['impuesto_id'];
            }
            $result = $this->producto->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $response_data['success']='error';
            if($data['impuesto_id'] == 0){
                $data['impuesto_id'] = 0;
            }else{
                $data['impuesto_id'] = $data['impuesto_id'];
            }
            $data['cod_identificador'] = str_replace('.', '', $data['cod_identificador']);

            $result = $this->producto->guardar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CAgente extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'agente');
    private $vista = '';
    private $table = 'mp_agente_retencion';
    private $titulo = 'Agente Retención';
    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MAgente','agente');
        $this->load->model('registro/MEmision','emision');
        $this->load->model('registro/MAmbiente','ambiente');
        $this->load->model('registro/MPais','pais');
    }
    public function index()
    {   
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['id']        = $this->modulo->lastId($this->table);
        $datos['lista']     = $this->agente->listar();
        $datos['emision']     = $this->emision->listar();
        $datos['ambiente']     = $this->ambiente->listar();
        $datos['pais']     = $this->pais->listar();
        $datos['token']     = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->agente->buscar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->agente->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('id')) {
            $data = $this->input->post();
            $hora_servicio = explode(' ', $data['hora_servicio']);
            $fecha = date('Y-m-d', strtotime($hora_servicio[0]));
            $data['hora_servicio'] = $fecha." ".$hora_servicio[1];
            $result = $this->agente->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {
        if ($this->input->post('id')) {
            $data = $this->input->post();
            $response_data['success']='error';
            $data['hora_servicio'] = NULL;
            $result = $this->agente->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
}

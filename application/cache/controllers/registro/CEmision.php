<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CEmision extends SI_Controller {

    private $dir   = 'registro/';
    private $files = array('new'=>'emision');
    private $vista = '';
    private $table = 'mp_emision';
    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('registro/MEmision','emision');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {
        $datos['id']        = $this->modulo->lastId($this->table);
        $datos['lista']     = $this->emision->listar();
        $datos['token']     = $this->libreria->token();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();

    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $row = $this->emision->buscar($id);
        echo json_encode($row);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->emision->eliminar($id);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $result = $this->emision->modificar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modifcado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
    public function guardar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $response_data['success']='error';
            $result = $this->emision->guardar($data);
            if($result == 1){
                $response_data['success']='existe';
                $response_data['msg']='<div>Registro existente</div>';
            }else if($result == 2){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro guardado con exito</div>';
            }
        }else{
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }
}

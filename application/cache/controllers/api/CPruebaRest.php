<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'libraries/REST_Controller.php');

class CPruebaRest extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
        echo "HOLA PRUEBA GET";
    }

}

/* End of file  */
/* Location: ./application/controllers/ */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'libraries/REST_Controller.php');

class CRestUser extends REST_Controller {

    //con esto limitamos las consultas y los permisos a la api
    protected $methods = array(
        'users_get' => array('level' => 0),//para acceder a users_get debe tener level 1 y no hay limite de consultas por hora
        'user_get' => array('level' => 0, 'limit' => 10),//user_get sólo level 0, pero máximo 10 consultas por hora
        'posts_user_get' => array('level' => 0, 'limit' => 10),//se necesita level 0 y sólo se pueden hacer 10 consultas por hora
        'new_user_post' => array('level' => 1),//se necesita level 1, no hay limite de peticiones
        'user_post' => array('level' => 1, 'limit' => 5),//se necesita level 1 y 5 peticiones
    );

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('seguridad/MUsers','user');
    }
    // Para obtener datos de todos los usuarios o uno en especifico
    public function users_get() {
        if(!empty($this->get("id"))){
            $id = $this->get("id");
            $user = $this->response($this->user->buscar($id));
        }else{
            $user = $this->response($this->user->listar());
        }
        if($user){
            $this->response($user, REST_Controller::HTTP_OK, 200);
        }else{
            $this->response(NULL, HTTP_NOT_FOUND, 404);
        }
    }

    public function userupdate_post() {
        echo "Metodo actualizar";
    }


}
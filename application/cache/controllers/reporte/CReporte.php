<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CReporte extends SI_Controller
{

    private $dir   = 'reporte/';
    private $files = array('new'=>'reporte','generacion'=>'factura');
    private $vista = '';
    #private $table = 'mp_reporte';
    private $titulo = 'Generación Reporte(s)';

    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('reporte/MReporte','reporte');
        $this->load->model('registro/MContribuyente','factura');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['factura']  = $this->factura->listar();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();
    }

    public function generar_factura()
    {
        $id = $this->input->get('id');
        $datos = $this->reporte->generar_factura($id);
        $this->load->view('reporte/factura', $datos);
    }
}

<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/***
 * File: (Codeigniterapp)/libraries/Controllerlist.php
 *
 * A simple library to list all your controllers with their methods.
 * This library will return an array with controllers and methods
 *
 * The library will scan the "controller" directory and (in case of) one (1) subdirectory level deep
 * for controllers
 *
 * Usage in one of your controllers:
 *
 * $this->load->library('controllerlist');
 * print_r($this->controllerlist->getControllers());
 *
 * @author Peter Prins
 */
define('EXT', '.php');

class ControllerList {

    /**
     * Codeigniter reference
     */
    private $CI;

    /**
     * Array that will hold the controller names and methods
     */
    private $aControllers;

    // Construct
    function __construct() {
        // Get Codeigniter instance
        $this->CI = get_instance();

        // Get all controllers
        // $this->setControllers();
    }

    /**
     * Return all controllers and their methods
     * @return array
     */
    public function getControllers() {
        return $this->aControllers;
    }

    /**
     * Set the array holding the controller name and methods
     */
    public function setControllerMethods($p_sControllerName, $p_aControllerMethods) {
        #$this->aControllers[$p_sControllerName] = $p_aControllerMethods;
        $this->aControllers = $p_aControllerMethods;
    }

    /**
     * Search and set controller and methods.
     */
    public function setControllers($dir='api',$controll) {
        // Loop through the controller directory
        $controller_f = APPPATH . 'controllers/'.$dir;

        if(is_dir($controller_f)) {
            $dirname = basename($controller_f, EXT);
            $subcontroller_f = APPPATH . 'controllers/'.$dirname.'/'.$controll.'.php';
            // echo $subcontroller_f;
            //$subdircontrollername = basename($subcontroller_f, EXT);
            // echo $subdircontrollername;
            if(!class_exists($controll)) {
                $this->CI->load->file($subcontroller_f);
            }else{
                echo 'eror';
            }
            $aMethods = get_class_methods($controll);
            $aUserMethods = array();
            $x = 0;
            foreach($aMethods as $method) {
                if($method != '__construct' && $method != 'get_instance' && $method != '__destruct' && $method != '_remap' && $method != 'response' && $method != 'set_response' && $method != 'options' && $method != 'head' && $method != 'get' && $method != 'post' && $method != 'put' && $method != 'delete' && $method != 'patch' && $method != 'query' && $method != 'validation_errors' && $method != $controll) {
                    $aUserMethods[]["method"] = $method;
                }
                $x = $x  + 1;
            }
             $this->setControllerMethods($controll, $aUserMethods);

        }else{
            echo 'no';
        }

    }
}
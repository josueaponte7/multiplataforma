<?php
require_once 'tcpdf.php';

class MyPDF extends TCPDF
{

    public function Header()
    {
        $this->setJPEGQuality(90);
        $image_file = assets_url('img/tcpdf.jpg');
        //$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', 'B', 20);

        //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    public function Footer()
    {
        date_default_timezone_set('America/Caracas');
        $fecha = "Fecha: " . date("d/m/Y h:i A");
        $this->SetY(-8);

        $this->SetFont('FreeSerif', '', 8);

        $style = array('width' => 0.30, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
        // Page number
        $this->Line(15, 285, 195, 285, $style);
        $this->Cell(35, 0, $fecha, 0, false, 'R', 0, '', 0, false, 'T', 'M');
        $this->Cell(160, 0, 'Página ' . $this->getAliasNumPage() . ' de ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

}

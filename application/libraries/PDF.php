<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/MyPDF.php';

class PDF extends MyPDF
{
    function __construct()
    {
        parent::__construct();
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*
*/
class Paginar
{
    private $ci;
    private $table;
    private $field;
    private $order;
    private $limit;
    public $offset;
    private $uri_segment;
    private $final;
    function __construct($params)
    {
        $this->ci = & get_instance();
        $default  = array('field'=>'id', 'order'=>'asc', 'limit'=> 5, 'offset' => 0, 'uri_segment'=>3);
        $options  = array_merge($default, $params);

        $this->table       = $options['table'];
        $this->field       = $options['field'];
        $this->order       = $options['order'];
        $this->limit       = $options['limit'];
        $this->offset      = $options['offset'];
        $this->uri_segment = $options['uri_segment'];
    }

    public function all()
    {
        $this->offset();
        $this->offset = $this->final == 0 ? 0 : ($this->ci->uri->segment($this->uri_segment)-1)*$this->limit;
        $query  = $this->ci->db->select('*')->order_by($this->field, $this->order)->limit($this->limit, $this->offset)->get($this->table);
        return $query->result();
    }

    public function count()
    {
        return $this->ci->db->count_all_results($this->table);
    }

    public function offset()
    {
        $off    = $this->ci->uri->segment($this->uri_segment);
        $this->final = (isset($off)) ? $this->ci->uri->segment($this->uri_segment) : 0;
        return $this->final;
    }

    public function counter()
    {
        $off = $this->final == 0 ? 1 : ($this->ci->uri->segment($this->uri_segment)-1)*$this->limit+1;
        return $off;

    }
}
<?php

class Prueba
{
    public $cedula = 0;
    public $nombre = '';
    public $fecha_naci = 0;
    public function __construct()
    {
    }
    public function setCedula($cedula)
    {

        $this->cedula = $cedula;
    }
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
    public function setFechaNaci($fecha_naci)
    {
        $this->fecha_naci = $fecha_naci;
    }

    public function format_cedula()
    {
        return number_format($this->cedula,0,'','.');
    }

    public function mayu_nombre()
    {
        return strtoupper($this->nombre);
    }
    public function format_fecha()
    {
        return date('d/m/Y',strtotime($this->fecha_naci));
    }
}

window.url_accion = 'index.php/seguridad/perfil/';
var row_action = 0;
$(document).ready(function() {

    var $folder = $('#folder').val();
    var $file   = $('#file').val();

    window.url_accion = $folder+$file;
    window.url_buscar = url_accion+'/buscar';
    window.url_eliminar = url_accion+'/eliminar';

    var $forms = $("#frmperfil");
    var $tabla = $('#tblperfil');

    var TTable = $tabla.DataTable({
        "pagingType": "full_numbers",
        "language": {
            "url": assets_url('js/es.json')
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "4%", },
        {"sWidth": "100%"},
        {"sWidth": "40%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}
        ],
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "iDisplayLength": 5,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],

    });

    $tabla.on('mouseenter', ' img.eliminar', function () {
        var clase = $(this).hasClass('modificar');
        var titulo = clase ? 'Modificar' : 'Eliminar';
        $(this).tooltip({placement: 'top', title: titulo, html: true}).tooltip('show');
    });

    $forms.validateForm();
    $('input[type="text"], textarea').on({
        keypress: function() {
            $(this).parent('div').removeClass('has-error');
            $(this).parent('div').find('span').css('display','none');
        }
    });


    $('#guardar').click(function(event) {
        if($.validar()){
            var data_send = $('#frmperfil').serialize();
            var $url = url_accion+$(this).data('accion');
            $.guardar($url,data_send,$(this).data('accion'),function(data){
                if(data.success=='ok'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            location.reload();
                        }
                    });
                }
            });
        }
    });


    // console.log(TTable.cell(0,1).data('Hola Mundo'))
    $tabla.on('click','img.mouse',function(){
        var $this = $(this);
        var id    = $this.data('id')
        var ids   = TTable.row($this.closest('tr')).id();
        var row   = TTable.row('#'+id).node();
        var first = $this.attr('class').split(' ')[0];
        row_action = $this.closest('tr').index();
        if(first == 'eliminar'){
            var url = url_accion+'eliminar'
            $.buscar(url,{id:id},first, function(data, textStatus, xhr) {
                if(data.success == 'ok'){
                    bootbox.alert({
                        size:'small',
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            TTable.row($this.parents('tr')).remove().draw();
                            var total = TTable.rows().count();
                            for (var i = 0; i< total; i++) {
                                TTable.cell(i,0).data(i+1).draw();
                            }
                        }
                    });
                }
            },'json');
        }else{
            $url = url_accion+'buscar';
            $.buscar($url,{id:id},first, function(data, textStatus, xhr) {
                $.each(data, function(i, valor) {
                    var tipo = $('#'+i).prop('tagName');
                    if(tipo != undefined){
                        if(tipo.toLowerCase() == 'input'){
                            $('#'+i).val(valor)
                        }else if(tipo.toLowerCase() == 'select'){
                            $('#'+i).select2('val',valor)
                        }
                    }else{
                        va = 1;
                        if(valor == 'f'){
                            va = 0;
                        }
                        $('[name="'+i+'"]').parent('label').removeClass('active');
                        $('[name="'+i+'"][value="'+va+'"]').prop('checked',true).parent('label').addClass('active');
                    }
                });
                $('#guardar').val('Modificar').data('accion','modificar');
                $('#if').val(id)
            },'json');
        }
    });

    $('#tblperfil tbody').on( 'click', 'tr', function (event) {
        // var id = TTable.row(this).id();
        var ind = TTable.row(this).index() ;
        var id = TTable.rows(ind).nodes().to$().attr('data-ids');
        $.buscar(url_buscar,{id:id},'', function(data, textStatus, xhr) {
            $.each(data, function(i, valor) {
                var tipo = $('#'+i).prop('tagName');
                if(tipo != undefined){
                    if(tipo.toLowerCase() == 'input'){
                        $('#'+i).val(valor)
                    }else if(tipo.toLowerCase() == 'select'){
                        $('#'+i).select2('val',valor)
                    }
                }else{
                    va = 1;
                    if(valor == 'f'){
                        va = 0;
                    }
                    $('[name="'+i+'"]').parent('label').removeClass('active');
                    $('[name="'+i+'"][value="'+va+'"]').prop('checked',true).parent('label').addClass('active');
                }
            });
            $('#guardar').val('Modificar').data('accion','modificar');
            $('#if').val(id)
        },'json');
    });
    $('#cancelar').click( function () {
        $('input[type="text"]').val('')
        $('input[name="activo"]').prop('checked',false).parent('label').removeClass('active');
        $('input[name="activo"][value="1"]').prop('checked',true).parent('label').addClass('active');
        $('select').select2('val',0);
        $('#guardar').val('Guardar');
        TTable.search( '' ).columns().search( '' ).draw();
    });
    // $('.input-number').inputNumber();
});

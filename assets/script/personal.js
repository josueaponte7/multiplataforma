
$(document).ready(function() {
    var $folder = $('#folder').val();
    var $file = $('#file').val();
    $("#fecha_ingreso,#fecha_egreso").mask("99-99-9999", {placeholder: "DD-MM-YYYY"});
    $(".telefono").mask("(9999) 999-99-99", {placeholder: "(0000) 000-00-00"});
    $(".duracion").mask("99");

    window.url_accion = $folder+$file+"/";
    window.url_buscar = url_accion+'buscar';
    window.url_eliminar = url_accion+'eliminar';

    var $tabla      = $('.tabla');
    var $formulario = $(".formulario");
    var $guardar    = $('#guardar');

    var Tabla = $tabla.DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "4%", },
        {"sWidth": "10%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "100%"},
        {"sWidth": "4%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        ]
    });

    var TablaNuevoE = $("#tblestrealizados").DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "12%", },
        {"sWidth": "30%"},
        {"sWidth": "30%"},
        {"sWidth": "5%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        ]
    });

    var TablaNuevoR = $("#tblrefelaboral").DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "5%", },
        {"sWidth": "100%"},
        {"sWidth": "5%"},
        {"sWidth": "5%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        ]
    });

    var TablaNuevoEmp = $("#tblempresalab").DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "20%", },
        {"sWidth": "80%"},
        {"sWidth": "5%"},
        {"sWidth": "5%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        ]
    });

    $.uploadPreview({
        input_field: "#foto_perfil",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Cambio",   // Default: Choose File
        label_selected: "Imagen",  // Default: Change File
        no_label: true                 // Default: false
    });

    $('.modificar').tooltip({
        html: true,
        placement: 'top',
        title: 'Modificar'
    });
    $('.eliminar').tooltip({
        html: true,
        placement: 'top',
        title: 'Eliminar'
    });

    $formulario.validateForm();

    $guardar.click(function(event) {
        if($.validar()){
            var data_send = $formulario.serialize();
            var $url = url_accion+$(this).data('accion');
            $.guardar($url,data_send,$(this).data('accion'),function(data){
                if(data.success=='ok'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            location.reload();
                        }
                    });
                } else if(data.success=='existe'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                        }
                    });
                }
            });
        }
    });

    $tabla.on('click', 'img.modificar', function() {
        var id = $(this).closest('tr').attr('id');
        $.get(base_url(url_buscar), {id: id}, function(res, textStatus, xhr) {
            $("#id").val(res.id);
            $("#salario").val(res.salario);
            $("#cedula_id").select2('val',[res.cedula_id]);
            $("#cargo_id").select2('val',[res.cargo_id]);
            $("#tiempo_contracto").val(res.tiempo_contracto);
            fecha_ingreso = res.fecha_ingreso.split('-');
            fecha_ingreso = fecha_ingreso[2]+"-"+fecha_ingreso[1]+"-"+fecha_ingreso[0];
            $("#fecha_ingreso").val(fecha_ingreso);
            if(res.fecha_egreso != null){
                fecha_egreso = res.fecha_egreso.split('-');
                fecha_egreso = fecha_egreso[2]+"-"+fecha_egreso[1]+"-"+fecha_egreso[0];
                $("#fecha_egreso").val(fecha_egreso);
            }
            $guardar.attr('data-accion','modificar').val('Modificar');
        },'json');
    });

    function estudio_ajax(id){
        // Carga de datos de los listados
        $.get(base_url("registro/personal/estudio_ajax"), {id: id}, function(res, textStatus, xhr) {
            $.each(res, function (i, valor) {
                var array_val = []; // Array inicial
                var select_nivel = {0:'Seleccione',1:'Primaria',2:'Secundaria', 3:'Universitario(a)', 4:'Diversificado'};
                var profesion = [];
                var total = TablaNuevoE.rows().count();
                nivel = "<select data-id='"+valor.id+"' id='nivel_"+total+"' class='form-control nivel' style='width:100%;height:50px;' class='form-control'></select>";
                profesion = "<select id='profesion_"+total+"' class='form-control profesion' style='width:400px;height:50px;'></select>";

                var instituto = "<textarea id='instituto_"+total+"' style='width:100%;height:50px;' data-text='upper' class='form-control'>"+valor.instituto+"</textarea>";
                var eliminar = "<img class='eliminar_estudio_realizado' src='" + assets_url('img/datatable/eliminar.png') + "' style='cursor:pointer;' title='Eliminar elemento'>";
                array_val.push(nivel);
                array_val.push(profesion);
                array_val.push(instituto);
                array_val.push(eliminar);
                TablaNuevoE.row.add(array_val).draw();
                TablaNuevoE.rows(i).nodes().to$().attr("data-id", valor.id);
                $.getJSON(base_url("registro/profesion/ajax_lista"), function(res, textStatus, xhr) {
                    var $option = [];
                    $option.push({id:"0",text:'Seleccione'})
                    $.each(res, function (i, valor) {
                        item = { id: valor.id, text: valor.descripcion};
                        $option.push(item);
                    });
                    $("select.profesion").select2({data: $option});
                    $("select#profesion_"+total).select2('val',[valor.profesion_id]);
                });

                var options = [];
                $.each(select_nivel, function(key, value) {
                    options.push($("<option/>", {
                        value: key,
                        text: value
                    }));
                });
                $('#nivel_'+total).append(options);
                $("select#nivel_"+total).val([valor.nivel]);
            });
        },'json');
    }

    function referencia_ajax(id){
        $.get(base_url("registro/personal/referencia_ajax"), {id: id}, function(res, textStatus, xhr) {
            $.each(res, function (i, valor) {
                var profesion = [];
                var array_val = [];
                var total = TablaNuevoR.rows().count();
                profesion = "<select id='profesion_"+total+"' class='form-control profesion' style='width:100%;'></select>";
                var nombres = "<textarea id='nombres_"+total+"' style='width:100%;height:50px;' row='2' data-text='upper' class='form-control'>"+valor.nombres+"</textarea>";
                var telefono = "<input id='telefono_"+total+"' style='width:100%;height:50px;' data-text='upper' class='form-control telefono' value='"+valor.telefono+"'/>";
                var eliminar = "<img class='eliminar_referencia_lab' src='" + assets_url('img/datatable/eliminar.png') + "' style='cursor:pointer;' title='Eliminar elemento'>";
                array_val.push(profesion);
                array_val.push(nombres);
                array_val.push(telefono);
                array_val.push(eliminar);
                TablaNuevoR.row.add(array_val).draw();
                TablaNuevoR.rows(i).nodes().to$().attr("data-id", valor.id);
                $.getJSON(base_url("registro/profesion/ajax_lista"), function(res, textStatus, xhr) {
                    var $option = [];
                    $option.push({id:"0",text:'Seleccione'})
                    $.each(res, function (i, valor) {
                        item = { id: valor.id, text: valor.descripcion};
                        $option.push(item);
                    });
                    $("select.profesion").select2({data: $option});
                    $("select#profesion_"+total).select2('val',[valor.profesion_id]);
                });
            });
        },'json');
    }

    function empresa_ajax(id){
        $.get(base_url("registro/personal/empresa_ajax"), {id: id}, function(res, textStatus, xhr) {
            $.each(res, function (i, valor) {
                var profesion = [];
                var array_val = [];
                var total = TablaNuevoEmp.rows().count();
                cargo = "<input id='cargo_"+total+"' style='width:100%;height:45px;' data-text='upper' class='form-control' value='"+valor.cargo+"'/>";
                var empresa = "<textarea id='empresa_"+total+"' style='width:100%;height:45px;' row='2' data-text='upper'>"+valor.nom_empresa+"</textarea>";
                var duracion = "<input id='duracion_"+total+"' style='width:100%;height:45px;' data-text='upper' class='form-control duracion' value='"+valor.duracion+"'/>";
                var eliminar = "<img class='eliminar_empresa' src='" + assets_url('img/datatable/eliminar.png') + "' style='cursor:pointer;' title='Eliminar elemento'>";
                array_val.push(cargo);
                array_val.push(empresa);
                array_val.push(duracion);
                array_val.push(eliminar);
                TablaNuevoEmp.row.add(array_val).draw();
                TablaNuevoEmp.rows(i).nodes().to$().attr("data-id", valor.id);
            });
        },'json');
    }


    $tabla.on('click', 'img.eliminar', function() {
        var id = $(this).closest('tr').attr('id');

        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el registro?</div>',
            callback: function(result){
                if(result){
                    $.get(base_url(url_eliminar), {id: id}, function(data, textStatus, xhr) {
                        if(data.success == 'ok'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                    location.reload();
                                }
                            });
                        } else if(data.success == 'existe'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                }
                            });
                        }
                    },'json');
                }else{
                    $('#cancelar').trigger('click');
                }
            }
        });
    });

    // Eliminar elemento de la lista de estudio realizado(s)
    $('table#tblestrealizados').on('click', 'img.eliminar_estudio_realizado', function () {
        var ids = $(this).closest('tr').attr('data-id');
        var i = $(this).index();
        var elemento = $(this);
        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el estudio realizado?</div>',
            callback: function(result){
                if(result){
                    if(ids != 0){
                        $.get(base_url("registro/personal/eliminar_estudio"), {id: ids}, function(data, textStatus, xhr) {
                            TablaNuevoE.row(i).remove().draw();
                        });
                    }else{
                        TablaNuevoE.row(i).remove().draw();
                    }
                }
            }
        });
    });

    // Eliminar elemento de empresa
    $('table#tblempresalab').on('click', 'img.eliminar_empresa', function () {
        var ids = $(this).closest('tr').attr('data-id');
        var i = $(this).index();
        var elemento = $(this);
        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar la empresa?</div>',
            callback: function(result){
                if(result){
                    if(ids != 0){
                        $.get(base_url("registro/personal/eliminar_empresa"), {id: ids}, function(data, textStatus, xhr) {
                            TablaNuevoEmp.row(i).remove().draw();
                        });
                    }else{
                        TablaNuevoEmp.row(i).remove().draw();
                    }
                }
            }
        });
    });

    // Eliminar elemento de la lista de referencia laboral
    $('table#tblrefelaboral').on('click', 'img.eliminar_referencia_lab', function () {
        var ids = $(this).closest('tr').attr('data-id');
        var i = $(this).index();
        var elemento = $(this);
        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar la referencia laboral?</div>',
            callback: function(result){
                if(result){
                    if(ids != 0){
                        $.get(base_url("registro/personal/eliminar_referencia"), {id: ids}, function(data, textStatus, xhr) {
                            TablaNuevoR.row(i).remove().draw();
                        });
                    }else{
                        TablaNuevoR.row(i).remove().draw();
                    }
                }
            }
        });
    });

    // Añadir estudio realizado
    $('button.btn-nuevo-estudio').click(function(){
        var array_val = []; // Array inicial
        var select_nivel = {0:'Seleccione',1:'Primaria',2:'Secundaria', 3:'Universitario(a)', 4:'Diversificado'};
        var profesion = [];
        var total = TablaNuevoE.rows().count();
        nivel = "<select data-id='0' id='nivel_"+total+"' class='form-control nivel' style='width:100%;height:50px;' class='form-control'></select>";
        profesion = "<select id='profesion_"+total+"' class='form-control profesion' style='width:400px;height:50px;'></select>";

        var instituto = "<textarea id='instituto_"+total+"' style='width:100%;height:50px;' data-text='upper' class='form-control'></textarea>";
        var eliminar = "<img class='eliminar_estudio_realizado' src='" + assets_url('img/datatable/eliminar.png') + "' style='cursor:pointer;' title='Eliminar elemento'>";
        array_val.push(nivel);
        array_val.push(profesion);
        array_val.push(instituto);
        array_val.push(eliminar);
        TablaNuevoE.row.add(array_val).draw();
        $.getJSON(base_url("registro/profesion/ajax_lista"), function(res, textStatus, xhr) {
            var $option = [];
            $option.push({id:"0",text:'Seleccione'})
            $.each(res, function (i, valor) {
                item = { id: valor.id, text: valor.descripcion};
                $option.push(item);
            });
            $("select.profesion").select2({data: $option});
        });

        var options = [];
        $.each(select_nivel, function(key, value) {
            options.push($("<option/>", {
                value: key,
                text: value
            }));
        });
        $('.nivel').append(options);
    });

    // AñadirReferencia Laboral
    $('button.btn-nuevo-referencia').click(function(){
        var profesion = [];
        var array_val = [];
        var total = TablaNuevoR.rows().count();
        profesion = "<select id='profesion_"+total+"' class='form-control profesion' style='width:100%;'></select>";
        var nombres = "<textarea id='nombres_"+total+"' style='width:100%;height:50px;' row='2' data-text='upper' class='form-control'></textarea>";
        var telefono = "<input id='telefono_"+total+"' style='width:100%;height:50px;' data-text='upper' class='form-control telefono'/>";
        var eliminar = "<img class='eliminar_referencia_lab' src='" + assets_url('img/datatable/eliminar.png') + "' style='cursor:pointer;' title='Eliminar elemento'>";
        array_val.push(profesion);
        array_val.push(nombres);
        array_val.push(telefono);
        array_val.push(eliminar);
        TablaNuevoR.row.add(array_val).draw();
        $.getJSON(base_url("registro/profesion/ajax_lista"), function(res, textStatus, xhr) {
            var $option = [];
            $option.push({id:"0",text:'Seleccione'})
            $.each(res, function (i, valor) {
                item = { id: valor.id, text: valor.descripcion};
                $option.push(item);
            });
            $("select.profesion").select2({data: $option});
        });
    });

    // Añadir nueva Empresa
    $('button.btn-nuevo-empresa').click(function(){
        var profesion = [];
        var array_val = [];
        var total = TablaNuevoEmp.rows().count();
        cargo = "<input id='cargo_"+total+"' style='width:100%;height:45px;' data-text='upper' class='form-control'/>";
        var empresa = "<textarea id='empresa_"+total+"' style='width:100%;height:45px;' row='2' data-text='upper'></textarea>";
        var duracion = "<input id='duracion_"+total+"' style='width:100%;height:45px;' data-text='upper' class='form-control duracion'/>";
        var eliminar = "<img class='eliminar_empresa' src='" + assets_url('img/datatable/eliminar.png') + "' style='cursor:pointer;' title='Eliminar elemento'>";
        array_val.push(cargo);
        array_val.push(empresa);
        array_val.push(duracion);
        array_val.push(eliminar);
        TablaNuevoEmp.row.add(array_val).draw();
    });

    $('#cancelar').click(function(){
        var $count = Tabla.rows().data().length;
        $('#id').val($count+1);
        $("select").select2('val',[0]);
        $('.reset').val('');
        $('#guardar').attr('data-accion','guardar').val('Guardar');
    });
});

$.estudio = function(){
    datos = "";
    var campos = [];
    $('#tblestrealizados tbody tr').each(function (i) {
        var nivel = $("#nivel_"+i).val();
        var profesion = $("#profesion_"+i).val();
        var instituto = $("#instituto_"+i).val();
        var personal_id = $("#id").val();
        datos = nivel + ";" + profesion + ";" + instituto+ ";" + personal_id;
        campos.push(datos);
    });
    $.get(base_url("registro/personal/estudio"), {'campos[]': campos}, function(data, textStatus, xhr) {

    },'json');
};

$.referencia = function(){
    datos = "";
    var campos = [];
    $('#tblestrealizados tbody tr').each(function (i) {
        var profesion = $("#profesion_"+i).val();
        var nombres = $("#nombres_"+i).val();
        var telefono = $("#telefono_"+i).val();
        var personal_id = $("#id").val();
        datos = profesion + ";" + nombres + ";" + telefono+ ";" + personal_id;
        campos.push(datos);
    });
    $.get(base_url("registro/personal/referencia"), {'campos[]': campos}, function(data, textStatus, xhr) {

    },'json');
};

$.empresa = function(){
    datos = "";
    var campos = [];
    $('#tblrefelaboral tbody tr').each(function (i) {
        var cargo = $("#cargo_"+i).val();
        var empresa = $("#empresa_"+i).val();
        var duracion = $("#duracion_"+i).val();
        var personal_id = $("#id").val();
        datos = cargo + ";" + empresa + ";" + duracion+ ";" + personal_id;
        campos.push(datos);
    });
    $.get(base_url("registro/personal/empresa"), {'campos[]': campos}, function(data, textStatus, xhr) {
        location.reload();
    },'json');
};

















$(document).ready(function() {

    var $folder = $('#folder').val();
    var $file = $('#file').val();

    window.url_accion = $folder+$file+"/";
    window.url_buscar = url_accion+'buscar';
    window.url_methods = url_accion+'methods';
    window.url_eliminar = url_accion+'eliminar';

    var $tabla      = $('.tabla');
    var $formulario = $(".formulario");
    var $guardar    = $('#guardar');

    var Tabla = $tabla.DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
            "buttons": {
                copyTitle: 'Copiar Registros',
                // copyKeys: 'Pulse <i> Ctrl </ i> o <i> \u2318 </ i> + <i> C </ i> para copiar datos de la tabla en el portapapeles. <br> cancelar, haga clic en este mensaje o pulse Esc.',
                copySuccess: {
                    _: 'Copiados %d registros',
                    1: 'Copiado 1 registro'
                }
            }
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "4%", },
        {"sWidth": "40%"},
        {"sWidth": "100%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}
        ],
        dom: "Bfrtip",
        buttons: [
        {
            text:'Copiar',
            extend: "copy",
            className: "btn-sm",
            fieldSeparator: ';',
            title: 'Delitos',
            exportOptions: {
                columns: [ 0, 1 ]
            },

        },
        {
            extend: "csv",
            className: "btn-sm",
            fieldSeparator: ';',
            title: 'Delitos',
            exportOptions: {
                columns: [ 0, 1 ],
            }
        },
        {
            extend: "excel",
            className: "btn-sm",
            title: 'Delitos',
        },
        {
            extend: "pdf",
            className: "btn-sm",
            title: 'Delitos',
            orientation: 'landscape',
            pageSize: 'LEGAL',
            exportOptions: {
                columns: [ 0, 1 ]
            },

        },
        {
            text:'Imprimir', extend: "print", className: "btn-sm",
            exportOptions: {
                columns: [ 0, 1 ]
            }
        },
        ]
    });

    $('.modificar').tooltip({
        html: true,
        placement: 'top',
        title: 'Modificar'
    });
    $('.eliminar').tooltip({
        html: true,
        placement: 'top',
        title: 'Eliminar'
    });

    $formulario.validateForm();

    $guardar.click(function(event) {
        if($.validar()){
            var methods = $("#methods option:selected").map(function () {
                return $(this).text();
            }).get().join(',');

            var users = $("#users option:selected").map(function () {
                return $(this).val();
            }).get().join(',');

            var data_send = $formulario.serialize()+'&'+$.param({methods:methods,users:users});
            var $url = url_accion+$(this).data('accion');

            $.guardar($url,data_send,$(this).data('accion'),function(data){
                if(data.success=='ok'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            location.reload();
                        }
                    });
                } else if(data.success=='existe'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                        }
                    });
                }
            });
        }
    });

    $("#controllers").on('change',function(event, param) {
        var $controller = $(this).val();
        var url = "api";
        var $url = $.base_url()+ url + "/"+$(this).find('option').filter(':selected').text();
        $("#url").val($url);
        $('#methods').find('option:gt(0)').remove().end().select2('val', "");
        $.get(base_url(url_methods), {'controller': $controller, 'directorio': 'api'}, function(res, textStatus, xhr) {
            var $option = [];
            $.each(res, function (i, valor) {
                controller = valor.method.split('_');
                var item = { id: controller[0], text: controller[0]};
                $option.push(item);
            });

            $("#methods").select2({data: $option});
            if(typeof param !== 'undefined'){
                var arrayArea = param.split(',');
                $('#methods').select2('val',[arrayArea]);
            }
        },'json');
    });

    $tabla.on('click', 'img.modificar', function() {
        var id = $(this).closest('tr').attr('id');
        $.get(base_url(url_buscar), {id: id}, function(res, textStatus, xhr) {
            $("#id").val(res.id);
            $('#all_access').select2('val',[res.all_access])
            $('#controllers').select2('val',[res.controllers])
            var arrayArea = res.users.split(',');
            $('#users').select2('val',[arrayArea]);

            /*$.each(res, function (i, valor) {
                var tipo = $('#' + i).prop('tagName');
                if (tipo !== undefined) {
                    if (tipo.toLowerCase() == 'input') {
                        $('#' + i).val(valor);
                    } else if (tipo.toLowerCase() == 'select') {
                        $('#' + i).select2('val',[valor]);
                    }else if (tipo.toLowerCase() == 'textarea') {
                        $('#' + i).val(valor);
                    }
                } else {
                    var va = 1;
                    if (valor == 'f') {
                        var va = 0;
                    }
                    $('[name="' + i + '"]').parent('label').removeClass('active');
                    $('[name="' + i + '"][value="' + va + '"]').prop('checked', true).parent('label').addClass('active');
                }
            });*/

            $("#controllers").trigger('change',res.methods);
            $guardar.attr('data-accion','modificar').val('Modificar');
        },'json');

    });


    $tabla.on('click', 'img.eliminar', function() {
        var id = $(this).closest('tr').attr('id');

        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el registro?</div>',
            callback: function(result){
                if(result){
                    $.get(base_url(url_eliminar), {id: id}, function(data, textStatus, xhr) {
                        if(data.success == 'ok'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                    location.reload();
                                }
                            });
                        } else if(data.success == 'existe'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                }
                            });
                        }
                    },'json');
                }else{
                    $('#cancelar').trigger('click');
                }
            }
        });
    });

    $('#cancelar').click(function(){
        var $count = Tabla.rows().data().length;
        $('#id').val($count+1);
        $("select").select2('val',[0]);
        $('#codigo,#impuesto').val('');
        $('#guardar').attr('data-accion','guardar').val('Guardar');
    });
});
$(document).ready(function() {

    var $folder = $('#folder').val();
    var $file = $('#file').val();
    $("#fecha_ingreso,#fecha_nac,#desde,#hasta,#fecha").mask("99-99-9999", {placeholder: "DD-MM-YYYY"});
    $(".telefono").mask("(9999) 999-99-99", {placeholder: "(0000) 000-00-00"});
    $(".duracion").mask("99");

    window.url_accion = $folder+$file+"/";
    window.url_buscar = url_accion+'buscar';
    window.url_eliminar = url_accion+'eliminar';

    var $tabla      = $('.tabla');
    var $formulario = $("#frmaspirante");
    var $guardar    = $('#guardar');

    $.uploadPreview({
        input_field: "#foto_perfil",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Cambio",   // Default: Choose File
        label_selected: "Imagen",  // Default: Change File
        no_label: true                 // Default: false
    });

    $formulario.validateForm();

    $guardar.click(function(event) {

        if($.validar()){

            var $url = 'aspirante/'+$(this).data('accion');
            var data_send = new FormData($("#frmaspirante")[0]);
            $.ajax({
                url: $url,
                type: 'POST',
                cache: false,
                data: data_send,
                processData: false,
                contentType: false,
                dataType: "json"
            }).done(function (data) {
                if (data.success == 'ok') {
                    bootbox.alert({
                        size: 'small',
                        closeButton: false,
                        message: data.msg,
                        callback: function (result) {
                            location.reload();
                        }
                    });
                } else if(data.success=='existe'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                        }
                    });
                }
            });
        }
    });
});
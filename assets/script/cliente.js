window.url_accion = 'registro/cliente/';
$(document).ready(function() {
    $("#telefono").numeric(); //Valida solo permite valores numericos
    var Tabla = $('.tabla').DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
            "buttons": {
                copyTitle: 'Copiar Registros',
                // copyKeys: 'Pulse <i> Ctrl </ i> o <i> \u2318 </ i> + <i> C </ i> para copiar datos de la tabla en el portapapeles. <br> cancelar, haga clic en este mensaje o pulse Esc.',
                copySuccess: {
                    _: 'Copiados %d registros',
                    1: 'Copiado 1 registro'
                }
            }
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "4%", },
        {"sWidth": "40%"},
        {"sWidth": "100%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}
        ],
        dom: "Bfrtip",
        buttons: [
        {
            text:'Copiar',
            extend: "copy",
            className: "btn-sm",
            fieldSeparator: ';',
            title: 'Cliente',
            exportOptions: {
                columns: [ 0, 1 ]
            },

        },
        {
            extend: "csv",
            className: "btn-sm",
            fieldSeparator: ';',
            title: 'Cliente',
            exportOptions: {
                columns: [ 0, 1 ],
            }
        },
        {
            extend: "excel",
            className: "btn-sm",
            title: 'Cliente',
            // action: function ( e, dt, node, config ) {
            //     alert('Hola');
            //     return true;
            // },
        },
        {
            extend: "pdf",
            className: "btn-sm",
            title: 'Cliente',
            orientation: 'landscape',
            pageSize: 'LEGAL',
            exportOptions: {
                columns: [ 0, 1 ]
            },
            // action: function ( e, dt, node, config ) {
            //     alert('Hola');
            //     return true;
            // },
        },
        {
            text:'Imprimir', extend: "print", className: "btn-sm",
            exportOptions: {
                columns: [ 0, 1 ]
            }
        },
        ]
    });

    $('.modificar').tooltip({
        html: true,
        placement: 'top',
        title: 'Modificar'
    });

    $("#frmcliente").validateForm();

    $('#guardar').click(function(event) {
        if($.validar()){
            var data_send = $('#frmcliente').serialize();
            var $url = url_accion+$(this).data('accion');
            $.guardar($url,data_send,$(this).data('accion'),function(data){
                if(data.success=='ok'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            location.reload();
                        }
                    });
                } else if(data.success=='existe'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                        }
                    });
                }
            });
        }
    });

    $('.tabla').on('click', 'img.modificar', function() {
        var id = $(this).closest('tr').attr('id');

        $.get(base_url('registro/cliente/buscar'), {id: id}, function(res, textStatus, xhr) {
            $.each(res, function (i, valor) {
                var tipo = $('#' + i).prop('tagName');
                if (tipo !== undefined) {
                    if (tipo.toLowerCase() == 'input') {
                        $('#' + i).val(valor);
                    } else if (tipo.toLowerCase() == 'select') {
                        $('#' + i).select2('val',[valor]);
                    }else if (tipo.toLowerCase() == 'textarea') {
                        $('#' + i).val(valor);
                    }
                } else {
                    var va = 1;
                    if (valor == 'f') {
                        var va = 0;
                    }
                    $('[name="' + i + '"]').parent('label').removeClass('active');
                    $('[name="' + i + '"][value="' + va + '"]').prop('checked', true).parent('label').addClass('active');
                }
            });
            $('#guardar').attr('data-accion','modificar').val('Modificar');
        },'json');
    });


    $('.tabla').on('click', 'img.eliminar', function() {
        var id = $(this).closest('tr').attr('id');

        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el registro?</div>',
            callback: function(result){
                if(result){
                    $.get(base_url('registro/cliente/eliminar'), {id: id}, function(data, textStatus, xhr) {
                        if(data.success == 'ok'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                    location.reload();
                                }
                            });
                        } else if(data.success == 'existe'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                }
                            });
                        }
                    },'json');
                }else{
                    $('#cancelar').trigger('click');
                }
            }
        });
    });

    $('#identificacion,#cedula').change(function(){
        var cliente = $("#tipo_cliente_id").val();
        var cedula = $("#cedula").val();
        var identificacion = $(this).val();
        if(cliente == 0){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Seleccione una Categoria del Cliente",
                callback: function(result){
                    $("#tipo_cliente_id").select2('open');
                }
            });
        } else if(cliente == 1 && identificacion.length != 13){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Indique un formato correcto para la Categoria (RUC)",
                callback: function(result){
                }
            });
        } else if(cliente == 2 && cedula.length != 10){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Indique un formato correcto para la Categoria (Cédula)",
                callback: function(result){
                }
            });
        } else if(cliente == 3 && identificacion.length != 13){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Indique un formato correcto para la Categoria (Pasaporte)",
                callback: function(result){
                }
            });
        } else if(cliente == 4 && identificacion.length != 13){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Indique un formato correcto para la Categoria (Venta al Consumidor Final)",
                callback: function(result){
                }
            });
        } else if(cliente == 5 && identificacion.length < 5){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Indique un formato correcto para la Categoria (Identificación del Exterior)",
                callback: function(result){
                }
            });
        } else if(cliente == 6 && identificacion.length != 7){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Indique un formato correcto para la Categoria (Placa)",
                callback: function(result){
                }
            });
        }
    });

    $('#categoria_cliente_id').change(function(event) {
        var $categoria_cliente = $(this).val();
        if($categoria_cliente == 2){
            $("#placa_guia_remision").prop("readonly",false);
        }else{
            $("#placa_guia_remision").val("");
            $("#placa_guia_remision").prop("readonly",true);
        }
    });
    
    $("#cedula").keypress(function(event) {
        var cedula = $(this).val();
        if (cedula > 0){
            var codigo = $("#id_codigo").val();
            $("#identificacion").val(cedula+""+codigo);
        }else{
            $("#identificacion").val("");
        }
        
    });


    $('#cancelar').click(function(){
        var $count = Tabla.rows().data().length;
        $('#id').val($count+1);
        $("select").select2('val',[0]);
        $("textarea,input:text").val("");
        $('#guardar').attr('data-accion','guardar').val('Guardar');
    });
});
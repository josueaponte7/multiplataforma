$(document).ready(function($) {

    $('#remember').prettyCheckable({
        color: 'red'
    });
    $('input[type="text"], textarea, input[type="password"]').on({
        keypress: function() {
            $(this).removeClass('error');
        },
        blur:function(){
            $(this).removeClass('error');
        }
    });

    $('#login').click(function(event) {
        if($('#user').val() === null || $('#user').val().length === 0 || /^\s+$/.test($('#user').val())){
            $('#user').addClass('error').focus();
        }else if($('#password').val() === null || $('#password').val().length === 0 || /^\s+$/.test($('#password').val())){
            $('#password').addClass('error').focus();
        }else{
            var data_send = $('#frmlogin').serialize();
            $.post(base_url('seguridad/users/validatelogin'),data_send, function(data, textStatus, xhr) {
                if(data.status == 'success'){
                    //window.location=base_url('CInicio');
                }else{
                    bootbox.alert({
                        size:'small',
                        closeButton :false,
                        message: '<div class="text-danger" style="text-aling:center">Usuario o Clave incorrecto!</div>',
                    });
                }
            },'json');
        }

    });

    $(document).keypress(function(e){
        if(e.keyCode == 13){
            $('#login').trigger('click');
        }
    });
});


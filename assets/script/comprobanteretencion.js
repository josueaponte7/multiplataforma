
$(document).ready(function() {

    var $folder = $('#folder').val();
    var $file = $('#file').val();

    window.url_accion = $folder+$file+"/";
    window.url_buscar = url_accion+'/buscar';
    window.url_eliminar = url_accion+'/eliminar';

    var $tabla      = $('.tabla');
    var $formulario = $(".formulario");
    var $guardar    = $('#guardar');

    var Tabla = $tabla.DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
            "buttons": {
                copyTitle: 'Copiar Registros',
                // copyKeys: 'Pulse <i> Ctrl </ i> o <i> \u2318 </ i> + <i> C </ i> para copiar datos de la tabla en el portapapeles. <br> cancelar, haga clic en este mensaje o pulse Esc.',
                copySuccess: {
                    _: 'Copiados %d registros',
                    1: 'Copiado 1 registro'
                }
            }
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "4%", },
        {"sWidth": "40%"},
        {"sWidth": "100%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}
        ],
        dom: "Bfrtip",
        buttons: [
        {
            text:'Copiar',
            extend: "copy",
            className: "btn-sm",
            fieldSeparator: ';',
            title: 'Comprobante Retención',
            exportOptions: {
                columns: [ 0, 1 ]
            },

        },
        {
            extend: "csv",
            className: "btn-sm",
            fieldSeparator: ';',
            title: 'Comprobante Retención',
            exportOptions: {
                columns: [ 0, 1 ],
            }
        },
        {
            extend: "excel",
            className: "btn-sm",
            title: 'Comprobante Retención',
            // action: function ( e, dt, node, config ) {
            //     alert('Hola');
            //     return true;
            // },
        },
        {
            extend: "pdf",
            className: "btn-sm",
            title: 'Comprobante Retención',
            orientation: 'landscape',
            pageSize: 'LEGAL',
            exportOptions: {
                columns: [ 0, 1 ]
            },
            // action: function ( e, dt, node, config ) {
            //     alert('Hola');
            //     return true;
            // },
        },
        {
            text:'Imprimir', extend: "print", className: "btn-sm",
            exportOptions: {
                columns: [ 0, 1 ]
            }
        },
        ]
    });

    var TDetalles = $("#tbldetalles").DataTable({
        "bFilter" : false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "language": {
            "url": assets_url('js/es.json'),
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "10%", },
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        ]
    });

    $('.modificar').tooltip({
        html: true,
        placement: 'top',
        title: 'Modificar'
    });
    $('.eliminar').tooltip({
        html: true,
        placement: 'top',
        title: 'Eliminar'
    });

    $formulario.validateForm();

    $guardar.click(function(event) {
        if($.validar()){
            var data_send = $formulario.serialize();
            var $url = url_accion+$(this).data('accion');
            $.guardar($url,data_send,$(this).data('accion'),function(data){
                if(data.success=='ok'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            location.reload();
                        }
                    });
                } else if(data.success=='existe'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                        }
                    });
                }
            });
        }
    });

    $tabla.on('click', 'img.modificar', function() {
        var id = $(this).closest('tr').attr('id');

        $.get(base_url(url_buscar), {id: id}, function(res, textStatus, xhr) {
            $.each(res, function (i, valor) {
                var tipo = $('#' + i).prop('tagName');
                if (tipo !== undefined) {
                    if (tipo.toLowerCase() == 'input') {
                        $('#' + i).val(valor);
                    } else if (tipo.toLowerCase() == 'select') {
                        $('#' + i).select2('val',[valor]);
                    }else if (tipo.toLowerCase() == 'textarea') {
                        $('#' + i).val(valor);
                    }
                } else {
                    var va = 1;
                    if (valor == 'f') {
                        var va = 0;
                    }
                    $('[name="' + i + '"]').parent('label').removeClass('active');
                    $('[name="' + i + '"][value="' + va + '"]').prop('checked', true).parent('label').addClass('active');
                }
            });
            $guardar.attr('data-accion','modificar').val('Modificar');
        },'json');
    });


    $tabla.on('click', 'img.eliminar', function() {
        var id = $(this).closest('tr').attr('id');

        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el registro?</div>',
            callback: function(result){
                if(result){
                    $.get(base_url(url_eliminar), {id: id}, function(data, textStatus, xhr) {
                        if(data.success == 'ok'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                    location.reload();
                                }
                            });
                        } else if(data.success == 'existe'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                }
                            });
                        }
                    },'json');
                }else{
                    $('#cancelar').trigger('click');
                }
            }
        });
    });

    $('#impuesto_id,#base_imponible').change(function(e){
        var base_imponible = $(this).val();
        var rentencion_imp = $("#impuesto_id").val();
        if(rentencion_imp != 0){
            var impuesto = $("#impuesto_id").find('option').filter(':selected').text();
            var impuesto = impuesto.split('(');
            var impuesto = impuesto[1].split(')');
            var impuesto = impuesto[0].split('%');
            var impuesto_g = impuesto[0];
            total = parseFloat(impuesto_g) * parseFloat(base_imponible);
            $("#total").val(total);
        }else{
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Indique el Impuesto de Retención",
                callback: function(result){
                    $("#impuesto_id").select2('open');
                }
            });
        }

    });

    $('#cancelar').click(function(){
        var $count = Tabla.rows().data().length;
        $('#id').val($count+1);
        $('select').select2('val',[0]);
        $('.reset').val('');
        $('#guardar').attr('data-accion','guardar').val('Guardar');
    });
});

$(document).ready(function() {
    var $folder = $('#folder').val();
    var $file = $('#file').val();

    window.url_accion = $folder+$file+"/";
    window.url_buscar = url_accion+'buscar';
    window.url_eliminar = url_accion+'eliminar';

    var $tabla      = $('.tabla');
    var $formulario = $(".formulario");
    var $guardar    = $('#guardar');

    var Tabla = $tabla.DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
            "buttons": {
                copyTitle: 'Copiar Registros',
                // copyKeys: 'Pulse <i> Ctrl </ i> o <i> \u2318 </ i> + <i> C </ i> para copiar datos de la tabla en el portapapeles. <br> cancelar, haga clic en este mensaje o pulse Esc.',
                copySuccess: {
                    _: 'Copiados %d registros',
                    1: 'Copiado 1 registro'
                }
            }
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "4%", },
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "40%"},
        {"sWidth": "100%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}
        ]
    });

    $.uploadPreview({
        input_field: "#logo_emisor",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Cambio",   // Default: Choose File
        label_selected: "Imagen",  // Default: Change File
        no_label: true                 // Default: false
    });

    $.uploadPreview({
        input_field: "#doc_legal",   // Default: .image-upload
        preview_box: "#image-preview-doc_legal",  // Default: .image-preview
        label_field: "#image-label-doc_legal",    // Default: .image-label
        label_default: "Cambio",   // Default: Choose File
        label_selected: "Imagen",  // Default: Change File
        no_label: true                 // Default: false
    });

    $('.modificar').tooltip({
        html: true,
        placement: 'top',
        title: 'Modificar'
    });
    $('.eliminar').tooltip({
        html: true,
        placement: 'top',
        title: 'Eliminar'
    });

    $formulario.validateForm();

    $guardar.click(function(event) {
        if($.validar()){
            var $url = 'empresa/'+$(this).data('accion');
            var data_send = new FormData($("#frmempresa")[0]);
            $.ajax({
                url: $url,
                type: 'POST',
                cache: false,
                data: data_send,
                processData: false,
                contentType: false,
                dataType: "json"
            }).done(function (data) {
                if (data.success == 'ok') {
                    bootbox.alert({
                        size: 'small',
                        closeButton: false,
                        message: data.msg,
                        callback: function (result) {
                            location.reload();
                        }
                    });
                } else if(data.success=='existe'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                        }
                    });
                }
            });
        }
    });

    $tabla.on('click', 'img.modificar', function() {
        var id = $(this).closest('tr').attr('id');

        $.get(base_url(url_buscar), {id: id}, function(res, textStatus, xhr) {

            $("#id").val(res.id);
            $("#ruc").val(res.ruc);
            $("#razon_social").val(res.razon_social);
            $("#nom_comercial").val(res.nom_comercial);
            $("#clase_contribuyente").val(res.clase_contribuyente);
            $("#direccion_emp").val(res.direccion_emp);
            $("#telefono_emp").val(res.telefono_emp);
            $("#email_emp").val(res.email_emp);
            $("#representante_legal").val(res.representante_legal);
            $("#direccion_rep").val(res.direccion_rep);
            $("#telefono_rep").val(res.telefono_rep);
            $("#email_rep").val(res.email_rep);

            logo_emisor = "fotos/"+res.logo_emisor;
            logo_emisor = logo_emisor.replace(/\s+/g,"");
            $('#image-preview').css("background-image", "url("+assets_url(logo_emisor)+")");
            $('#image-preview').css({"width":"150px","height":"150px"});
            $('#image-preview').css("background-size", "cover");
            $('#image-preview').css("background-position", "center center");

            doc_legal = "fotos/"+res.doc_legal;
            doc_legal = doc_legal.replace(/\s+/g,"");
            $('#image-preview-doc_legal').css("background-image", "url("+assets_url(doc_legal)+")");
            $('#image-preview-doc_legal').css({"width":"150px","height":"150px"});
            $('#image-preview-doc_legal').css("background-size", "cover");
            $('#image-preview-doc_legal').css("background-position", "center center");
            $guardar.attr('data-accion','modificar').val('Modificar');
        },'json');
    });


    $tabla.on('click', 'img.eliminar', function() {
        var id = $(this).closest('tr').attr('id');

        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el registro?</div>',
            callback: function(result){
                if(result){
                    $.get(base_url(url_eliminar), {id: id}, function(data, textStatus, xhr) {
                        if(data.success == 'ok'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                    location.reload();
                                }
                            });
                        } else if(data.success == 'existe'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                }
                            });
                        }
                    },'json');
                }else{
                    $('#cancelar').trigger('click');
                }
            }
        });
    });

    $('#ruc').change(function(){
        var $ruc = $(this);
        if($ruc.val().length < 13){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Disculpe, formato incorrecto solo se admiten 13 dígitos para el RUC",
                callback: function(result){
                    $ruc.focus().val();
                }
            });
        }else if($ruc.val().substring(10,13) !="001"){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Disculpe, formato incorrecto para ser conformado el RUC",
                callback: function(result){
                    $ruc.focus().val();
                }
            });
        }
    });

    $('#cancelar').click(function(){
        var $count = Tabla.rows().data().length;
        $('#id').val($count+1);
        $("select").select2('val',[0]);
        $('.reset').val('');
        $('#guardar').attr('data-accion','guardar').val('Guardar');
    });
});


function readURL(input) {

    alert(input);
    return true;

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function() {

    var $folder = $('#folder').val();
    var $file   = $('#file').val();

    window.url_accion = $folder+$file;
    window.url_buscar = url_accion+'/buscar';
    window.url_eliminar = url_accion+'/eliminar';

    $('select').select2();
    var $tabla      = $('.tabla');
    var $formulario = $(".formulario");
    var $guardar    = $('#guardar');

    var $options = $('#controller > option');

    var items = $options.map(function() {
        var opt = {};
        opt[$(this).val()] = $(this).text();
        return opt;
    }).get();

    /*var opt = $.map($('#controller'), function(v,k) {
        console.log(v.innerHTML+'='+k)
        return $("<option>").val(k).text(v);
    })
    */

    var TTable = $tabla.DataTable({

        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "iDisplayLength": 5,
        "iDisplayStart": 0,
        "language": {
            "url": assets_url('js/es.json')
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "4%", },
        {"sWidth": "40%"},
        {"sWidth": "100%"},
        {"sWidth": "100%"},
        {"sWidth": "100%"},
        {"sWidth": "100%"},
        {"sWidth": "100%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}
        ],
    });

    // $('.modificar').tooltip({
    //     html: true,
    //     placement: 'top',
    //     title: 'Modificar'
    // });

    $tabla.on('mouseenter', ' img.eliminar', function () {
        var clase = $(this).hasClass('modificar');
        var titulo = clase ? 'Modificar' : 'Eliminar';
        $(this).tooltip({placement: 'top', title: titulo, html: true}).tooltip('show');
    });


    $formulario.validateForm();

    $("#controller").change(function () {
        var al = $(this).val();
        $('#route').val('');
        if(al != null){
            var valor = al.replace(/([\/])?[C|c]/, function($0, $1){ return $1 ? $1+ '' : $0; }).toLowerCase();
            valor = valor.replace(/([\/])?[_]/, function($0, $1){ return $1 ? $1+ '' : $0; });
            if(valor != 0){
                $('#route').val(valor);
            }

            //$(this).val(al.replace(/([\/])?c/, function($0, $1){ return $1 ? $1+ 'C' : $0; }));
        }
    });

    $guardar.click(function(event) {
        if($.validar()){
            var data_send = $formulario.serialize();
            var $url = url_accion+'/'+$(this).data('accion');
            $.guardar($url,data_send,$(this).data('accion'),function(data){
                if(data.success=='ok'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            location.reload();
                        }
                    });
                }
            });
        }
    });

    $tabla.on('click','img.accion',function(){
        var id    = $(this).data('id');
        var ids   = TTable.row($(this).closest('tr')).id();
        var row   = TTable.row('#'+id).node();
        var first = $(this).attr('class').split(' ')[0];
        if(first == 'eliminar'){
            $.buscar(url_eliminar,{id:id},first, function(data, textStatus, xhr) {
                if(data.success == 'ok'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            location.reload();
                        }
                    });
                }else if(data.success == 'existe'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                        }
                    });
                }
            },'json');
        }else{

            $.buscar(url_buscar,{id:id},first, function(data, textStatus, xhr) {
                $('#modulo').val(data.modulo);
                $('#posicion').val(data.posicion);
                $('#modulo_id').select2('val',[data.modulo_id])
                $("#controller").select2('val',['0'])
                if(data.modulo_id != 0){
                    $("#controller").select2({data: [{id: data.controller, text: data.controller}]}).select2('val', [data.controller])
                }

                $guardar.val('Modificar').data('accion','modificar');
                $('input[name="id"]').val(id);
            },'json');
        }
    });


    $tabla.find('tbody').on( 'click', 'tr', function (event) {
         // var id = TTable.row(this).id();
         var row = TTable.row( '#1' ).node();
         console.log(row)
        // $elemento = $(event.target);
        // $elemento = $elemento.is('img') ? 1 : 0;
        // if($elemento === 0){
        //     $(this).toggleClass('selected');
        // }
    });

    $('#cancelar').click( function () {
        $guardar.attr('data-accion','guardar').val('Guardar');
        $('#controller option').remove().end();
        // $('#controller').append(opt)
        for(var i = 0; i < items.length; i++) {
            for(key in items[i]) {
                $('#controller').append($('<option>', {
                    value: key,
                    text : items[i][key]
                }));
            }
        }

        $('input[type="text"]').val('');
        $('input[name="activo"]').prop('checked',false).parent('label').removeClass('active');
        $('input[name="activo"][value="1"]').prop('checked',true).parent('label').addClass('active');
        $('select').select2('val',['0']);
        TTable.search( '' ).columns().search( '' ).draw();
    });
    // $('.input-number').inputNumber();
});



$(document).ready(function() {

    var $folder = $('#folder').val();
    var $file = $('#file').val();

    window.url_accion = $folder+$file+"/";
    window.url_buscar = url_accion+'/buscar';
    window.url_procesar = url_accion+'procesar';
    window.url_eliminar = url_accion+'/eliminar';

    var $tabla      = $('.tabla');
    var $formulario = $(".formulario");
    var $formulario_producto = $("#frmproducto");
    var $guardar    = $('#guardar');
    var $guardar_producto = $('#guardar_producto');
    var $comprobante_id    = $('#comprobante_id');
    var $accion    = $('.btn-accion');

    var Tabla = $tabla.DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
            "buttons": {
                copyTitle: 'Copiar Registros',
                // copyKeys: 'Pulse <i> Ctrl </ i> o <i> \u2318 </ i> + <i> C </ i> para copiar datos de la tabla en el portapapeles. <br> cancelar, haga clic en este mensaje o pulse Esc.',
                copySuccess: {
                    _: 'Copiados %d registros',
                    1: 'Copiado 1 registro'
                }
            }
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "4%", },
        {"sWidth": "40%"},
        {"sWidth": "100%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}
        ],
        dom: "Bfrtip",
        buttons: [
        {
            text:'Copiar',
            extend: "copy",
            className: "btn-sm",
            fieldSeparator: ';',
            title: 'Contribuyente',
            exportOptions: {
                columns: [ 0, 1 ]
            },
        },
        {
            extend: "csv",
            className: "btn-sm",
            fieldSeparator: ';',
            title: 'Contribuyente',
            exportOptions: {
                columns: [ 0, 1 ],
            }
        },
        {
            extend: "excel",
            className: "btn-sm",
            title: 'Contribuyente',
        },
        {
            extend: "pdf",
            className: "btn-sm",
            title: 'Contribuyente',
            orientation: 'landscape',
            pageSize: 'LEGAL',
            exportOptions: {
                columns: [ 0, 1 ]
            },
        },
        {
            text:'Imprimir', extend: "print", className: "btn-sm",
            exportOptions: {
                columns: [ 0, 1 ]
            }
        },
        ]
    });


    var TProducto = $('#tblproducto').DataTable({

        "bFilter" : false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "language": {
            "url": assets_url('js/es.json')
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "2%", },
        {"sWidth": "100%"},
        {"sWidth": "2%"},
        {"sWidth": "2%"},
        {"sWidth": "2%"},
        {"sWidth": "2%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}

        ],
    });

    var TProductoDetalle = $('#tblproductodetalle').DataTable({

        "bFilter" : false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "language": {
            "url": assets_url('js/es.json')
        },
        "aoColumns": [
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}
        ],
    });


    $('.modificar').tooltip({
        html: true,
        placement: 'top',
        title: 'Modificar'
    });
    $('.eliminar').tooltip({
        html: true,
        placement: 'top',
        title: 'Eliminar'
    });

    $formulario.validateForm();

    $guardar.click(function(event) {
        var total = TProducto.rows().count();
        var id = $(".lastId").val();
        if($.validar()){
            if(total == 0){
                bootbox.alert({
                    closeButton :false,
                    message: "Disculpe, debe incorporar al menos uno o mas Producto(s)",
                    callback: function(result){
                        $('#producto_id').select2('open');
                        $("ul.nav-tabs li#li_factura").css("display","block");
                        $('.nav-tabs a[href="#div_factura"]').tab('show');
                    }
                });
            }else {
                bootbox.confirm({
                    size: 'small',
                    closeButton :false,
                    message: '<div style="text-align: center" class="text-danger">¿Desea guardar la información?</div>',
                    callback: function(result){
                        if(result){
                            var campos = "";
                            var data_send = new FormData($("#frmcontribuyente")[0]);
                            $('#tblproducto tbody tr').each(function (i) {
                                var cantidad = 0;
                                var producto_id = parseInt(TProducto.row(i).nodes().to$().attr('data-ids'));
                                var datas = TProducto.row(i).data();
                                var precio_unitario = TProducto.cell(i,2).data();
                                var descuento = TProducto.cell(i,3).data();

                                var cell = TProducto.cell(i,4).node();
                                cantidad = $('input', cell).val();

                                var total = $(this).find("td").eq(5).html();
                                if(precio_unitario != undefined && producto_id != undefined && descuento != undefined && cantidad != undefined && total != undefined){
                                    producto_id = producto_id;
                                    precio_unitario = precio_unitario;
                                    descuento = descuento;
                                    cantidad = cantidad;
                                    total = total;
                                    campos = producto_id + ";" + precio_unitario + ";" + descuento + ";" + cantidad + ";" + total +";" +id;
                                    data_send.append("campos[]", campos);

                                }
                            });
                            var $url = 'contribuyente/guardar';
                            $.ajax({
                                url: $url,
                                type: 'POST',
                                cache: false,
                                data: data_send,
                                processData: false,
                                contentType: false,
                                dataType: "json"
                            }).done(function (data) {
                                if (data.success == 'ok') {
                                    bootbox.alert({
                                        size: 'small',
                                        closeButton: false,
                                        message: data.msg,
                                        callback: function (result) {
                                            location.reload();
                                        }
                                    });
                                } else if(data.success=='existe'){
                                    bootbox.alert({
                                        closeButton :false,
                                        message: data.msg,
                                        callback: function(result){
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        }
    });

    // Validacion de clave de acceso
    $(".btn-validar-clave-acceso").click(function(event) {
        var $id = $("#clave_acceso_id").val();
        var $clave = $("#clave_acceso_id").find('option').filter(':selected').text();
        var $length_clave = $clave.length;
        if($length_clave == 48){
            bootbox.alert({
                closeButton :false,
                message: "Clave error, longitud no esta conformada por 49 dígitos, se envia como observación para su revisión correspondiente",
                callback: function(result){
                    $.get(base_url(url_procesar), {id: $id}, function(res, textStatus, xhr) {
                    },'json');
                }
            });
        }else{
            bootbox.alert({
                closeButton :false,
                message: "Clave Valida",
                callback: function(result){
                }
            });
        }

    });

    $("#cliente_id").change(function(event) {
        var id = $(this).val();
        if (id > 0){
            var cliente = $(this).find('option').filter(':selected').text().split('-')[1];
            $("#num_ruc").val(cliente);
        }else{
            $("#num_ruc").val("");
        }

    });

    // Validacion de opciones de comprobante
    $comprobante_id.change(function(event) {
        var $v = $(this).val();
        var $accion = $(".btn-accion");
        if($v == 2){
            $accion.prop('disabled', false);
            $(".btn-accion").text('Nota de Crédito');
        }else if($v == 3){
            $accion.prop('disabled', false);
            $(".btn-accion").text('Nota de Débito');
        }else if($v == 4){
            $accion.prop('disabled', false);
            $(".btn-accion").text('Guia de Remisión');
        }else if($v == 5){
            $accion.prop('disabled', false);
            $(".btn-accion").text('Comprobante de Retención');
        }else{
            $accion.prop('disabled', true);
            $(".btn-accion").text('Factura');
            $("ul.nav-tabs li#li_prod").css("display","none");
        }
    });

    $('#producto_id').change(function(){
        var id = $(this).val();
        if(id > 0){
            var producto_id = $(this).val();
            var producto = $(this).find('option').filter(':selected').text();
            var array_val = [];
            var x = 1;
            $.buscar('registro/producto/buscar_producto',{id:id},'', function(dato, textStatus, xhr) {
                var iva_vigente = 0.00;
                var iva_cero = 0.00;
                var objeto_iva = 0.00;
                var exento_iva = 0.00;
                var descuento = 0.00;
                var ice = 0.00;
                var valor_vigente = 0.00;
                var irbpnr = 0.00;
                var propina = 0.00;
                var descuento_solidario = 0.00;
                var valor_total= 0.00;
                var bandera = false;
                var sum_total_pagar = 0.00;
                $("span.iva_vigente").html(dato.porcentaje);

                //alert(dato.validador);
                if(dato.validador == 1){
                    //alert(dato.valor_unitario)
                    iva_vigente_actual = $("#iva_vigente").val();
                    iva_vigente += parseFloat(dato.valor_unitario) + parseFloat(iva_vigente_actual);
                    $("#iva_vigente").val(iva_vigente.toFixed(2));
                }
                if(dato.validador == 2){
                    iva_cero_actual = $("#iva_cero").val();
                    iva_cero += parseFloat(dato.valor_unitario) + parseFloat(iva_cero_actual);
                    $("#iva_cero").val(iva_cero.toFixed(2));
                }
                if(dato.validador == 3){
                    objeto_iva_actual = $("#objeto_iva").val();
                    objeto_iva += parseFloat(dato.valor_unitario) + parseFloat(objeto_iva_actual);
                    $("#objeto_iva").val(objeto_iva.toFixed(2));
                }
                if(dato.validador == 4){
                    exento_iva_actual = $("#exento_iva").val();
                    exento_iva += parseFloat(dato.valor_unitario) + parseFloat(exento_iva_actual);
                    $("#exento_iva").val(exento_iva.toFixed(2));
                }
                descuento_actual = $("#descuento").val();
                descuento += parseFloat(dato.descuento) + parseFloat(descuento_actual);
                $("#descuento").val(descuento.toFixed(2));

                // Calculo de Iva Vigente
                sum_iva_vigente = $("#iva_vigente").val();
                sum_ice = $("#ice").val();
                valor_iva_vigente = parseFloat(sum_iva_vigente) + parseFloat(sum_ice);
                $("#valor_vigente").val(valor_iva_vigente);

                // Calculo de Valor A Pagar
                sum_iva_vigente = $("#iva_vigente").val();
                sum_ice_v = $("#ice").val();
                sum_irbpnr = $("#irbpnr").val();
                sum_valor_vigente = $("#valor_vigente").val();
                sum_propina = $("#propina").val();
                var subtotal = TProducto.column(5).data().sum();
                //alert(subtotal)
                sum_total_pagar = parseFloat(sum_iva_vigente)
                                    + parseFloat(sum_ice_v)
                                    + parseFloat(sum_irbpnr)
                                    + parseFloat(sum_valor_vigente)
                                    + parseFloat(sum_propina)
                $("#valor_total").val(parseFloat(sum_total_pagar) + parseFloat(subtotal));

                TProducto.rows().eq(0).each( function ( index ) {
                    var producto_ids = parseInt(TProducto.row(index).nodes().to$().attr('data-ids'));
                    if ($('#'+producto_id).length > 0) {
                        bandera = true;
                    }
                } );
                if(bandera == false){
                    var cantidad =  '<input type="text" class="cantidad" value="1" style="width:100% !important;"/>';
                    var eliminar = "<img data-id=" + dato.id + " class='eliminar_producto' src='" + assets_url('img/datatable/eliminar.png') + "' style='cursor:pointer;' title='Eliminar elemento de la lista de producto'>";
                    array_val.push('');
                    array_val.push(producto);
                    array_val.push(dato.valor_unitario);
                    array_val.push(dato.descuento);
                    array_val.push(cantidad);
                    var total = dato.valor_unitario * 1;
                    array_val.push(total);
                    array_val.push(eliminar);
                    TProducto.row.add(array_val).draw();
                    var total = TProducto.rows().count();
                    for (var i = 0; i< total; i++) {
                        TProducto.cell(i,0).data(i+1).draw();
                    }
                    var $count = TProducto.rows().data().length;
                    var $last  = $count-1;
                    TProducto.row($last).nodes().to$().attr({'id':producto_id,'data-ids':producto_id});
                }

            },'json');
        }else{
            TProducto.clear().draw();
        }

    });

    $('#tblproducto').on('change','input.cantidad', function(){
        var cantidad = $(this).val();
        var padre    = $(this).closest('tr');
        var ind      = padre.index();
        var precio   = TProducto.cell(ind,2).data();
        var total    = precio*cantidad;

        TProducto.cell(ind,5).data(total.toFixed(2)).draw();

        var subtotal = TProducto.column(5).data().sum();
        $("#sin_impuesto").val(subtotal);
        //$(TProducto.column(5).footer()).text(subtotal);

    });

    // Eliminar elemento de la lista de producto(s)
    $('table#tblproducto').on('click', 'img.eliminar_producto', function () {
        var id = $(this).data('id');
        var i = $(this).index();
        var elemento = $(this);
        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el producto?</div>',
            callback: function(result){
                if(result){
                    TProducto.row(i).remove().draw();
                }
            }
        });
    });

    $tabla.on('click', 'img.modificar', function() {
        var id = $(this).closest('tr').attr('id');

        $.get(base_url(url_buscar), {id: id}, function(res, textStatus, xhr) {
            $.each(res, function (i, valor) {
                var tipo = $('#' + i).prop('tagName');
                if (tipo !== undefined) {
                    if (tipo.toLowerCase() == 'input') {
                        $('#' + i).val(valor);
                    } else if (tipo.toLowerCase() == 'select') {
                        $('#' + i).select2('val',[valor]);
                    }else if (tipo.toLowerCase() == 'textarea') {
                        $('#' + i).val(valor);
                    }
                } else {
                    var va = 1;
                    if (valor == 'f') {
                        var va = 0;
                    }
                    $('[name="' + i + '"]').parent('label').removeClass('active');
                    $('[name="' + i + '"][value="' + va + '"]').prop('checked', true).parent('label').addClass('active');
                }
            });
            $guardar.attr('data-accion','modificar').val('Modificar');
        },'json');
    });


    $tabla.on('click', 'img.eliminar', function() {
        var id = $(this).closest('tr').attr('id');

        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el registro?</div>',
            callback: function(result){
                if(result){
                    $.get(base_url(url_eliminar), {id: id}, function(data, textStatus, xhr) {
                        if(data.success == 'ok'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                    location.reload();
                                }
                            });
                        } else if(data.success == 'existe'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                }
                            });
                        }
                    },'json');
                }else{
                    $('#cancelar').trigger('click');
                }
            }
        });
    });

    $('#cancelar').click(function(){
        var $count = Tabla.rows().data().length;
        $('#id').val($count+1);
        $("select").select2('val', [0]);
        $('.reset').val('');
        $("div.div_content_factura").css('display','block');
        $('#guardar').attr('data-accion','guardar').val('Guardar');
    });
});

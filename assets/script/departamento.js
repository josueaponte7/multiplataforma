$(document).ready(function() {
    var Tabla = $('.tabla').DataTable({
        "bLengthChange": false,
        "iDisplayLength": 5,
        "iDisplayStart": 0,
        "order": [[ 0, "desc" ]],
        "language": {
            "url": assets_url('js/es.json')
        }
    });
});
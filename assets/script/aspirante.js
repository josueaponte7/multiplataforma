$(document).ready(function() {
    var $folder = $('#folder').val();
    var $file = $('#file').val();
    $("#fecha_ingreso,#fecha_nac,#desde,#hasta,#fecha").mask("99-99-9999", {placeholder: "DD-MM-YYYY"});
    $(".telefono").mask("(9999) 999-99-99", {placeholder: "(0000) 000-00-00"});
    $(".duracion").mask("99");

    window.url_accion = $folder+$file+"/";
    window.url_buscar = url_accion+'buscar';
    window.url_eliminar = url_accion+'eliminar';

    var $tabla      = $('.tabla');
    var $formulario = $("#frmaspirante");
    var $guardar    = $('#guardar');

    var Tabla = $tabla.DataTable({
        "bLengthChange": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "order": [[ 0, "asc" ]],
        "language": {
            "url": assets_url('js/es.json'),
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "4%", },
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "100%"},
        {"sWidth": "2%"},
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        {"sWidth": "5%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        ]
    });

    // Estructura lista Referencia Personal
    var TablaRP = $(".table-referencia").DataTable({
        "bFilter" : false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "language": {
            "url": assets_url('js/es.json'),
        },
        "aoColumns": [
        {"sClass": "right", "sWidth": "100%", },
        {"sWidth": "4%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
        ]
    });

    $.uploadPreview({
        input_field: "#foto_perfil",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Cambio",   // Default: Choose File
        label_selected: "Imagen",  // Default: Change File
        no_label: true                 // Default: false
    });

    $('.modificar').tooltip({
        html: true,
        placement: 'top',
        title: 'Modificar'
    });
    $('.eliminar').tooltip({
        html: true,
        placement: 'top',
        title: 'Eliminar'
    });

    $formulario.validateForm();

    $guardar.click(function(event) {

        if($.validar()){

            var $url = 'aspirante/'+$(this).data('accion');
            var data_send = new FormData($("#frmaspirante")[0]);
            $.ajax({
                url: $url,
                type: 'POST',
                cache: false,
                data: data_send,
                processData: false,
                contentType: false,
                dataType: "json"
            }).done(function (data) {
                if (data.success == 'ok') {
                    bootbox.alert({
                        size: 'small',
                        closeButton: false,
                        message: data.msg,
                        callback: function (result) {
                            location.reload();
                        }
                    });
                } else if(data.success=='existe'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                        }
                    });
                }
            });
        }
    });

    $tabla.on('click', 'button.panel-show', function() {
        $("div.content-panel-element").toggle(1000);
        var id = $(this).closest('tr').attr('id');
        $(".panel-valor").text(id);
    });

    $('button.referencia').click(function(){
        var id = $(".panel-valor").text();
        window.location.href = base_url('registro/referencia?id='+id);
    });

    $('button.habilidad').click(function(){
        var id = $(".panel-valor").text();
        window.location.href = base_url('registro/habilidad?id='+id);
    });

    $('button.experiencia').click(function(){
        var id = $(".panel-valor").text();
        window.location.href = base_url('registro/experiencia?id='+id);
    });

    $('button.educacion').click(function(){
        var id = $(".panel-valor").text();
        window.location.href = base_url('registro/educacion?id='+id);
    });

    $('button.curso').click(function(){
        var id = $(".panel-valor").text();
        window.location.href = base_url('registro/curso?id='+id);
    });

    $('button.close-panel-element').click(function(){
        $("div.content-panel-element").hide(1000);
    });

    $tabla.on('click', 'img.modificar', function() {
        var id = $(this).closest('tr').attr('id');
        $.get(base_url(url_buscar), {id: id}, function(res, textStatus, xhr) {
            $("#id").val(res.id);
            $("#cedula").val(res.cedula);
            $("#p_nombre").val(res.p_nombre);
            $("#s_nombre").val(res.s_nombre);
            $("#p_apellido").val(res.p_apellido);
            $("#s_apellido").val(res.s_apellido);
            $("#edad").val(res.edad);
            $("#lugar_nac").val(res.lugar_nac);
            $("#correo").val(res.correo);
            $("#direccion").val(res.direccion);
            $("#telefono").val(res.telefono);
            $("#nacionalidad").select2("val",[res.nacionalidad]);
            $("#genero_id").select2("val",[res.genero_id]);
            $("#estado_civil_id").select2("val",[res.estado_civil_id]);
            $("#estatus").select2("val",[res.estatus]);
            $("#tiempo_contracto").val(res.tiempo_contracto);
            fecha_nac = res.fecha_nac.split('-');
            fecha_nac = fecha_nac[2]+"-"+fecha_nac[1]+"-"+fecha_nac[0];
            $("#fecha_nac").val(fecha_nac);
            foto_perfil = "fotos/"+res.foto_perfil;
            foto_perfil = foto_perfil.replace(/\s+/g,"");
            $('#image-preview').css("background-image", "url("+assets_url(foto_perfil)+")");
            $('#image-preview').css({"width":"130px","height":"180px"});
            $('#image-preview').css("background-size", "cover");
            $('#image-preview').css("background-position", "center center");
            $("#content_asp").show();
            $(".table-asp").hide();
            $guardar.attr('data-accion','modificar').val('Modificar');
        },'json');
    });

    $tabla.on('click', 'img.eliminar', function() {
        var id = $(this).closest('tr').attr('id');

        bootbox.confirm({
            size: 'small',
            closeButton :false,
            message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el registro?</div>',
            callback: function(result){
                if(result){
                    $.get(base_url(url_eliminar), {id: id}, function(data, textStatus, xhr) {
                        if(data.success == 'ok'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                    location.reload();
                                }
                            });
                        } else if(data.success == 'existe'){
                            bootbox.alert({
                                size:'small',
                                closeButton :false,
                                message: data.msg,
                                callback: function(result){
                                }
                            });
                        }
                    },'json');
                }else{
                    $('#cancelar').trigger('click');
                }
            }
        });
    });

    $('.nuevo-aspirante').click(function(){
        $("#content_asp").show();
        $(".table-asp").hide();
    });

    // Validacion para calcular la edad con la fecha de nacimiento
    $('#fecha_nac').change(function(){
        var fecha_nac = $(this).val();
        $.edad(fecha_nac,'edad');
    });

    $('#nacionalidad,#cedula').change(function(){
        var cedula = $(this).val();
        var nacionalidad = $("#nacionalidad").val();
        if(nacionalidad == 1 && cedula.length < 10){
            bootbox.alert({
                size:'small',
                closeButton :false,
                message: "Formato incorrecto",
                callback: function(result){
                }
            });
        }else {
            return false;
        }
    });

    $('.topologia').change(function(){
        var tipo=$(this).attr('id');
        var id=$(this).val();
        $.get(base_url('registro/aspirante/ajax_search'),{tipo:tipo, id:id}, function(data) {

        });
    })

    $('#cancelar').click(function(){
        var $count = Tabla.rows().data().length;
        $('#id').val($count+1);
        $("select").select2('val',[0]);
        $('.reset').val('');
        $("#content_asp").hide();
        $(".table-asp").show();
        $('#guardar').attr('data-accion','guardar').val('Guardar');
    });
});
















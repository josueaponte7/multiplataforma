window.url_accion = 'seguridad/users/';
var row_action = 0;
$(document).ready(function () {

    $("select").select2();

    var $forms = $("#frmusers");
    var $table = $('#tblusers');

    var TTable = $table.DataTable({
        "pagingType": "full_numbers",
        "language": {
            "url": assets_url('js/es.json')
        },
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "iDisplayLength": 5,
        "iDisplayStart": 0,
    });
    $forms.validateForm();

    $table.on('mouseenter', 'img.modificar, img.eliminar', function () {
        var clase = $(this).hasClass('modificar');

        var titulo = clase ? 'Modificar' : 'Eliminar';
        $(this).tooltip({placement: 'top', title: titulo, html: true}).tooltip('show');
    });

    $('#guardar').click(function (event) {

        if ($.validar()) {
            var $perfil = $('#perfil').val();
            var $activo = $('input:radio:checked').siblings('span').text();
            // SABER CUANTOS ELEMNTOS HAY EN LA TABLA Y OBTENER LA ULTIMA  FILA
            //var $ultimo = 1 + parseInt(otable.cell((otable.rows().data().length)-1,0).data()[0]);
            var data_send = $forms.not("[data-nosend=true]").serialize();
            var $url = url_accion + $(this).data('accion');
            // IMPORTANTE OBTERNER EL ULTIMO ID LA TABLA
            // console.log(TTable.row((TTable.rows().data().length)-1).nodes().to$().attr('data-ids','25'))
            $.guardar($url, data_send, $(this).data('accion'), function (data) {
                if (data.success == 'ok') {
                    bootbox.alert({
                        closeButton: false,
                        message: data.msg,
                        callback: function (result) {
                            location.reload();
                        }
                    });
                }
            });
        }
    });

    /*$table.on('click','img.mouse',function(){
        var $this = $(this);
        var id    = $this.data('id');
        var ids   = TTable.row($this.closest('tr')).id();
        var row   = TTable.row('#'+id).node();
        var first = $this.attr('class').split(' ')[0];
        row_action = $this.closest('tr').index();
        if(first == 'eliminar'){
            var url = url_accion+'eliminar';
            $.buscar(url,{id:id},first, function(data, textStatus, xhr) {
                if(data.success == 'ok'){
                    bootbox.alert({
                        size:'small',
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            TTable.row($this.parents('tr')).remove().draw();
                            var data = TTable.columns().data()[0];
                            var j = 1;
                            $.each(data, function(i){
                                var num = TTable.cell(i,0).data(j++).draw();
                            });
                        }
                    });
                }
            },'json');
        }else{
            $url = url_accion+'buscar';
            $.buscar($url,{id:id},first, function(data, textStatus, xhr) {

                $.each(data, function(i, valor) {
                    $("#id").val(valor.id);
                    $("#si_user").val(valor.si_user);
                    $("#first_name").val(valor.first_name);
                    $("#last_name").val(valor.last_name);
                    $("#correo").val(valor.correo);

                    $("select#perfil_id").val(valor.perfil_id);
                    $("select#show_panel").val(valor.show_panel);
                });
                $('#guardar').val('Modificar').data('accion','modificar');
                $('#if').val(id);
            },'json');
        }
    });*/
    $('#tblusers tbody').on('click', 'tr', function (event) {
        // var id = TTable.row( this ).id();
        var id = TTable.row( this ).id().node().data('id');
        $url = url_accion+'buscar';
        $.buscar($url,{id:id},'modificar', function(data, textStatus, xhr) {

            $.each(data, function(i, valor) {
                $("#id").val(valor.id);
                $("#si_user").val(valor.si_user);
                $("#first_name").val(valor.first_name);
                $("#last_name").val(valor.last_name);
                $("#correo").val(valor.correo);

                $("select#perfil_id").select2('val',[valor.perfil_id]);
                $("select#show_panel").select2('val',[valor.show_panel]);
            });
            $('#guardar').val('Modificar').data('accion','modificar');
            $('#if').val(id);
        },'json');
    });
    $('#cancelar').click(function () {
        $('input[type="text"]').val('');
        $('input[type="password"]').val('');
        $('input[name="activo"]').prop('checked', false).parent('label').removeClass('active');
        $('input[name="activo"][value="1"]').prop('checked', true).parent('label').addClass('active');
        $('select:not("[name=tblusers_length]")').each(function() {
            var id = $(this).attr('id');
             $('select#'+id).find('option:eq(0)').each(function() {
                 $('select#'+id).select2().select2('val', $(this).val());
             });
        });

        $('#guardar').val('Guardar');
    //otable.search( '' ).columns().search( '' ).draw();
});
// $('.input-number').inputNumber();
});
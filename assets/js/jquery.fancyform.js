(function ($) {
    $.fn.extend({

        transformCheckbox: function (settings) {
            var defaults = {
					base : "image", // Can be image/class
					checked: "",
					unchecked: "",
					disabledChecked: "",
					disabledUnchecked: "",
					tristateHalfChecked: "",
					changeHandler: function (is_checked) { },
					trigger: "self", // Can be self, parent
					tristate: 0
				},
			options = $.extend(defaults, settings),
			method = {
                // Handling the image
                setImage: function () {
                    var cb = $(this),
						settings = cb.data('settings'),
						src;

                    if (cb.is(":disabled")) {
                        src = cb.is(":checked") ? "disabledChecked" : "disabledUnchecked";
                    }
                    else if (cb.hasClass("half-checked")) // Tri-state image
                    {
                        src = "tristateHalfChecked";
                    }
                    else if (cb.is(":checked")) {
                        src = "checked";
                    }
                    else {
                        src = "unchecked";
                    }

					if( settings.base == "image" )
					{
						cb.next().attr("src", settings[src]);
					}
					else
					{
						_removeClasses.call(this);
						cb.parent().addClass(src);
					}
                },
                setProp: function (el, name, bool) {
                    $(el).prop(name, bool).change();
                    method.setImage.call(el);

					// Checked and radio, change others
					if( name == "checked" && !$(el).data('settings').type )
					{
						$("[name='" + $(el).attr("name") + "']").each(function(){
							method.setImage.call(this);
						});
					}
                },
                // Handling the check/uncheck/disable/enable functions
                uncheck: function () {
                    method.setProp(this, "checked", 0);
                },
                check: function () {
                    method.setProp(this, "checked", 1);
                },
                disable: function () {
                    method.setProp(this, "disabled", 1);
                },
                enable: function () {
                    method.setProp(this, "disabled", 0);
                },
                // Clicking the image
                imageClick: function () {
                    var _this = $(this),
						settings = _this.data('settings');

                    if (!_this.is(":disabled")) {
                        if (_this.is(":checked") && settings.type) {
							method.uncheck.call(_this);
							options.changeHandler.call(_this, 1);
						}
						else {
							method.check.call(_this);
							options.changeHandler.call(_this, 0);
						}
						method.handleTriState.call(_this);
                    }
                },
                // Tristate
                handleTriState: function () {
                    var cb = $(this),
						settings = cb.data('settings'),
						li = cb.parent(),
						ul = li.find("ul"),
						pli = li.closest("li");

                    if (settings.tristate)
					{
						// Fix children
						if (cb.hasClass("half-checked") || cb.is(":checked")) {
							cb.removeClass("half-checked");
							method.check.call(cb);
							ul.find("input:checkbox").removeClass("half-checked").each(method.check);
						}
						else if (cb.not(":checked")) {
							cb.removeClass("half-checked");
							ul.find("input:checkbox").each(method.uncheck);
						}
						ul.find("input:checkbox").each(method.setImage);

						// Fix parents
						if (cb.parent().parent().parent().is("li")) {
							method.handleTriStateLevel.call(cb.parent().parent().parent());
						}

						cb.trigger("transformCheckbox.tristate");
					}
                },
                // Handle all including parent levels
                handleTriStateLevel: function (upwards) {
                    var _this = $(this),
						firstCheckbox = _this.find("input:checkbox").first(),
						ul = _this.find("ul"),
						inputs = ul.find("input:checkbox"),
						checked = inputs.filter(":checked");
					if( upwards !== false || inputs.length) {
						firstCheckbox.removeClass("half-checked");

						if (inputs.length == checked.length) {
							method.check.call(firstCheckbox);
						}
						else if (checked.length) {
							firstCheckbox.addClass("half-checked");
						}
						else {
							method.uncheck.call(firstCheckbox);
						}
						method.setImage.call(firstCheckbox);

						if (upwards !== false && _this.parent().parent().is("li")) {
							method.handleTriStateLevel.call(_this.parent().parent());
						}
					}
                }
            }

            return this.each(function () {
                if (typeof settings == "string") {
                    method[settings].call(this);
                }
                else {
                    var _this = $(this);

                    // Is already initialized
                    if (!_this.data("tf.init")) {
						// set initialized
						_this.data("tf.init", 1)
							   .data("settings", options);

						options.type = _this.is("[type=checkbox]");

						// Radio hide
						_this.hide();

						// Afbeelding
						if( options.base == "image" )
						{
							_this.after("<img />");
						}
						else
						{
							_this.wrap("<span class='trans-element-" + (options.type ? "checkbox" : "radio") + "' />");
						}
						method.setImage.call(this);
						if (settings.tristate) {
							method.handleTriStateLevel.call(_this.parent(), false);
						}

						if( options.base == "image" )
						{
							switch (options.trigger) {
								case "parent":
									_this.parent().click($.proxy(method.imageClick, this));
									break;
								case "self":
									_this.next("img").click($.proxy(method.imageClick, this));
									break;
							}
						}
						else
						{
							switch (options.trigger) {
								case "parent":
									_this.parent().parent().click($.proxy(method.imageClick, this));
									break;
								case "self":
									_this.parent().click($.proxy(method.imageClick, this));
									break;
							}
						}
					}
                }
            });
        },
    });

	// Radio and checkbox now use the same function
	$.fn.transformRadio = $.fn.transformCheckbox;
})(jQuery);

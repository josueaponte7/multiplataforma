 $(document).ready(function() {
        $('#login').click(function(event) {
            if($('#user').val() === null || $('#user').val().length === 0 || /^\s+$/.test($('#user').val())){
                $('#user').focus();
            }else if($('#password').val() === null || $('#password').val().length === 0 || /^\s+$/.test($('#password').val())){
                $('#password').focus();
            }else{
                var data_send = $('#frmlogin').serialize();

                $.post(base_url('seguridad/users/validatelogin'),data_send, function(data, textStatus, xhr) {
                    if(data.status == 'success'){
                        window.location=base_url('inicio');
                    }else{
                        $('#error').css('display','block')
                    }
                },'json');
            }
        });

        $(document).keypress(function(e){
            if(e.keyCode == 13){
                $('#login').trigger('click');
            }
        });
    });